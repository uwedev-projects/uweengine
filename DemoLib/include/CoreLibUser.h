#pragma once

#include "UE/Utility/HighResolutionTimer.h"

using Uwe::Utility::HighResolutionTimer;

class CoreLibUser {
	HighResolutionTimer<> hrt;

public:
	CoreLibUser();
};