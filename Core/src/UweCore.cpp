﻿#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "UweCore.h"
#include "Config.h"

#include "Rendering/Shader.h"

#include <iostream>

namespace Uwe {

	void sanityCheck() {
		Rendering::Shader myShader{};

		std::cout << "Sanity Check succeeded!" << std::endl;
	}

	std::string getVersion() {
		return std::string(Config::UweEngineVersion);
	}

}