cmake_minimum_required (VERSION 3.12)

target_sources (UweCoreObjects
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/Shader.h
		${CMAKE_CURRENT_SOURCE_DIR}/Shader.cpp
)