#include "Rendering/Shader.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <string>

#include <glm/gtc/type_ptr.hpp>

namespace Uwe {

namespace Rendering {

Shader::Shader(const std::string& vertexSource, const std::string& fragmentSource, const std::string& geometrySource)
	: id{}, hasGeometryShader{ geometrySource.length() > 0 }, vertexShaderCode{}, fragmentShaderCode{}, geometryShaderCode{}
{
	if (vertexSource.empty() || fragmentSource.empty())
		throw std::invalid_argument{ "The path to the fragment or vertex shader source must not be empty." };

	std::ifstream vertexShaderFileStream {};
	std::ifstream fragmentShaderFileStream {};
	std::ifstream geometryShaderFileStream {};

	vertexShaderFileStream.exceptions(std::ios::badbit | std::ios::failbit);
	fragmentShaderFileStream.exceptions(std::ios::badbit | std::ios::failbit);
	geometryShaderFileStream.exceptions(std::ios::badbit | std::ios::failbit);

	try {
		vertexShaderFileStream.open(vertexSource, std::ios::in);
		fragmentShaderFileStream.open(fragmentSource, std::ios::in);

		std::stringstream vertexSourceStream {};
		std::stringstream fragmentSourceStream {};

		vertexSourceStream << vertexShaderFileStream.rdbuf();
		fragmentSourceStream << fragmentShaderFileStream.rdbuf();

		vertexShaderFileStream.close();
		fragmentShaderFileStream.close();

		vertexShaderCode = vertexSourceStream.str();
		fragmentShaderCode = fragmentSourceStream.str();

		if (hasGeometryShader) {
			geometryShaderFileStream.open(geometrySource, std::ios::in);

			std::stringstream geometrySourceStream{};
			geometrySourceStream << geometryShaderFileStream.rdbuf();
			geometryShaderFileStream.close();

			geometryShaderCode = geometrySourceStream.str();
		}
	} 
	catch (std::ifstream::failure &e) {
		std::cout << "ERROR: Failed to open or read from vertex / fragment shader source file." << std::endl
			<< e.what() << std::endl;
		throw;
	}

	assemble();
}

Shader::~Shader()
{
}

void Shader::assemble() {
	unsigned int vertexShader{ glCreateShader(GL_VERTEX_SHADER) };
	unsigned int fragmentShader{ glCreateShader(GL_FRAGMENT_SHADER) };
	unsigned int geometryShader{ hasGeometryShader ? glCreateShader(GL_GEOMETRY_SHADER) : 0 };

	try {
		compile(vertexShader, fragmentShader, geometryShader);
		linkProgram(vertexShader, fragmentShader, geometryShader);
	}
	catch (...) {
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
		glDeleteShader(geometryShader);
		throw;
	}
	
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	glDeleteShader(geometryShader);

	std::cout << "DEBUG: shader program #" << id << " assembly successful." << std::endl;
}

void Shader::compile(unsigned int& vertexShader, unsigned int& fragmentShader, unsigned int& geometryShader) {
	int success{};
	char shaderErrorLog[512]{};

	const char* sourceCodeCString{ vertexShaderCode.c_str() };

	glShaderSource(vertexShader, 1, &sourceCodeCString, nullptr);
	glCompileShader(vertexShader);

	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, nullptr, shaderErrorLog);
		std::string errMsg{ "ERROR: Compilation of vertex shader failed!\n" };
		errMsg += shaderErrorLog;
		throw std::exception{ errMsg.c_str() };
	}

	sourceCodeCString = fragmentShaderCode.c_str();

	glShaderSource(fragmentShader, 1, &sourceCodeCString, nullptr);
	glCompileShader(fragmentShader);

	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragmentShader, 512, nullptr, shaderErrorLog);
		std::string errMsg{ "ERROR: Compilation of fragment shader failed!\n" };
		errMsg += shaderErrorLog;
		throw std::exception{ errMsg.c_str() };
	}
	
	if (hasGeometryShader) {
		sourceCodeCString = geometryShaderCode.c_str();

		glShaderSource(geometryShader, 1, &sourceCodeCString, nullptr);
		glCompileShader(geometryShader);

		glGetShaderiv(geometryShader, GL_COMPILE_STATUS, &success);
		if (!success) {
			glGetShaderInfoLog(geometryShader, 512, nullptr, shaderErrorLog);
			std::string errMsg{ "ERROR: Compilation of geometry shader failed!\n" };
			errMsg += shaderErrorLog;
			throw std::exception{ errMsg.c_str() };
		}
	}
}

void Shader::linkProgram(unsigned int vertexShader, unsigned int fragmentShader, unsigned int geometryShader) {
	int success{};
	char programErrorLog[512]{};

	id = glCreateProgram();
	
	glAttachShader(id, vertexShader);
	glAttachShader(id, fragmentShader);
	
	if(hasGeometryShader)
		glAttachShader(id, geometryShader);

	glLinkProgram(id);
	glGetProgramiv(id, GL_LINK_STATUS, &success);

	if (!success) {
		glGetProgramInfoLog(id, 512, nullptr, programErrorLog);
		std::string errMsg{ "ERROR: Failed to link the shader program: " + id };
		errMsg += "\n";
		errMsg += programErrorLog;
		throw std::exception{ errMsg.c_str() };
	}
}

void Shader::validate() const {
	char programErrorLog[512]{};

	glValidateProgram(id);

	int success{};
	glGetProgramiv(id, GL_VALIDATE_STATUS, &success);

	if (!success) {
		glGetProgramInfoLog(id, 512, nullptr, programErrorLog);
		std::string errMsg{ "ERROR: Failed to validate shader.\n" };
		errMsg += programErrorLog;
		throw std::exception{ errMsg.c_str() };
	}
};

void Shader::use() const {
	glUseProgram(id);
};

void Shader::setVector(const std::string& name, float x, float y, float z, float w) {
	glUniform4f(glGetUniformLocation(id, name.c_str()), x, y, z, w);
};

void Shader::setVector(const std::string& name, float x, float y, float z) {
	glUniform3f(glGetUniformLocation(id, name.c_str()), x, y, z);
};

void Shader::setVector(const std::string& name, const glm::vec3& vec) {
	glUniform3f(glGetUniformLocation(id, name.c_str()), vec.x, vec.y, vec.z);
};

void Shader::setMatrix(const std::string& name, const glm::mat4& matrix) {
	glUniformMatrix4fv(glGetUniformLocation(id, name.c_str()), 1, GL_FALSE, glm::value_ptr(matrix));
};

void Shader::setMatrix(const std::string& name, const glm::mat3& matrix) {
	glUniformMatrix3fv(glGetUniformLocation(id, name.c_str()), 1, GL_FALSE, glm::value_ptr(matrix));
};

void Shader::setMatrices(const std::string& name, const std::vector<glm::mat4>& matrices) {
	for (unsigned int i = 0; i < matrices.size(); ++i) {
		std::string uniformName{ name + "[" + std::to_string(i) + "]" };
		setMatrix(uniformName, matrices[i]);
	};
};

void Shader::setInt(const std::string& name, int value) {
	glUniform1i(glGetUniformLocation(id, name.c_str()), value);
};

void Shader::setInts(const std::string& name, const std::vector<int>& values) {
	for (unsigned int i = 0; i < values.size(); ++i) {
		std::string uniformName{ name + "[" + std::to_string(i) + "]" };
		setInt(uniformName, values[i]);
	};
};

void Shader::setFloat(const std::string& name, float value) {
	glUniform1f(glGetUniformLocation(id, name.c_str()), value);
};

unsigned int Shader::getId() const noexcept {
	return id;
};

}

}