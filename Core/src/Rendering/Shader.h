#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>
#include <string>
#include <vector>

namespace Uwe {

namespace Rendering {

	class Shader
	{
	public:
		Shader() = default;
		Shader(const std::string& vertexSource, const std::string& fragmentSource, const std::string& geometrySource = "");
		~Shader();

		void use() const;
		void validate() const;

		void setVector(const std::string& name, float x, float y, float z, float w);
		void setVector(const std::string& name, float x, float y, float z);
		void setVector(const std::string& name, const glm::vec3& vec);

		void setMatrix(const std::string& name, const glm::mat4& matrix);
		void setMatrix(const std::string& name, const glm::mat3& matrix);

		void setMatrices(const std::string& name, const std::vector<glm::mat4>& matrices);

		void setInt(const std::string& name, int value);
		void setInts(const std::string& name, const std::vector<int>& values);
		void setFloat(const std::string& name, float value);

		unsigned int getId() const noexcept;
	protected:
		unsigned int id;
		bool hasGeometryShader;
		std::string vertexShaderCode;
		std::string fragmentShaderCode;
		std::string geometryShaderCode;

		void assemble();
		void compile(unsigned int& vertexShader, unsigned int& fragmentShader, unsigned int& geometryShader);
		void linkProgram(unsigned int vertexShader, unsigned int fragmentShader, unsigned int geometryShader);
	};

}

}

