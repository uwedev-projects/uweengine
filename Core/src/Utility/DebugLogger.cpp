#include "Utility/DebugLogger.h"

namespace Uwe {

namespace Utility {

	DebugLogger::DebugLogger()
		: Logger<DebugLogger>{LogLevel::Debug}
	{
	}

	DebugLogger& loggerInstance() noexcept {
		static DebugLogger instance{};
		return instance;
	}
}

}