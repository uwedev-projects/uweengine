#include "Time.h"

namespace Uwe {

namespace Utility {

	std::tm getLocalTime(const std::time_t& timePoint) {
		std::tm dateTime{};

		#if (defined(WIN32) || defined(_WIN32) || defined(__WIN32__))
		errno_t err{ localtime_s(&dateTime, &timePoint) };
		if (err)
			throw UweException{ "Unable to retrieve localtime." };
		#else
		localtime_r(&timePoint, &dateTime); // POSIX
		#endif

		return dateTime;
	}

}

}