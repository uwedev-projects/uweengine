#pragma once

#include "Exceptions.h"

#include <time.h>
#include <errno.h>
#include <ctime>
#include <chrono>
#include <string>

namespace Uwe {

	namespace Utility {

		std::tm getLocalTime(const std::time_t& timePoint);

		template <typename Clock = std::chrono::system_clock>
		std::string getLocalTimestamp() {
			auto timePoint{ Clock::now() };
			std::tm dateTime{ getLocalTime(timePoint) };

			char buffer[32U]{};

			#if (defined(_WIN32))
			errno_t err{ asctime_s(buffer, sizeof(buffer), &dateTime) };
			if (err)
				throw UweException{ "Unable to convert std::tm to c string." };
			#else
			const char* result{ asctime_r(&dateTime, buffer) };
			if (!result)
				throw UweException{ "Unable to convert std::tm to c string." };
			#endif

			return std::string{ buffer };
		}

		template <typename Clock = std::chrono::system_clock>
		std::string getTimestamp() {
			auto timePoint{ Clock::now() };
			auto dateTime{ Clock::to_time_t(timePoint) };

			char buffer[32U]{};

			#if (defined(WIN32) || defined(_WIN32) || defined(__WIN32__))
			errno_t err{ ctime_s(buffer, sizeof(buffer), &dateTime) };
			if (err)
				throw UweException{ "Unable to convert std::time_t to c string." };
			#else		
			const char* result{ ctime_r(&dateTime, buffer) };
			if (!result)
				throw UweException{ "Unable to convert std::tm to c string." };
			#endif

			buffer[24] = 0;
			return std::string{ buffer };
		}

	}

}