#ifndef _UWE_CORE_CONFIG_H_
#define _UWE_CORE_CONFIG_H_

namespace Uwe {

namespace Config {

	constexpr char* UweEngineVersion = "0.1.0";
	constexpr short UweEngineVersionMajor = 0;
	constexpr short UweEngineVersionMinor = 1;
	constexpr short UweEngineVersionPatch = 0;


	enum class BuildType : unsigned char { Undefined = 0U, Debug, Release, RelWithDebInfo, MinSizeRel };
	constexpr BuildType Build		= BuildType::RelWithDebInfo;

	constexpr bool IsBigEndian		= false;
	constexpr bool IsLittleEndian	= !IsBigEndian;

}

}

#endif
