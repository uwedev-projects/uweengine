#pragma once

#include <chrono>
#include <ctime>
#include <ratio>

namespace Uwe {

namespace Mocks {

    class FakeClock {
	public:
		using time_point = typename std::chrono::high_resolution_clock::time_point;

		static unsigned long currentNanoseconds;

		static time_point now() noexcept {
			return time_point{ std::chrono::duration<unsigned long, std::nano>(currentNanoseconds) };
		}

		 static std::time_t to_time_t(const time_point& tp) noexcept {
		 	return std::time_t{};
		 }
	};
}

}