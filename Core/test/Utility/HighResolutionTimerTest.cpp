#include "gtest/gtest.h"

#include <chrono>

#include "Mocks/FakeClock.h"
#include "Utility/HighResolutionTimer.h"

namespace Uwe {

namespace Utility {


	TEST(HighResolutionTimerTest, InitializesAtZero) {
		Mocks::FakeClock::currentNanoseconds = 0UL;

		HighResolutionTimer<Mocks::FakeClock> timer{};

		EXPECT_EQ(timer.getNanoseconds(), 0UL);
		EXPECT_EQ(timer.getMicroseconds(), 0UL);
		EXPECT_EQ(timer.getMilliseconds(), 0UL);

		EXPECT_FLOAT_EQ(timer.getSeconds(), 0.0f);
		EXPECT_FLOAT_EQ(timer.getMinutes(), 0.0f);
		EXPECT_FLOAT_EQ(timer.getHours(), 0.0f);
	}

	TEST(HighResolutionTimerTest, StartsTicking) {
		Mocks::FakeClock::currentNanoseconds = 0UL;

		HighResolutionTimer<Mocks::FakeClock> timer{};
		timer.start();
		
		EXPECT_EQ(timer.getNanoseconds(), 0UL);

		Mocks::FakeClock::currentNanoseconds = 1250UL;
		EXPECT_EQ(timer.getNanoseconds(), 1250UL);
		EXPECT_EQ(timer.getMicroseconds(), 1250UL / 1000UL);
		
		Mocks::FakeClock::currentNanoseconds = 125UL * 1000UL * 1000UL;
		EXPECT_EQ(timer.getNanoseconds(), 125UL * 1000UL * 1000UL);
		EXPECT_EQ(timer.getMicroseconds(), 125UL * 1000UL);
		EXPECT_EQ(timer.getMilliseconds(), 125UL);
		EXPECT_FLOAT_EQ(timer.getSeconds(), 125.0f / 1000.0f);
		EXPECT_FLOAT_EQ(timer.getMinutes(), 125.0f / 1000.0f / 60.0f);
		EXPECT_FLOAT_EQ(timer.getHours(), 125.0f / 1000.0f / 60.f / 60.0f);
	}

	TEST(HighResolutionTimerTest, StopsTicking) {
		Mocks::FakeClock::currentNanoseconds = 4250UL;

		HighResolutionTimer<Mocks::FakeClock> timer{};
		timer.start();

		Mocks::FakeClock::currentNanoseconds = 6750UL;
		EXPECT_EQ(timer.getNanoseconds(), 2500UL);
		timer.stop();

		Mocks::FakeClock::currentNanoseconds = 12500UL;
		EXPECT_EQ(timer.getNanoseconds(), 2500UL);
		EXPECT_EQ(timer.getMicroseconds(), 2500UL / 1000UL);
		EXPECT_EQ(timer.getMilliseconds(), 2500UL / 1000UL / 1000UL);
		EXPECT_EQ(timer.getSeconds(), 2.5f / 1000.0f / 1000.0f);
		EXPECT_EQ(timer.getMinutes(), 2.5f / 1000.0f / 1000.0f / 60.0f);
		EXPECT_EQ(timer.getHours(), 2.5f / 1000.0f / 1000.0f / 60.0f / 60.0f);
	}

	TEST(HighResolutionTimerTest, IgnoresMultipleConsecutiveStartCalls) {
		Mocks::FakeClock::currentNanoseconds = 240UL;
		HighResolutionTimer<Mocks::FakeClock> timer{};

		timer.start();
		EXPECT_EQ(timer.getNanoseconds(), 0UL);

		Mocks::FakeClock::currentNanoseconds = 520UL;

		timer.start();
		EXPECT_EQ(timer.getNanoseconds(), 280UL);
	}

	TEST(HighResolutionTimerTest, IgnoresMultipleConsecutivePauseCalls) {
		Mocks::FakeClock::currentNanoseconds = 0UL;
		HighResolutionTimer<Mocks::FakeClock> timer{};

		timer.start();
		Mocks::FakeClock::currentNanoseconds = 200UL;

		timer.stop();
		Mocks::FakeClock::currentNanoseconds = 300UL;

		timer.stop();
		Mocks::FakeClock::currentNanoseconds = 600UL;

		EXPECT_EQ(timer.getNanoseconds(), 200UL);
	}

	TEST(HighResolutionTimerTest, IgnoresPauseBeforeStarted) {
		Mocks::FakeClock::currentNanoseconds = 1000UL;
		HighResolutionTimer<Mocks::FakeClock> timer{};

		timer.stop();

		Mocks::FakeClock::currentNanoseconds = 2000UL;
		timer.start();

		Mocks::FakeClock::currentNanoseconds = 3500UL;
		EXPECT_EQ(timer.getNanoseconds(), 1500UL);
	}

	TEST(HighResolutionTimerTest, ResumesWhenPaused) {
		Mocks::FakeClock::currentNanoseconds = 200UL;
		HighResolutionTimer<Mocks::FakeClock> timer{};

		timer.start();
		Mocks::FakeClock::currentNanoseconds = 300UL;

		timer.stop();
		Mocks::FakeClock::currentNanoseconds = 600UL;

		timer.resume();
		Mocks::FakeClock::currentNanoseconds = 1000UL;

		EXPECT_EQ(timer.getNanoseconds(), 500UL);
	}

	TEST(HighResolutionTimerTest, IgnoresMultipleConsecutiveResumeCalls) {
		Mocks::FakeClock::currentNanoseconds = 0UL;
		HighResolutionTimer<Mocks::FakeClock> timer{};

		timer.start();
		Mocks::FakeClock::currentNanoseconds = 100UL;

		timer.stop();
		Mocks::FakeClock::currentNanoseconds = 250UL;

		timer.resume();
		Mocks::FakeClock::currentNanoseconds = 350UL;

		EXPECT_EQ(timer.getNanoseconds(), 200UL);

		timer.resume();
		EXPECT_EQ(timer.getNanoseconds(), 200UL);
	}

	TEST(HighResolutionTimerTest, IgnoresResumeCallWhenNotStarted) {
		Mocks::FakeClock::currentNanoseconds = 5000UL;
		HighResolutionTimer<Mocks::FakeClock> timer{};

		timer.resume();
		EXPECT_EQ(timer.getNanoseconds(), 0UL);

		timer.start();
		Mocks::FakeClock::currentNanoseconds = 7500UL;
		
		EXPECT_EQ(timer.getNanoseconds(), 2500UL);
	}

	TEST(HighResolutionTimerTest, ResetsAfterStarting) {
		Mocks::FakeClock::currentNanoseconds = 50UL;
		HighResolutionTimer<Mocks::FakeClock> timer{};

		timer.start();
		Mocks::FakeClock::currentNanoseconds = 100UL;

		timer.reset();
		EXPECT_EQ(timer.getNanoseconds(), 0UL);

	}

	TEST(HighResolutionTimerTest, IgnoresResumeCallAfterReset) {
		Mocks::FakeClock::currentNanoseconds = 1000UL;
		HighResolutionTimer<Mocks::FakeClock> timer{};

		timer.start();
		Mocks::FakeClock::currentNanoseconds = 2000UL;

		timer.reset();
		Mocks::FakeClock::currentNanoseconds = 2500UL;

		timer.resume();
		Mocks::FakeClock::currentNanoseconds = 4000UL;

		EXPECT_EQ(timer.getNanoseconds(), 0UL);

	}
}

}