#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <sstream>
#include <iostream>

#include "Exceptions.h"
#include "Utility/Logger.h"
#include "Utility/DebugLogger.h"
#include "Utility/Time.h"

#include "Mocks/FakeClock.h"

namespace Uwe {

namespace Utility {

    class TestLogger : public Logger<TestLogger, Mocks::FakeClock> {
    public:
        std::stringstream stream;

        TestLogger(LogLevel level = LogLevel::Debug)
			: Logger<TestLogger, Mocks::FakeClock>{ level }, stream{}
		{
		}

        template <typename T>
        void write(T&& param) {
			stream << param;
        }
    };

	TEST(LoggerTest, PrintsTheCurrentTimestampWhenLogging) {
		Mocks::FakeClock::currentNanoseconds = 0U;

		TestLogger logger{};
		std::string expectedTimestamp{ getTimestamp<Mocks::FakeClock>() };

		logger.log(LogLevel::Debug, "test");

		std::string logOutput{ logger.stream.str() };
		std::string actualTimestamp{ logOutput.substr(0U, 24U) };

		EXPECT_EQ(expectedTimestamp, actualTimestamp);
	}

	TEST(LoggerTest, PrintsTheLogLevelWhenLogging) {
		TestLogger logger{LogLevel::Trace};

		for (unsigned lvl = LogLevel::Trace; lvl <= LogLevel::Fatal; ++lvl) {
			logger.log((LogLevel)lvl, "test");

			std::string logOutput{ logger.stream.str() };
			std::string logLevel{ logOutput.substr(24U, 10U) };

			EXPECT_EQ(LogLevelStrings[lvl], logLevel);

			logger.stream.str("");
		}
	}

	TEST(LoggerTest, DoesNotPrintMessagesBelowTheLogLevel) {
		
		for (int lvl = LogLevel::Debug; lvl <= LogLevel::Fatal; ++lvl) {
			TestLogger logger{(LogLevel) lvl};
			logger.log((LogLevel)(lvl - 1), "should not be logged");

			std::string logOutput{ logger.stream.str() };
			EXPECT_EQ(logOutput.length(), 0U);
		}
	}

	TEST(LoggerTest, PrintsTheActualMessage) {
		TestLogger logger{};

		logger.log(LogLevel::Debug, "test");

		std::string logOutput{ logger.stream.str() };
		std::string actualMessage{ logOutput.substr(34U, 4U) };

		EXPECT_EQ(actualMessage, "test");
	}

	TEST(LoggerTest, AppendsLineFeedToEachLogLine) {
		TestLogger logger{};

		logger.log(LogLevel::Warn, "test");

		std::string logOutput{ logger.stream.str() };
		char last{ logOutput[logOutput.length() - 1]};

		EXPECT_EQ('\n', last);
	}

	TEST(LoggerTest, PrintsAnArbitraryAmountOfMessages) {
		TestLogger logger{};

		logger.log(LogLevel::Info, "info message #", 0, " - ", 0.25f, "%");
		std::string logOutput{ logger.stream.str() };
		std::string actualMessage{ logOutput.substr(34U) };

		EXPECT_EQ("info message #0 - 0.25%\n", actualMessage);
	}

	struct Printable {
		std::string data;

		Printable(const std::string& data)
			: data{ data } {};

		Printable(const Printable& rhs)
			: data{ rhs.data } 
		{
		};

		friend std::ostream& operator<<(std::ostream& outStream, const Printable& printable) {
			return outStream << "Data: " << printable.data;
		};
	};

	TEST(LoggerTest, PrintsObjectsImplementingInsertionOperator) {
		Printable printable{ "test#1" };
		TestLogger logger{};

		logger.log(LogLevel::Info, printable);

		std::string logOutput{ logger.stream.str() };
		std::string actual{ logOutput.substr(34U) };

		EXPECT_EQ(actual, "Data: test#1\n");
	}

	TEST(DebugLoggerTest, LogsToStandardOut) {
		std::stringstream outStream{};
		
		std::streambuf* stdOutBuffer{ std::cout.rdbuf() };
		std::cout.rdbuf(outStream.rdbuf());

		DebugLogger logger{};
		logger.log(LogLevel::Info, "Hello world!");

		std::cout.rdbuf(stdOutBuffer);

		std::string logOutput{ outStream.str() };
		std::string actual{ logOutput.substr(34U) };

		EXPECT_EQ(actual, "Hello world!\n");
	}
}

}