#include "gtest/gtest.h"
#include "gmock/gmock.h"

int main(int argc, char* argv[]) {
	::testing::InitGoogleMock(&argc, argv);
	//::testing::FLAGS_gtest_death_test_style = "fast";

	return RUN_ALL_TESTS();
};