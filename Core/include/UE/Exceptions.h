#pragma once

#include <stdexcept>

namespace Uwe {
	
	constexpr const char* unspecified{ "Unspecified exception" };

	struct UweException : public std::runtime_error {
		UweException()
			: std::runtime_error{ unspecified } {};

		UweException(const std::string& msg)
			: std::runtime_error{ msg } {};
	};

	struct OutOfBoundsException : public UweException {
		OutOfBoundsException()
			: UweException{} {};

		OutOfBoundsException(const std::string& msg)
			: UweException{ msg } {};
	};

}