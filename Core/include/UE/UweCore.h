﻿#pragma once

#include <string>

namespace Uwe {

	void sanityCheck(void);
	std::string getVersion(void);

}
