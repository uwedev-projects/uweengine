#pragma once

#include <string>

#include "Exceptions.h"

namespace Uwe {

namespace IO {

    template <typename StreamType>
    class Stream {
    private:
        StreamType& implementation() noexcept;
    public:
        void open();
		void close();

        template <typename First, typename... Args>
		void write(First first, Args... args);

        template <typename T>
        StreamType& operator<<(T param);
    };

    template <typename StreamType>
    StreamType& Stream<StreamType>::implementation() noexcept {
        return *static_cast<StreamType*>(this);
    }

    template <typename StreamType>
    void Stream<StreamType>::open() {
        return implementation().open();
    }

    template <typename StreamType>
	void Stream<StreamType>::close() {
        return implementation().close();
    }

    template <typename StreamType>
        template <typename First, typename... Args>
	void Stream<StreamType>::write(First first, Args... args) {
        return implementation.write(first, args);
    }

    template <typename StreamType>
        template <typename T>
    StreamType& Stream<StreamType>::operator<<(T param) {
        implementation().write(param);
        return implementation();
    }

}

}