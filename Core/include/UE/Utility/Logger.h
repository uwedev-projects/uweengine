#pragma once

#include "Utility/Time.h"

#include <chrono>
#include <string>
#include <utility>

namespace Uwe {

namespace Utility {
    
    enum LogLevel : std::size_t { 
		Trace,
        Debug,
        Info,
        Warn,
        Error,
        Fatal
     };

     const std::string LogLevelStrings[] {
		 " [TRACE]: ",
         " [DEBUG]: ",
         " [INFO ]: ",
         " [WARN ]: ",
         " [ERROR]: ",
         " [FATAL]: "
     };

    template <typename LoggerType, typename Clock = std::chrono::system_clock>
    class Logger {
    protected:
		LogLevel level;

        LoggerType& implementation() noexcept;

		template <typename First, typename... Args>
		void write(First&& first, Args&&... args);

		template <typename T>
		void write(T&& param);

    public: 
        Logger(LogLevel level = LogLevel::Debug);

        template <typename First, typename... Args>
        void log(LogLevel lvl, First&& first, Args&&... args);
    };

    template <typename LoggerType, typename Clock>
    Logger<LoggerType, Clock>::Logger(LogLevel level)
		: level{level}
    {
    }

    template <typename LoggerType, typename Clock>
    LoggerType& Logger<LoggerType, Clock>::implementation() noexcept {
        return *static_cast<LoggerType*>(this);
    }

    template <typename LoggerType, typename Clock>
        template <typename First, typename... Args>
	void Logger<LoggerType, Clock>::log(LogLevel lvl, First&& first, Args&&... args) {
		if (level <= lvl) {
			write(getTimestamp<Clock>());
			write(LogLevelStrings[lvl]);
			write(std::forward<First>(first), std::forward<Args>(args)...);
			write('\n');
		}
    }

	template <typename LoggerType, typename Clock>
		template <typename First, typename... Args>
	void Logger<LoggerType, Clock>::write(First&& first, Args&&... args) {
		write(std::forward<First>(first));
		write(std::forward<Args>(args)...);
	}

	template <typename LoggerType, typename Clock>
		template <typename T>
	void Logger<LoggerType, Clock>::write(T&& param) {
		implementation().write(std::forward<T>(param));
	}

}
    
}