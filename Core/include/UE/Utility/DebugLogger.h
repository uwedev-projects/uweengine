#pragma once

#include "Utility/Logger.h"
#include <iostream>
#include <utility>

namespace Uwe {

namespace Utility {

	// Unsynchronized debug logger
	class DebugLogger : public Logger<DebugLogger> {
	public:
		DebugLogger();

		template <typename T>
		void write(T&& streamable);
	};

	template <typename T>
	void DebugLogger::write(T&& streamable) {
		std::cout << std::forward<T>(streamable);
	}

	DebugLogger& loggerInstance() noexcept;

	template <typename First, typename... Args>
	void logDebug(First&& first, Args&&... args) {
		loggerInstance().log(
			LogLevel::Debug, 
			std::forward<First>(first),
			std::forward<Args>(args)...
		);
	}

	template <typename First, typename... Args>
	void logInfo(First&& first, Args&&... args) {
		loggerInstance().log(
			LogLevel::Info, 
			std::forward<First>(first), 
			std::forward<Args>(args)...
		);
	}

	template <typename First, typename... Args>
	void logWarn(First&& first, Args&&... args) {
		loggerInstance().log(
			LogLevel::Warn, 
			std::forward<First>(first), 
			std::forward<Args>(args)...
		);
	}

	template <typename First, typename... Args>
	void logError(First&& first, Args&&... args) {
		loggerInstance().log(
			LogLevel::Error, 
			std::forward<First>(first), 
			std::forward<Args>(args)...
		);
	}

	template <typename First, typename... Args>
	void logFatal(First&& first, Args&&... args) {
		loggerInstance().log(
			LogLevel::Fatal, 
			std::forward<First>(first), 
			std::forward<Args>(args)...
		);
	}
}

}