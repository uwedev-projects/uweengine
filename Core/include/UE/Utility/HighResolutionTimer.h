#pragma once

#include <chrono>
#include <ratio>

namespace Uwe {

namespace Utility {

	// Type aliases for convenience

	template <typename Rep, typename T>
	using Duration		= std::chrono::duration<Rep, T>;

	using Hours			= Duration<float, std::ratio<60 * 60>>;
	using Minutes		= Duration<float, std::ratio<60>>;
	using Seconds		= Duration<float, std::ratio<1>>;
	using Milliseconds	= Duration<unsigned long, std::milli>;
	using Microseconds	= Duration<unsigned long, std::micro>;
	using Nanoseconds	= Duration<unsigned long, std::nano>;

	template <typename ClockType = std::chrono::high_resolution_clock>
	class HighResolutionTimer {
	protected:
		using TimePoint = typename ClockType::time_point;

		TimePoint startTime, pauseStartTime;
		bool isRunning;
		bool isPaused;
	public:
		HighResolutionTimer();
		~HighResolutionTimer();

		void start();
		void stop();
		void resume();
		void reset();

		float getHours() const;
		float getMinutes() const;
		float getSeconds() const;
		unsigned long getMilliseconds() const;
		unsigned long getMicroseconds() const;
		unsigned long getNanoseconds() const;

	};

	template <typename ClockType>
	HighResolutionTimer<ClockType>::HighResolutionTimer()
		: startTime{}, pauseStartTime{}, isRunning{}, isPaused{}
	{

	}

	template <typename ClockType>
	HighResolutionTimer<ClockType>::~HighResolutionTimer() {

	}

	template <typename ClockType>
	void HighResolutionTimer<ClockType>::start() {
		if (!isRunning) {
			isRunning = true;
			isPaused  = false;
			startTime = ClockType::now();
		}
	}

	template <typename ClockType>
	void HighResolutionTimer<ClockType>::stop() {
		if (isRunning && !isPaused) {
			isPaused  = true;
			pauseStartTime = ClockType::now();
		}
	}

	template <typename ClockType>
	void HighResolutionTimer<ClockType>::reset() {
		isRunning = false;
		isPaused  = false;
		startTime = ClockType::time_point{};
	}

	template <typename ClockType>
	void HighResolutionTimer<ClockType>::resume() {
		if (isPaused) {
			isPaused = false;
			startTime += (ClockType::now() - pauseStartTime);
		}
	}

	template <typename ClockType>
	float HighResolutionTimer<ClockType>::getHours() const {
		auto duration{ isPaused || !isRunning ?
			std::chrono::duration_cast<Hours>(pauseStartTime - startTime) :
			std::chrono::duration_cast<Hours>(ClockType::now() - startTime)
			
		};
		return duration.count();
	}

	template <typename ClockType>
	float HighResolutionTimer<ClockType>::getMinutes() const {
		auto duration{ isPaused || !isRunning ?
			std::chrono::duration_cast<Minutes>(pauseStartTime - startTime) :
			std::chrono::duration_cast<Minutes>(ClockType::now() - startTime)
		};
		return duration.count();
	}

	template <typename ClockType>
	float HighResolutionTimer<ClockType>::getSeconds() const {
		auto duration{ isPaused || !isRunning ?
			std::chrono::duration_cast<Seconds>(pauseStartTime - startTime) :
			std::chrono::duration_cast<Seconds>(ClockType::now() - startTime)
		};
		return duration.count();
	}

	template <typename ClockType>
	unsigned long HighResolutionTimer<ClockType>::getMilliseconds() const {
		auto duration{ isPaused || !isRunning ?
			std::chrono::duration_cast<Milliseconds>(pauseStartTime - startTime) :
			std::chrono::duration_cast<Milliseconds>(ClockType::now() - startTime)
		};
		return duration.count();
	}

	template <typename ClockType>
	unsigned long HighResolutionTimer<ClockType>::getMicroseconds() const {
		auto duration{ isPaused || !isRunning ?
			std::chrono::duration_cast<Microseconds>(pauseStartTime - startTime) :
			std::chrono::duration_cast<Microseconds>(ClockType::now() - startTime)
		};
		return duration.count();
	}

	template <typename ClockType>
	unsigned long HighResolutionTimer<ClockType>::getNanoseconds() const {
		auto duration{ isPaused || !isRunning ?
			std::chrono::duration_cast<Nanoseconds>(pauseStartTime - startTime) :
			std::chrono::duration_cast<Nanoseconds>(ClockType::now() - startTime)
		};
		return duration.count();
	}

}

}