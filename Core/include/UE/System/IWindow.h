#pragma once

#include <string>

namespace Uwe {

namespace System {

	enum class WindowMode : unsigned char { Windowed, Fullscreen, WindowedFullscreen };

	struct WindowConfig {
		std::string title;
		short width;
		short height;
		bool isVisible;
		WindowMode mode;
	};

	template <class Impl>
	class IWindow {
	private:
		Impl& implementation() noexcept;
	public:
		void setTitle(const std::string& title);
		const std::string& getTitle() const noexcept;

		void setVisibility(bool isVisible);
		void toggleVisibility();
		bool isVisible() const noexcept;

		void setHeight(short height);
		short getHeight() const noexcept; 

		void setWidth(short width);
		short getWidth() const noexcept;

		void setMode(WindowMode mode);
		WindowMode getMode() const noexcept;
	};

	template <class Impl>
	inline Impl& IWindow<Impl>::implementation() noexcept {
		return *static_cast<Impl*>(this);
	}

	template <class Impl>
	inline void IWindow<Impl>::setTitle(const std::string& title) {
		implementation().setTitle(title);
	}

	template <class Impl>
	inline const std::string& IWindow<Impl>::getTitle() const noexcept {
		return implementation().getTitle();
	}

	template <class Impl>
	inline void IWindow<Impl>::setWidth(short width) {
		implementation().setWidth(width);
	}

	template <class Impl>
	inline short IWindow<Impl>::getWidth() const noexcept {
		return implementation().getWidth();
	}

	template <class Impl>
	inline void IWindow<Impl>::setHeight(short height) {
		implementation().setHeight(height);
	}

	template <class Impl>
	inline short IWindow<Impl>::getHeight() const noexcept {
		return implementation().getHeight();
	}

	template <class Impl>
	inline void IWindow<Impl>::setMode(WindowMode mode) {
		implementation().setMode(mode);
	}

	template <class Impl>
	inline WindowMode IWindow<Impl>::getMode() const noexcept {
		return implementation().getMode();
	}

	template <class Impl>
	inline void IWindow<Impl>::setVisibility(bool isVisible) {
		implementation().setVisibility(isVisible);
	}

	template <class Impl>
	inline void IWindow<Impl>::toggleVisibility() {
		implementation().toggleVisibility();
	}

	template <class Impl>
	inline bool IWindow<Impl>::isVisible() const noexcept {
		return implementation().isVisible();
	}

}

}