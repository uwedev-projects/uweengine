#pragma once

#include "System/IWindow.h"

namespace Uwe {

namespace System {

    

    class Window {
    public:
        Window(
            const char* title, 
            unsigned short width, 
            unsigned short height, 
            WindowMode mode = WindowMode::Windowed
        );

        ~Window();

        Window(const Window&) = delete;
        Window& operator=(const Window&) = delete;

        Window(Window&& window);

    };

}

}