#pragma once

#include "UE/Utility/HighResolutionTimer.h"
#include "UE/Memory/Block.h"

using Uwe::Utility::HighResolutionTimer;
using Uwe::Memory::Block;

class CoreLibUser {
	HighResolutionTimer<> hrt;
	Block block;

public:
	CoreLibUser()
		: hrt{}, block{}
	{

	}

};