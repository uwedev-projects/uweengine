cmake_minimum_required (VERSION 3.12)

target_sources (Demo
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/main.cpp
)

# add further subdirectories here ..