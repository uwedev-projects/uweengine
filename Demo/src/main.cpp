#include "UE/UweCore.h"
#include "UE/Utility/HighResolutionTimer.h"
#include "CoreLibUser.h"

#include <iostream>
#include <string>

int main()
{
	std::cout << "Using UweCore lib version: " << Uwe::getVersion() << "\n";
	std::cout << "Running sanity check .. \n";

	Uwe::sanityCheck();

	std::cout << "Sizeof void*: " << std::to_string(sizeof(void*)) << "\n";
	std::cout << "Sizeof long: " << std::to_string(sizeof(long)) << "\n";
	std::cout << "Sizeof long long: " << std::to_string(sizeof(long long)) << "\n";
	std::cout << "Sizeof int: " << std::to_string(sizeof(int)) << "\n";
	std::cout << "Sizeof unsigned: " << std::to_string(sizeof(unsigned)) << "\n";

	CoreLibUser user{};

	Uwe::Utility::HighResolutionTimer hrt{};
	char c{};
	std::cin >> c;
}