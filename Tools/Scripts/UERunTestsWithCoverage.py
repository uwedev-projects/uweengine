import os
import re
import glob
import subprocess

UELibs = [ @UELibs@ ]
BaseDirUnix = "@CMAKE_CURRENT_BINARY_DIR@"
BaseDir = BaseDirUnix.replace("/", "\\")

print ("Running tests with coverage collection ..")
os.chdir(f"{BaseDir}\\..\\..")

for lib in UELibs:
	print (f"Running {lib} testsuite ..")
	cmd = [	"OpenCppCoverage", "--sources", lib, "--excluded_sources", "glfw", "--excluded_sources", "benchmarks", 
			"--excluded_sources", "glad", "--excluded_sources", "glm", "--excluded_sources", "KHR",
			"--quiet", f"--export_type=binary:CoverageData\\{lib}Coverage.bin", "--", f".\\{lib}\\test\\Uwe{lib}Test.exe"
	]

	print (cmd)
	proc = subprocess.run(cmd)

print ("Collecting test coverage results ..")

CoverageFiles = []

CoverageFiles = glob.glob(f"CoverageData\\*Coverage*.bin")

if (len(CoverageFiles) > 0):
	CoverageFileArgs = []
	
	with open('opencppcoverage_config.txt', 'w') as cmdConfigFile:
		for covFile in CoverageFiles:
			CoverageFileArgs.append(f" --input_coverage={covFile}")
			cmdConfigFile.write(f"input_coverage={covFile}\n")



	cmd = ["OpenCppCoverage", "--quiet", "--export_type=html:coverage", "--sources", "UweEngine", "--config_file", "opencppcoverage_config.txt"]
	proc = subprocess.run(cmd)

	cmd = ["OpenCppCoverage", "--quiet", "--export_type=cobertura:coverage.xml", "--sources", "UweEngine", "--config_file", "opencppcoverage_config.txt"]
	proc = subprocess.run(cmd)

	coverageResult = "Coverage: unknown"
	with open('coverage\\index.html', 'r') as resultFile:
		for word in resultFile:
			match = re.search("Cover (\d+%)", word)
			if match:
				coverageResult = "Coverage: " + match.group(1)
				break


	print(coverageResult)