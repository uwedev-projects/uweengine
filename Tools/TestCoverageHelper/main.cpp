#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <memory>
#include <numeric>
#include <regex>
#include <string>
#include <vector>


void createEmptyFile(std::string const& path) {
    std::ofstream ofs(path);
    ofs << '\n';
}

const std::string separator = "--sep--";
const std::string LogfilePrefix = "--log-file=";

bool startsWith(std::string const& str, std::string const& pref) {
    return str.find(pref) == 0;
}

int parseLogFileArg(std::string const& arg) {
    assert(startsWith(arg, LogfilePrefix) && "Attempting to parse incorrect arg!");
    auto fileName = arg.substr(LogfilePrefix.size());
    createEmptyFile(fileName);
    std::regex regex("MemoryChecker\\.(\\d+)\\.log", std::regex::icase);
    std::smatch match;
    if (std::regex_search(fileName, match, regex)) {
        return std::stoi(match[1]);
    }
    else {
        throw std::domain_error("Couldn't find desired expression in string: " + fileName);
    }
}

std::string windowsifyPath(std::string path) {
    for (auto& c : path) {
        if (c == '/') {
            c = '\\';
        }
    }
    return path;
}

void execCmd(std::string const& cmd, int logID, std::string const& path) {
    std::array<char, 128> buffer{};
    auto realCmd = "OpenCppCoverage --export_type binary:CoverageReports\\cov-report" + std::to_string(logID)
        + ".bin --quiet " + "--sources " + path + " --cover_children -- " + cmd;
    std::cout << "=== Marker ===: Cmd: " << realCmd << '\n';
    std::shared_ptr<FILE> pipe(_popen(realCmd.c_str(), "r"), _pclose);

    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 128, pipe.get()) != nullptr) {
            std::cout << buffer.data();
        }
    }
}

// argv should be:
// [0]: our path
// [1]: "--log-file=<path>"
// [2]: "--sep--"
// [3]+: the actual command

//"--log-file=F:/Dev/UweEngine/CMakeBuilds/builds/x64-Debug/Testing/Temporary/MemoryChecker.1.log" "--sep--" "F:/Dev/UweEngine/CMakeBuilds/builds/x64-Debug/Platform/test/UwePlatformTest.exe" "--gtest_filter=Bitmask128Test.ComparesEqualIfLoAndHiWordsAreEqual" "--gtest_also_run_disabled_tests"

int main(int argc, char** argv) {
    std::vector<std::string> args(argv, argv + argc);

    auto sep = std::find(begin(args), end(args), separator);
    assert(sep - begin(args) == 2 && "Structure differs from expected!");

    auto num = parseLogFileArg(args[1]);

    auto cmdline = std::accumulate(++sep, end(args), std::string{}, [](const std::string& lhs, const std::string& rhs) {
        return lhs + ' ' + rhs;
    });

    const auto winCmd = windowsifyPath(args[0]);
    execCmd(cmdline, num, winCmd);
}