#include "UE/Platform/BitScan.h"

namespace Uwe {

namespace Platform {

	unsigned countConsecutiveSetBits(unsigned long long mask) {
		unsigned count{};

		while (mask > 0) {
			mask &= (mask << 1ULL);
			++count;
		}

		return count;
	}

}

}