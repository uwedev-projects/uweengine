#pragma once

// Targeting windows 7 and up
#define _WIN32_WINNT 0x0601

#include <sdkddkver.h>

#define WIN32_LEAN_AND_MEAN

#define NOCOMM
#define NORPC
#define NOHELP
#define NOATOM
#define NOSCROLL
#define NOSYSMETRICS
#define NOKERNEL
#define NODRAWTEXT
#define NOGDICAPMASKS  
#define NODEFERWINDOWPOS
#define NOMINMAX         
#define NOCTLMGR          
#define NOMENUS
#define NOICONS
#define NORASTEROPS
#define OEMRESOURCE       
#define NONLS
#define NOMEMMGR
#define NOMETAFILE
#define NOWH
#define NOSERVICE 
#define NOSOUND
#define NOMCX
#define NOIMAGE
#define NOTEXTMETRIC
#define NOOPENFILE
#define NOTAPE
#define NOPROXYSTUB
#define NOKANJI

#define STRICT

#include <Windows.h>