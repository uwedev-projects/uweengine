#include "UE/Platform/Bitmask.h"
#include <bitset>

namespace Uwe {

namespace Platform {

	std::ostream& operator<<(std::ostream& outStream, const Bitmask128& mask) {
		std::bitset<64> loBits{ mask.lo }, hiBits{ mask.hi };
		return outStream
			<< hiBits.to_string() << " " << loBits.to_string();
	}

#	if (!defined UWE_ARCH_X64 && defined UWE_ARCH_X86 && defined UWE_COMPILER_MSVC)

	bool Bitmask128::findFirstSetBit(unsigned long& index) const noexcept {
		bool bitFound{};
		unsigned long offset{};
		uint32_t splitWord;

		std::memcpy(&splitWord, &lo, sizeof(uint32_t));
		bitFound = static_cast<bool>(_BitScanForward(&index, splitWord));

		if (!bitFound) {
			std::memcpy(&splitWord, reinterpret_cast<const char*>(&lo) + sizeof(uint32_t), sizeof(uint32_t));
			bitFound = static_cast<bool>(_BitScanForward(&index, splitWord));
			offset += sizeof(uint32_t) * CHAR_BIT;
		}

		if (!bitFound) {
			std::memcpy(&splitWord, &hi, sizeof(uint32_t));
			bitFound = static_cast<bool>(_BitScanForward(&index, splitWord));
			offset += sizeof(uint32_t) * CHAR_BIT;
		}

		if (!bitFound) {
			std::memcpy(&splitWord, reinterpret_cast<const char*>(&hi) + sizeof(uint32_t), sizeof(uint32_t));
			bitFound = static_cast<bool>(_BitScanForward(&index, splitWord));
			offset += sizeof(uint32_t) * CHAR_BIT;
		}
		
		index += bitFound * offset;

		return bitFound;
	}

	bool Bitmask128::findLastSetBit(unsigned long& index) const noexcept {
		bool bitFound{};
		unsigned long offset{ 3U * sizeof(uint32_t) * CHAR_BIT };
		uint32_t splitWord;

		std::memcpy(&splitWord, reinterpret_cast<const char*>(&hi) + sizeof(uint32_t), sizeof(uint32_t));
		bitFound = static_cast<bool>(_BitScanReverse(&index, splitWord));

		if (!bitFound) {
			std::memcpy(&splitWord, &hi, sizeof(uint32_t));
			bitFound = static_cast<bool>(_BitScanReverse(&index, splitWord));
			offset -= sizeof(uint32_t) * CHAR_BIT;
		}

		if (!bitFound) {
			std::memcpy(&splitWord, reinterpret_cast<const char*>(&lo) + sizeof(uint32_t), sizeof(uint32_t));
			bitFound = static_cast<bool>(_BitScanReverse(&index, splitWord));
			offset -= sizeof(uint32_t) * CHAR_BIT;
		}

		if (!bitFound) {
			std::memcpy(&splitWord, &lo, sizeof(uint32_t));
			bitFound = static_cast<bool>(_BitScanReverse(&index, splitWord));
			offset -= sizeof(uint32_t) * CHAR_BIT;
		}
		else {
			index += offset;
		}

		return bitFound;
	}

#	endif

}

}