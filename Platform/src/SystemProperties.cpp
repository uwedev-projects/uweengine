#include "UE/Platform/SystemProperties.h"

#include "UE/EnginePlatform.h"

#include <array>
#include <vector>
#include <memory>
#include <cmath>
#include <cstddef>

#include <intrin.h>

#if defined UWE_COMPILER_MSVC

#	include "UweWindows.h"

#	if defined UWE_ARCH_X86

#		pragma intrinsic(__cpuid)

#	endif

#endif

namespace Uwe {

namespace Platform {

	SystemProperties::SystemProperties()
		: cachePropsL1D{}, extensions {}
	{
		queryISAExtensions();
		gatherCacheProperties();
	}

#	if defined UWE_COMPILER_MSVC && defined UWE_ARCH_X86
	
	void SystemProperties::queryISAExtensions() {
		std::array<int, 4U> registers{};

		__cpuid(registers.data(), 0);
		int maxId{ registers[0] };

		std::vector<std::array<int, 4U>> recordedRegisters{};

		for (int id{}; id <= maxId; ++id) {
			__cpuidex(registers.data(), id, 0);
			recordedRegisters.push_back(registers);
		}

		std::array<char, sizeof(int) * 4U> vendorName{ 0 };
		*reinterpret_cast<int*>(vendorName.data())		= recordedRegisters[0][1];
		*reinterpret_cast<int*>(vendorName.data() + 4)	= recordedRegisters[0][3];
		*reinterpret_cast<int*>(vendorName.data() + 8)	= recordedRegisters[0][2];

		vendorString = vendorName.data();

		if (vendorString == "GenuineIntel") {
			vendor = Vendor::Intel;
		}
		else if (vendorString == "AuthenticAMD") {
			vendor = Vendor::AMD;
		}
		else {
			vendor = Vendor::Unknown;
		}

		using Bitset = std::bitset<sizeof(int) * CHAR_BIT>;
		if (maxId >= 1) {
			Bitset tempRegister{ static_cast<unsigned long long>(recordedRegisters[1][2]) };

			extensions[static_cast<std::size_t>(ISAExtension::SSE3)]		= tempRegister[0];
			extensions[static_cast<std::size_t>(ISAExtension::PCLMULQDQ)]	= tempRegister[1];
			extensions[static_cast<std::size_t>(ISAExtension::MONITOR)]		= tempRegister[3];
			extensions[static_cast<std::size_t>(ISAExtension::SSSE3)]		= tempRegister[9];
			extensions[static_cast<std::size_t>(ISAExtension::FMA)]			= tempRegister[12];
			extensions[static_cast<std::size_t>(ISAExtension::CMPXCHG16B)]	= tempRegister[13];
			extensions[static_cast<std::size_t>(ISAExtension::SSE41)]		= tempRegister[19];
			extensions[static_cast<std::size_t>(ISAExtension::SSE42)]		= tempRegister[20];

			extensions[static_cast<std::size_t>(ISAExtension::MOVBE)]		= tempRegister[22];
			extensions[static_cast<std::size_t>(ISAExtension::POPCNT)]		= tempRegister[23];
			extensions[static_cast<std::size_t>(ISAExtension::AES)]			= tempRegister[25];
			extensions[static_cast<std::size_t>(ISAExtension::XSAVE)]		= tempRegister[26];
			extensions[static_cast<std::size_t>(ISAExtension::OSXSAVE)]		= tempRegister[27];
			extensions[static_cast<std::size_t>(ISAExtension::AVX)]			= tempRegister[28];
			extensions[static_cast<std::size_t>(ISAExtension::F16C)]		= tempRegister[29];
			extensions[static_cast<std::size_t>(ISAExtension::RDRAND)]		= tempRegister[30];

			tempRegister = { static_cast<unsigned long long>(recordedRegisters[1][3]) };

			extensions[static_cast<std::size_t>(ISAExtension::MSR)]			= tempRegister[5];
			extensions[static_cast<std::size_t>(ISAExtension::CX8)]			= tempRegister[8];
			extensions[static_cast<std::size_t>(ISAExtension::SEP)]			= tempRegister[11];
			extensions[static_cast<std::size_t>(ISAExtension::CMOV)]		= tempRegister[15];
			extensions[static_cast<std::size_t>(ISAExtension::CLFSH)]		= tempRegister[19];
			extensions[static_cast<std::size_t>(ISAExtension::MMX)]			= tempRegister[23];
			extensions[static_cast<std::size_t>(ISAExtension::FXSR)]		= tempRegister[24];
			extensions[static_cast<std::size_t>(ISAExtension::SSE)]			= tempRegister[25];
			extensions[static_cast<std::size_t>(ISAExtension::SSE2)]		= tempRegister[26];
		}

		if (maxId >= 7) {
			Bitset tempRegister{ static_cast<unsigned long long>(recordedRegisters[7][1]) };

			extensions[static_cast<std::size_t>(ISAExtension::FSGSBASE)]	= tempRegister[0];
			extensions[static_cast<std::size_t>(ISAExtension::BMI1)]		= tempRegister[3];
			extensions[static_cast<std::size_t>(ISAExtension::HLE)]			= vendor == Vendor::Intel && tempRegister[4];
			extensions[static_cast<std::size_t>(ISAExtension::AVX2)]		= tempRegister[5];
			extensions[static_cast<std::size_t>(ISAExtension::BMI2)]		= tempRegister[8];
			extensions[static_cast<std::size_t>(ISAExtension::ERMS)]		= tempRegister[9];
			extensions[static_cast<std::size_t>(ISAExtension::INVPCID)]		= tempRegister[10];
			extensions[static_cast<std::size_t>(ISAExtension::RTM)]			= vendor == Vendor::Intel && tempRegister[11];

			extensions[static_cast<std::size_t>(ISAExtension::AVX512F)]		= tempRegister[16];
			extensions[static_cast<std::size_t>(ISAExtension::RDSEED)]		= tempRegister[18];
			extensions[static_cast<std::size_t>(ISAExtension::ADX)]			= tempRegister[19];
			extensions[static_cast<std::size_t>(ISAExtension::AVX512PF)]	= tempRegister[26];
			extensions[static_cast<std::size_t>(ISAExtension::AVX512ER)]	= tempRegister[27];
			extensions[static_cast<std::size_t>(ISAExtension::AVX512CD)]	= tempRegister[28];
			extensions[static_cast<std::size_t>(ISAExtension::SHA)]			= tempRegister[29];

			tempRegister = static_cast<unsigned long long>(recordedRegisters[7][2]);

			extensions[static_cast<std::size_t>(ISAExtension::PREFETCHWT1)] = tempRegister[0];
		}

		registers = {};
		__cpuid(registers.data(), 0x80000000);
		int maxExtensionId{ registers[0] };

		recordedRegisters = {};
		for (int id{ static_cast<int>(0x80000000) }; id <= maxExtensionId; ++id) {
			__cpuidex(registers.data(), id, 0);
			recordedRegisters.push_back(registers);
		}

		if (maxExtensionId >= 0x80000001) {
			Bitset tempRegister{ static_cast<unsigned long long>(recordedRegisters[1][2]) };

			extensions[static_cast<std::size_t>(ISAExtension::LAHF)]		= tempRegister[0];
			extensions[static_cast<std::size_t>(ISAExtension::LZCNT)]		= vendor == Vendor::Intel && tempRegister[5];
			extensions[static_cast<std::size_t>(ISAExtension::ABM)]			= vendor == Vendor::AMD && tempRegister[5];
			extensions[static_cast<std::size_t>(ISAExtension::SSE4a)]		= vendor == Vendor::AMD && tempRegister[6];
			extensions[static_cast<std::size_t>(ISAExtension::XOP)]			= vendor == Vendor::AMD && tempRegister[11];
			extensions[static_cast<std::size_t>(ISAExtension::TBM)]			= vendor == Vendor::AMD && tempRegister[21];

			tempRegister = static_cast<unsigned long long>(recordedRegisters[1][3]);

			extensions[static_cast<std::size_t>(ISAExtension::SYSCALL)]		= vendor == Vendor::Intel && tempRegister[11];
			extensions[static_cast<std::size_t>(ISAExtension::MMXEXT)]		= vendor == Vendor::AMD && tempRegister[22];
			extensions[static_cast<std::size_t>(ISAExtension::RDTSCP)]		= vendor == Vendor::Intel && tempRegister[27];
			extensions[static_cast<std::size_t>(ISAExtension::_3DNOWEXT)]	= vendor == Vendor::AMD && tempRegister[30];
			extensions[static_cast<std::size_t>(ISAExtension::_3DNOW)]		= vendor == Vendor::AMD && tempRegister[31];
		}

		if (maxExtensionId >= 0x80000004) {
			std::array<char, sizeof(int) * 16U> brandName{ 0 };
			std::memcpy(brandName.data(), recordedRegisters[2].data(), sizeof(registers));
			std::memcpy(brandName.data() + 16U, recordedRegisters[3].data(), sizeof(registers));
			std::memcpy(brandName.data() + 32U, recordedRegisters[4].data(), sizeof(registers));

			brandString = brandName.data();
		}
	}

#	endif	// MSVC x86

#	if defined UWE_COMPILER_MSVC

	void SystemProperties::gatherCacheProperties() {
		using CPUInfo = SYSTEM_LOGICAL_PROCESSOR_INFORMATION_EX;

		DWORD entryBufferLength{};
		constexpr const auto infoType = LOGICAL_PROCESSOR_RELATIONSHIP::RelationCache;

		// Get the necessary length in bytes
		GetLogicalProcessorInformationEx(infoType, nullptr, &entryBufferLength);

		BOOL success{ TRUE };
		const auto numEntries = static_cast<int>(
			std::ceil(static_cast<double>(entryBufferLength) / sizeof(CPUInfo))
		);

		auto cacheEntries = std::make_unique<CPUInfo[]>(numEntries);
		success = GetLogicalProcessorInformationEx(infoType, cacheEntries.get(), &entryBufferLength);
		
		if (success) {
			const CPUInfo* current{ cacheEntries.get() };
			for (int i{}; i < numEntries; ++i, ++current) {
				const auto cacheNode = current->Cache;

				if (cacheNode.Type == CacheData && cacheNode.Level == 1) {
					cachePropsL1D = {
						static_cast<CacheLevel>(cacheNode.Level),
						cacheNode.CacheSize,
						cacheNode.LineSize,
						cacheNode.Associativity
					};
				}

			}
		}

	}

	const SystemProperties& SystemProperties::getGlobalSystemProperties() {
		const static SystemProperties instance{};
		return instance;
	}

#	endif // MSVC

}

}