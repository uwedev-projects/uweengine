#ifndef _UWE_ENGINE_PLATFORM_H_
#define _UWE_ENGINE_PLATFORM_H_

// OS Config

#if defined _WIN32 || defined _WIN64

#	define UWE_OS_WINDOWS

#endif

// Compiler Config

#if defined _MSC_VER

#	define UWE_COMPILER_MSVC

#endif

// Architecture Config

#define UWE_ENDIAN			0
#if UWE_ENDIAN

#	define UWE_BIG_ENDIAN

#else

#	define UWE_LITTLE_ENDIAN

#endif

#define UWE_ARCH_X86_BIT	0x00000001
#define UWE_ARCH_X64_BIT	0x00000002
#define UWE_ARCH_SSE2_BIT	0x00000004

#if defined _M_X64 

#	define UWE_ARCH (UWE_ARCH_X64_BIT | UWE_ARCH_SSE2_BIT)
#	define UWE_ARCH_X64
#	define UWE_ARCH_X86
#	define UWE_ARCH_SSE2

#elif defined _M_IX86

#	define UWE_ARCH (UWE_ARCH_X86_BIT)
#	define UWE_ARCH_X86

#elif defined _M_IX86_FP

#	if _M_IX86_FP >= 2
#		define UWE_ARCH (UWE_ARCH_X86_BIT | UWE_ARCH_SSE2_BIT)
#	else
#		define UWE_ARCH (UWE_ARCH_X86_BIT)
#	endif

#endif

#endif
