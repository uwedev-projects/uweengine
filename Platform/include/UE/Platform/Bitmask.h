#pragma once

#include "UE/EngineConfig.h"
#include "UE/EnginePlatform.h"

#include <array>
#include <limits>
#include <cstdint>
#include <ostream>
#include <type_traits>

#ifdef UWE_ARCH_SSE2
	
#	include <emmintrin.h>
#	pragma intrinsic(_mm_and_si128, _mm_or_si128, _mm_xor_si128, _mm_andnot_si128)

#endif

#ifdef UWE_ARCH_X86

#	include <intrin.h>
#	pragma intrinsic(_BitScanForward, _BitScanReverse)

#endif

#ifdef UWE_ARCH_X64

#	pragma intrinsic(_BitScanForward64, _BitScanReverse64)

#endif

namespace Uwe {

namespace Platform {

	struct Bitmask128 {

#		ifdef UWE_LITTLE_ENDIAN

		uint64_t lo;
		uint64_t hi;

#		else

		uint64_t hi;
		uint64_t lo;

#		endif

		constexpr Bitmask128(Bitmask128&& src) noexcept;
		constexpr Bitmask128& operator=(Bitmask128&& rhs) noexcept;

		constexpr Bitmask128(uint64_t hi, uint64_t lo) noexcept;
		explicit constexpr Bitmask128(uint64_t lo) noexcept;

		constexpr Bitmask128() noexcept;

		constexpr Bitmask128(const Bitmask128& src) noexcept;
		constexpr Bitmask128& operator=(const Bitmask128& rhs) noexcept;

		constexpr static Bitmask128 min() noexcept;
		constexpr static Bitmask128 max() noexcept;

		constexpr bool operator==(const Bitmask128& rhs) const noexcept;
		constexpr bool operator!=(const Bitmask128& rhs) const noexcept;

		constexpr Bitmask128 operator&(const Bitmask128& rhs) const noexcept;
		constexpr Bitmask128 operator|(const Bitmask128& rhs) const noexcept;
		constexpr Bitmask128 operator^(const Bitmask128& rhs) const noexcept;
		constexpr Bitmask128 andNot(const Bitmask128& rhs) const noexcept;

		constexpr Bitmask128 operator+(const Bitmask128& rhs) const noexcept;
		constexpr Bitmask128 operator-(const Bitmask128& rhs) const noexcept;
		constexpr Bitmask128 operator+(const uint64_t lo) const noexcept;
		constexpr Bitmask128 operator-(const uint64_t lo) const noexcept;

		Bitmask128& operator++() noexcept;
		Bitmask128& operator--() noexcept;
		Bitmask128& operator+=(const Bitmask128& rhs) noexcept;
		Bitmask128& operator-=(const Bitmask128& rhs) noexcept;
		Bitmask128& operator+=(const uint64_t lo) noexcept;
		Bitmask128& operator-=(const uint64_t lo) noexcept;

		Bitmask128& operator&=(const Bitmask128& rhs) noexcept;
		Bitmask128& operator|=(const Bitmask128& rhs) noexcept;
		Bitmask128& operator^=(const Bitmask128& rhs) noexcept;

		constexpr Bitmask128 operator~() const noexcept;

		constexpr Bitmask128 operator<<(const int shiftLen) const noexcept;
		constexpr Bitmask128 operator>>(const int shiftLen) const noexcept;
		constexpr Bitmask128& operator<<=(const int shiftLen) noexcept;
		constexpr Bitmask128& operator>>=(const int shiftLen) noexcept;

		bool findFirstSetBit(unsigned long& index) const noexcept;
		bool findLastSetBit(unsigned long& index) const noexcept;

#		ifdef UWE_ARCH_SSE2
		
		constexpr static bool SupportsSIMD = true;

		explicit constexpr Bitmask128(const __m128i& vec) noexcept;
		constexpr  operator __m128i() const noexcept;

#		else 

		constexpr static bool SupportsSIMD = false;

#		endif

		friend std::ostream& operator<<(std::ostream& outStream, const Bitmask128& mask);
	};

	constexpr Bitmask128::Bitmask128() noexcept
		: lo{}, hi{}
	{
	}

	constexpr Bitmask128::Bitmask128(uint64_t hi, uint64_t lo) noexcept
		: lo{ lo }, hi{ hi }
	{
	}

	constexpr Bitmask128::Bitmask128(uint64_t lo) noexcept
		: lo{ lo }, hi{ 0ULL }
	{
	}

	constexpr Bitmask128::Bitmask128(const Bitmask128& src) noexcept
		: lo{ src.lo }, hi{ src.hi }
	{
	}

	constexpr Bitmask128& Bitmask128::operator=(const Bitmask128& rhs) noexcept {
		lo = rhs.lo;
		hi = rhs.hi;
		return *this;
	}

	constexpr Bitmask128::Bitmask128(Bitmask128&& src) noexcept
		: lo{ src.lo }, hi{ src.hi }
	{
	}

	constexpr Bitmask128& Bitmask128::operator=(Bitmask128&& rhs) noexcept {
		lo = rhs.lo;
		hi = rhs.hi;
		return *this;
	}

	constexpr Bitmask128 Bitmask128::min() noexcept {
		return {};
	}

	constexpr Bitmask128 Bitmask128::max() noexcept {
		return { std::numeric_limits<uint64_t>::max(), std::numeric_limits<uint64_t>::max() };
	}

	constexpr bool Bitmask128::operator==(const Bitmask128& rhs) const noexcept {
		return (lo == rhs.lo) & (hi == rhs.hi);
	}

	constexpr bool Bitmask128::operator!=(const Bitmask128& rhs) const noexcept {
		return (lo != rhs.lo) | (hi != rhs.hi);
	}

	constexpr Bitmask128 Bitmask128::operator~() const noexcept {
		return { ~hi, ~lo };
	}

	constexpr Bitmask128 Bitmask128::operator<<(const int shiftLen) const noexcept {
		Bitmask128 result{};

		if (shiftLen > 0U) {

			if (shiftLen < 64U) {
				result.lo = lo << shiftLen;
				result.hi = (hi << shiftLen) | (lo >> (64ULL - shiftLen));
			}
			else if (shiftLen < 128U) {
				result.lo = 0ULL;
				result.hi = lo << (shiftLen & 63U);
			}

		}
		else {
			result = *this;
		}

		return result;
	}

	constexpr Bitmask128 Bitmask128::operator>>(const int shiftLen) const noexcept {
		Bitmask128 result{};

		if (shiftLen > 0U) {

			if (shiftLen < 64U) {
				result.lo = (lo >> shiftLen) | (hi << (64ULL - shiftLen));
				result.hi = hi >> shiftLen;
			}
			else if (shiftLen < 128U) {
				result.lo = hi >> (shiftLen & 63U);
				result.hi = 0ULL;
			}

		}
		else {
			result = *this;
		}

		return result;
	}

	constexpr Bitmask128& Bitmask128::operator<<=(const int shiftLen) noexcept {
		if (shiftLen > 0U) {

			if (shiftLen < 64U) {
				hi = (hi << shiftLen) | (lo >> (64ULL - shiftLen));
				lo = lo << shiftLen;
			}
			else if (shiftLen < 128U) {
				hi = lo << (shiftLen & 63U);
				lo = 0ULL;
			}

		}

		return *this;
	}

	constexpr Bitmask128& Bitmask128::operator>>=(const int shiftLen) noexcept {
		if (shiftLen > 0U) {

			if (shiftLen < 64U) {
				lo = (lo >> shiftLen) | (hi << (64ULL - shiftLen));
				hi = hi >> shiftLen;
			}
			else if (shiftLen < 128U) {
				lo = hi >> (shiftLen & 63U);
				hi = 0ULL;
			}

		}

		return *this;
	}

	constexpr Bitmask128 Bitmask128::operator+(const Bitmask128& rhs) const noexcept {
		Bitmask128 result{ hi + rhs.hi, lo + rhs.lo };
		result.hi += (result.lo < lo);
		return result;
	}

	constexpr Bitmask128 Bitmask128::operator+(const uint64_t rhs) const noexcept {
		Bitmask128 result{ hi, lo + rhs };
		result.hi += (result.lo < lo);
		return result;
	}

	constexpr Bitmask128 Bitmask128::operator-(const Bitmask128& rhs) const noexcept {
		Bitmask128 result{ hi - rhs.hi, lo - rhs.lo };
		result.hi -= (lo < rhs.lo);
		return result;
	}

	constexpr Bitmask128 Bitmask128::operator-(const uint64_t rhs) const noexcept {
		Bitmask128 result{ hi, lo - rhs };
		result.hi -= (lo < rhs);
		return result;
	}

	inline Bitmask128& Bitmask128::operator+=(const Bitmask128& rhs) noexcept {
		hi += rhs.hi;
		lo += rhs.lo;
		hi += (lo < rhs.lo);
		return *this;
	}

	inline Bitmask128& Bitmask128::operator+=(const uint64_t rhs) noexcept {
		lo += rhs;
		hi += (lo < rhs);
		return *this;
	}

	inline Bitmask128& Bitmask128::operator-=(const Bitmask128& rhs) noexcept {
		hi -= rhs.hi;
		lo -= rhs.lo;
		hi -= (rhs.lo < lo);
		return *this;
	}

	inline Bitmask128& Bitmask128::operator-=(const uint64_t rhs) noexcept {
		lo -= rhs;
		hi -= (rhs < lo);
		return *this;
	}

	inline Bitmask128& Bitmask128::operator++() noexcept {
		return *this+=1ULL;
	}

	inline Bitmask128& Bitmask128::operator--() noexcept {
		return *this-=1ULL;
	}

#	if (defined UWE_ARCH_X64 && defined UWE_COMPILER_MSVC)

	inline bool Bitmask128::findFirstSetBit(unsigned long& index) const noexcept {
		bool bitFound{ static_cast<bool>(_BitScanForward64(&index, lo)) };

		if (!bitFound) {
			bitFound = static_cast<bool>(_BitScanForward64(&index, hi));
			index += sizeof(uint64_t) * CHAR_BIT;
		}

		return bitFound;
	}

	inline bool Bitmask128::findLastSetBit(unsigned long& index) const noexcept {
		bool bitFound{ static_cast<bool>(_BitScanReverse64(&index, hi)) };
		index += sizeof(uint64_t) * CHAR_BIT;

		if (!bitFound) {
			bitFound = static_cast<bool>(_BitScanReverse64(&index, lo));
		}

		return bitFound;
	}

#	endif

#	ifdef UWE_ARCH_SSE2

	constexpr Bitmask128::Bitmask128(const __m128i& vec) noexcept
		: lo{ vec.m128i_u64[0] }, hi{ vec.m128i_u64[1] }
	{
	}

	constexpr  Bitmask128::operator __m128i() const noexcept {
		__m128i vec {};
		vec.m128i_u64[0] = lo;
		vec.m128i_u64[1] = hi;
		return vec;
	}

	constexpr Bitmask128 Bitmask128::operator&(const Bitmask128& rhs) const noexcept {
		return Bitmask128{ _mm_and_si128(*this, rhs) };
	}

	constexpr Bitmask128 Bitmask128::operator|(const Bitmask128& rhs) const noexcept {
		return Bitmask128{ _mm_or_si128(*this, rhs) };
	}

	constexpr Bitmask128 Bitmask128::operator^(const Bitmask128& rhs) const noexcept {
		return Bitmask128{ _mm_xor_si128(*this, rhs) };
	}

	constexpr Bitmask128 Bitmask128::andNot(const Bitmask128& rhs) const noexcept {
		return Bitmask128{ _mm_andnot_si128(rhs, *this) };
	}

	inline Bitmask128& Bitmask128::operator&=(const Bitmask128& rhs) noexcept {
		*this = Bitmask128{ _mm_and_si128(*this, rhs) };
		return *this;
	}

	inline Bitmask128& Bitmask128::operator|=(const Bitmask128& rhs) noexcept {
		*this = Bitmask128{ _mm_or_si128(*this, rhs) };
		return *this;
	}

	inline Bitmask128& Bitmask128::operator^=(const Bitmask128& rhs) noexcept {
		*this = Bitmask128{ _mm_xor_si128(*this, rhs) };
		return *this;
	}

#	else // UWE_ARCH_SSE2

	constexpr Bitmask128 Bitmask128::operator&(const Bitmask128& rhs) const noexcept {
		return { hi & rhs.hi, lo & rhs.lo };
	}

	constexpr Bitmask128 Bitmask128::operator|(const Bitmask128& rhs) const noexcept {
		return { hi | rhs.hi, lo | rhs.lo };
	}

	constexpr Bitmask128 Bitmask128::operator^(const Bitmask128& rhs) const noexcept {
		return { hi ^ rhs.hi, lo ^ rhs.lo };
	}

	constexpr Bitmask128 Bitmask128::andNot(const Bitmask128& rhs) const noexcept {
		return { hi & ~rhs.hi, lo & ~rhs.lo };
	}

	inline Bitmask128& Bitmask128::operator&=(const Bitmask128& rhs) noexcept {
		lo &= rhs.lo;
		hi &= rhs.hi;
		return *this;
	}

	inline Bitmask128& Bitmask128::operator|=(const Bitmask128& rhs) noexcept {
		lo |= rhs.lo;
		hi |= rhs.hi;
		return *this;
	}

	inline Bitmask128& Bitmask128::operator^=(const Bitmask128& rhs) noexcept {
		lo ^= rhs.lo;
		hi ^= rhs.hi;
		return *this;
	}

#	endif // UWE_ARCH_SSE2
	
}

}