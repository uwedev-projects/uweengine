#pragma once

#include "UE/EnginePlatform.h"
#include "UE/Platform/Bitmask.h"

#include <intrin.h>

#include <type_traits>
#include <iterator>
#include <cstddef>
#include <array>

#if defined UWE_ARCH_X64 && defined UWE_COMPILER_MSVC
#	pragma intrinsic(_BitScanForward64, _BitScanReverse64, __popcnt64, _bittest64)
#endif

#if defined UWE_ARCH_X86 && defined UWE_COMPILER_MSVC
#	pragma intrinsic(_BitScanForward, _BitScanReverse, __popcnt, _bittest)
#endif

namespace Uwe {

namespace Platform {

	template <typename T>
	constexpr bool isPowerOfTwo(T n) noexcept {
		static_assert(std::is_integral_v<T>, "T needs to be integral.");
		return (n & (n - 1)) == 0;
	}

#	if (defined UWE_ARCH_X64 && defined UWE_COMPILER_MSVC)

	inline bool findFirstSetBit(unsigned long long mask, unsigned long& index) noexcept {
		return static_cast<bool>(_BitScanForward64(&index, mask));
	}

	inline bool findLastSetBit(unsigned long long mask, unsigned long& index) noexcept {
		return static_cast<bool>(_BitScanReverse64(&index, mask));
	}

#	endif

#	if (defined UWE_ARCH_X86 && defined UWE_COMPILER_MSVC)

	inline bool findFirstSetBit(unsigned long mask, unsigned long& index) noexcept {
		return static_cast<bool>(_BitScanForward(&index, mask));
	}

	inline bool findLastSetBit(unsigned long mask, unsigned long& index) noexcept {
		return static_cast<bool>(_BitScanReverse(&index, mask));
	}

#	endif

	unsigned countConsecutiveSetBits(unsigned long long mask);

#	if (defined UWE_ARCH_X64 && defined UWE_COMPILER_MSVC)

	inline unsigned countSetBits(unsigned long long mask) noexcept {
		return static_cast<unsigned>(__popcnt64(mask));
	}

#	endif

#	if (defined UWE_ARCH_X86 && defined UWE_COMPILER_MSVC)

	inline unsigned countSetBits(unsigned mask) noexcept {
		return __popcnt(mask);
	}

#	endif

#	if (!defined UWE_ARCH_X64 && defined UWE_ARCH_X86 && defined UWE_COMPILER_MSVC)

	inline unsigned countSetBits(unsigned long long mask) noexcept {
		return __popcnt(mask) + __popcnt(mask >> (sizeof(unsigned) * CHAR_BIT));
	}

#	endif

#	if (defined UWE_ARCH_X64 && defined UWE_COMPILER_MSVC)

	inline bool bitTest(const long long mask, unsigned index) noexcept {
		return _bittest64(&mask, static_cast<long long>(index));
	}

#	endif

#	if (defined UWE_ARCH_X86 && defined UWE_COMPILER_MSVC)

	inline bool bitTest(const long mask, unsigned index) noexcept {
		return _bittest(&mask, static_cast<long>(index));
	}

#endif

	namespace Detail {
		using BitmaskArray = std::array<Bitmask128, sizeof(Bitmask128) * CHAR_BIT + 1>;

		constexpr const size_t maskBits{ sizeof(Bitmask128) * CHAR_BIT };

		constexpr const BitmaskArray BaseMasks{ 
			Bitmask128 { 0x0000000000000000, 0x0000000000000000 },
			Bitmask128 { 0x0000000000000000, 0x0000000000000001 },
			Bitmask128 { 0x0000000000000000, 0x0000000000000003 },
			Bitmask128 { 0x0000000000000000, 0x0000000000000007 },
			Bitmask128 { 0x0000000000000000, 0x000000000000000F },
			Bitmask128 { 0x0000000000000000, 0x000000000000001F },
			Bitmask128 { 0x0000000000000000, 0x000000000000003F },
			Bitmask128 { 0x0000000000000000, 0x000000000000007F },
			Bitmask128 { 0x0000000000000000, 0x00000000000000FF },

			Bitmask128 { 0x0000000000000000, 0x00000000000001FF },
			Bitmask128 { 0x0000000000000000, 0x00000000000003FF },
			Bitmask128 { 0x0000000000000000, 0x00000000000007FF },
			Bitmask128 { 0x0000000000000000, 0x0000000000000FFF },
			Bitmask128 { 0x0000000000000000, 0x0000000000001FFF },
			Bitmask128 { 0x0000000000000000, 0x0000000000003FFF },
			Bitmask128 { 0x0000000000000000, 0x0000000000007FFF },
			Bitmask128 { 0x0000000000000000, 0x000000000000FFFF },

			Bitmask128 { 0x0000000000000000, 0x000000000001FFFF },
			Bitmask128 { 0x0000000000000000, 0x000000000003FFFF },
			Bitmask128 { 0x0000000000000000, 0x000000000007FFFF },
			Bitmask128 { 0x0000000000000000, 0x00000000000FFFFF },
			Bitmask128 { 0x0000000000000000, 0x00000000001FFFFF },
			Bitmask128 { 0x0000000000000000, 0x00000000003FFFFF },
			Bitmask128 { 0x0000000000000000, 0x00000000007FFFFF },
			Bitmask128 { 0x0000000000000000, 0x0000000000FFFFFF },

			Bitmask128 { 0x0000000000000000, 0x0000000001FFFFFF },
			Bitmask128 { 0x0000000000000000, 0x0000000003FFFFFF },
			Bitmask128 { 0x0000000000000000, 0x0000000007FFFFFF },
			Bitmask128 { 0x0000000000000000, 0x000000000FFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x000000001FFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x000000003FFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x000000007FFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x00000000FFFFFFFF },

			Bitmask128 { 0x0000000000000000, 0x00000001FFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x00000003FFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x00000007FFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x0000000FFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x0000001FFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x0000003FFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x0000007FFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x000000FFFFFFFFFF },

			Bitmask128 { 0x0000000000000000, 0x000001FFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x000003FFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x000007FFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x00000FFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x00001FFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x00003FFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x00007FFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x0000FFFFFFFFFFFF },

			Bitmask128 { 0x0000000000000000, 0x0001FFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x0003FFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x0007FFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x000FFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x001FFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x003FFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x007FFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x00FFFFFFFFFFFFFF },

			Bitmask128 { 0x0000000000000000, 0x01FFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x03FFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x07FFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x0FFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x1FFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x3FFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0x7FFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000000, 0xFFFFFFFFFFFFFFFF },

			Bitmask128 { 0x0000000000000001, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000003, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000007, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000000000000000F, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000000000000001F, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000000000000003F, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000000000000007F, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00000000000000FF, 0xFFFFFFFFFFFFFFFF },

			Bitmask128 { 0x00000000000001FF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00000000000003FF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00000000000007FF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000000FFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000001FFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000003FFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000007FFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000000000000FFFF, 0xFFFFFFFFFFFFFFFF },

			Bitmask128 { 0x000000000001FFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000000000003FFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000000000007FFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00000000000FFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00000000001FFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00000000003FFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00000000007FFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000000FFFFFF, 0xFFFFFFFFFFFFFFFF },

			Bitmask128 { 0x0000000001FFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000003FFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000007FFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000000000FFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000000001FFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000000003FFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000000007FFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00000000FFFFFFFF, 0xFFFFFFFFFFFFFFFF },

			Bitmask128 { 0x00000001FFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00000003FFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00000007FFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000000FFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000001FFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000003FFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000007FFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000000FFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },

			Bitmask128 { 0x000001FFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000003FFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000007FFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00000FFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00001FFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00003FFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00007FFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0000FFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },

			Bitmask128 { 0x0001FFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0003FFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0007FFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x000FFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x001FFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x003FFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x007FFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x00FFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },

			Bitmask128 { 0x01FFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x03FFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x07FFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x0FFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x1FFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x3FFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0x7FFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128 { 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
};

	}

	template <class Iter>
	class BitScanner {
	protected:
		using V = typename std::iterator_traits<Iter>::value_type;
		static_assert(std::is_same_v<V, Bitmask128>, "Invalid iterator type. Iterator of Bitmask128 is required.");

		static constexpr const size_t maskBits{ sizeof(Bitmask128) * CHAR_BIT };
		
		Bitmask128 scanMask;
		Iter begin, end;

		unsigned matchStartIndex;
		unsigned matchedBits;
		unsigned requestedBits;
		unsigned currentMaskIndex;
		unsigned scanBitWidth;
		unsigned scanOffset;

		void selectScanMask() noexcept;
		void scan(const Bitmask128& current) noexcept;
		Iter seekCurrentMask();
		bool isValidRange(unsigned startIndex, const unsigned count) noexcept;

	public:

		BitScanner(Iter begin, Iter end) noexcept;

		void attachSource(Iter begin, Iter end) noexcept;
		void reset() noexcept;

		bool findSequenceOfSetBits(Iter begin, Iter end, const unsigned countSetBits, unsigned& matchIndex);
		bool findSequenceOfSetBits(const unsigned countSetBits, unsigned& matchIndex);

		void flipBitsInRange(unsigned startIndex, const unsigned count);

	};

	template <class Iter>
	BitScanner<Iter>::BitScanner(Iter begin, Iter end) noexcept : 
		scanMask{}, begin{ begin }, end{ end }, matchStartIndex{}, 
		matchedBits{}, requestedBits{}, currentMaskIndex{}, scanBitWidth{}, scanOffset{}
	{
	}

	template <class Iter>
	inline void BitScanner<Iter>::attachSource(Iter newBegin, Iter newEnd) noexcept {
		begin	= newBegin;
		end		= newEnd;
	}

	template <class Iter>
	inline void BitScanner<Iter>::reset() noexcept {
		matchedBits			= 0U;
		requestedBits		= 0U;
		scanOffset			= 0U;
		currentMaskIndex	= 0U;
	}

	template <class Iter>
	inline bool BitScanner<Iter>::findSequenceOfSetBits(Iter newBegin, Iter newEnd, const unsigned countSetBits, unsigned& matchIndex) {
		reset();
		attachSource(newBegin, newEnd);
		return findSequenceOfSetBits(countSetBits, matchIndex);
	}

	template <class Iter>
	bool BitScanner<Iter>::findSequenceOfSetBits(const unsigned countSetBits, unsigned& matchIndex) {
		matchedBits		= 0U;
		requestedBits	= countSetBits;

		Iter current{ seekCurrentMask() };
		while (current != end && matchedBits < requestedBits) {
			selectScanMask();

			scanOffset = 0U;
			scan(*current);

			for (scanOffset = 1U; scanOffset < maskBits && matchedBits < requestedBits; ++scanOffset) {
				scanMask <<= 1ULL;
				scan(*current);
			}

			++current; ++currentMaskIndex;
		}

		bool sequenceFound{ matchedBits >= requestedBits };

		if (sequenceFound) {
			matchIndex = matchStartIndex;
			currentMaskIndex -= (scanOffset < maskBits);
		}
		
		if (current == end)
			reset();

		return sequenceFound;
	}

	template <class Iter>
	void BitScanner<Iter>::scan(const Bitmask128& current) noexcept {
		const Bitmask128 result{ current & scanMask };

		if (result == scanMask) {

			if (matchedBits == 0U)
				matchStartIndex = currentMaskIndex * maskBits + scanOffset;

			if (scanOffset > 0U) {
				const unsigned bitsScanned{ (scanOffset + scanBitWidth) > maskBits ? (maskBits - scanOffset) : scanBitWidth };
				matchedBits += bitsScanned;
				scanOffset += bitsScanned - 1U;
			}
			else
				matchedBits += scanBitWidth;

		}
		else {

			if (scanOffset == 0U && matchedBits > 0U) {
				matchedBits = 0U;
			}
			
			unsigned long lastZeroIdx{};
			const Bitmask128 invertedResult{ result ^ scanMask };

			invertedResult.findLastSetBit(lastZeroIdx);
			scanOffset = static_cast<unsigned>(lastZeroIdx);
		}
	}

	template <class Iter>
	void BitScanner<Iter>::selectScanMask() noexcept {
		const unsigned remainingBits{ requestedBits - matchedBits };

		if (matchedBits > 0U && remainingBits <= maskBits) {
			scanMask = Detail::BaseMasks[remainingBits];
			scanBitWidth = remainingBits;
		}
		else if (requestedBits > maskBits) {
			scanMask = Bitmask128::max();
			scanBitWidth = maskBits;
		}
		else {
			scanMask = Detail::BaseMasks[requestedBits];
			scanBitWidth = requestedBits;
		}
	}

	template <class Iter>
	Iter BitScanner<Iter>::seekCurrentMask() {
		Iter current = begin;

		if constexpr (std::is_same_v<std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>) {
			current += currentMaskIndex;
		}
		else {
			for (unsigned i{}; i < currentMaskIndex; ++i, ++current) {}
		}

		return current;
	}

	template <class Iter>
	void BitScanner<Iter>::flipBitsInRange(unsigned startIndex, const unsigned count) {
		if (!isValidRange(startIndex, count))
			return;

		unsigned maskIndex{ startIndex / maskBits };
		unsigned bitIndex{ startIndex & (maskBits - 1U) };

		Iter current{ begin };
		std::advance(current, maskIndex);

		for (unsigned bitsFlipped{}; bitsFlipped < count; bitsFlipped += scanBitWidth, ++current, ++maskIndex) {
			if (count - bitsFlipped + bitIndex > maskBits) {
				scanBitWidth = maskBits - bitIndex;
			}
			else {
				scanBitWidth = count - bitsFlipped;
			}

			const Bitmask128 flipMask{ Detail::BaseMasks[scanBitWidth] << bitIndex };
			*current ^= flipMask;

			bitIndex = (bitIndex + scanBitWidth) & (maskBits - 1U);
		}
		
	}

	template <class Iter>
	inline bool BitScanner<Iter>::isValidRange(unsigned startIndex, const unsigned count) noexcept {
		return std::distance(begin, end) * maskBits >= startIndex + count;
	}

}

}