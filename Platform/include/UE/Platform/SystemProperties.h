#pragma once

#include <bitset>
#include <string>
#include <cstddef>

namespace Uwe {

namespace Platform {

	struct CPUStats {
		short numaNodeCount;
		short coreCount;
		short logicalCoreCount;
		short lvl1CacheCount;
		short lvl2CacheCount;
		short lvl3CacheCount;

		CPUStats() = default;
	};

	enum class CacheLevel : unsigned {
		L1 = 1U,
		L2 = 2U,
		L3 = 3U
	};

	struct CacheProperties {
		std::size_t size;
		std::size_t lineSize;
		CacheLevel level;
		unsigned associativity;

		CacheProperties() noexcept = default;
		CacheProperties(CacheLevel level, std::size_t size, std::size_t lineSize, unsigned associativity) noexcept :
			size{size}, lineSize{lineSize}, level{level}, associativity{associativity} {}

		CacheProperties(const CacheProperties&) noexcept = default;
		CacheProperties& operator=(const CacheProperties&) noexcept = default;

		CacheProperties(CacheProperties&&) noexcept = default;
		CacheProperties& operator=(CacheProperties&&) noexcept = default;
	};

	enum class ISAExtension : std::size_t {
		SSE3 = 0U,
		PCLMULQDQ,
		MONITOR,
		SSSE3,
		FMA,
		CMPXCHG16B,
		SSE41,
		SSE42,
		MOVBE,
		POPCNT,
		AES,
		XSAVE,
		OSXSAVE,
		AVX,
		F16C,
		RDRAND,
		MSR,
		CX8,
		SEP,
		CMOV,
		CLFSH,
		MMX,
		FXSR,
		SSE,
		SSE2,
		FSGSBASE,
		BMI1,
		HLE,
		AVX2,
		BMI2,
		ERMS,
		INVPCID,
		RTM,
		AVX512F,
		RDSEED,
		ADX,
		AVX512PF,
		AVX512ER,
		AVX512CD,
		SHA,
		PREFETCHWT1,
		LAHF,
		LZCNT,
		ABM,
		SSE4a,
		XOP,
		TBM,
		SYSCALL,
		MMXEXT,
		RDTSCP,
		_3DNOWEXT,
		_3DNOW,
		FEATURE_COUNT
	};

	enum class Vendor : unsigned char { Unknown, AMD, Intel };

	class SystemProperties {
	public:
		SystemProperties();

		inline bool supportsISAExtension(ISAExtension id) const { 
			return extensions[static_cast<std::size_t>(id)];
		}

		inline bool supportsISAExtensions(std::size_t mask) const {
			return (extensions.to_ullong() & mask) == mask;
		}

		inline const std::string& getVendorAsString() const noexcept { return vendorString; }

		inline const std::string& getBrandAsString() const noexcept { return brandString; }

		inline Vendor getVendor() const noexcept { return vendor; }

		inline bool isAMD() const noexcept { return vendor == Vendor::AMD; };
		inline bool isIntel() const noexcept { return vendor == Vendor::Intel; };

		inline CacheProperties getL1CacheProperties() const noexcept { return cachePropsL1D; };

		void queryISAExtensions();
		void gatherCacheProperties();

		static const SystemProperties& getGlobalSystemProperties();

	protected:
		using FeatureBitset = std::bitset<static_cast<std::size_t>(ISAExtension::FEATURE_COUNT)>;

		CacheProperties cachePropsL1D;

		FeatureBitset extensions;
		std::string vendorString;
		std::string brandString;
		Vendor vendor;
	
	};

}

}