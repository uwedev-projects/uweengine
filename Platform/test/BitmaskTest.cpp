#include "gtest/gtest.h"

#include "UE/EnginePlatform.h"
#include "UE/Platform/Bitmask.h"

#include <iostream>

namespace Uwe {

namespace Platform {

	TEST(Bitmask128Test, ComparesEqualIfLoAndHiWordsAreEqual) {
		Bitmask128 m1{ 0xF3F3, 0xFFFF };
		EXPECT_TRUE(m1 == m1);

		Bitmask128 m2{ 0xF3F3, 0xFFEF };
		EXPECT_FALSE(m1 == m2);
		EXPECT_FALSE(m1 == Bitmask128::max());

		Bitmask128 m3{ m1 };
		EXPECT_TRUE(m1 == m3);
		EXPECT_TRUE(Bitmask128::max() == Bitmask128::max());
	}

	TEST(Bitmask128Test, ComparesUnequalIfLoAndHiByteDiffer) {
		Bitmask128 m1{ 0xFFFFA404, 0ULL };
		Bitmask128 m2{ 0xFFFFA404, 1ULL };

		EXPECT_TRUE(m1 != m2);
		EXPECT_FALSE(m1 != m1);

		Bitmask128 m3{ 0xFFFEA404, 0ULL };

		EXPECT_TRUE(m1 != m3);
		EXPECT_FALSE(m3 != m3);

		EXPECT_TRUE(Bitmask128::min() != Bitmask128::max());
	}

	TEST(Bitmask128Test, BitwiseAndTest) {
		Bitmask128 m1{ 0x3333ULL , 0x5555ULL << 48 };
		Bitmask128 result{ m1 & Bitmask128::max() };

		EXPECT_EQ(m1 & Bitmask128::max(), m1);

		Bitmask128 m2{ 0x03ULL, 0x03ULL };
		result = m1 & m2;

		EXPECT_EQ(result.hi, 0x03);
		EXPECT_EQ(result.lo, 0x00);

		Bitmask128 m3{ 0xFF0000ULL, 0x5555ULL << 48 };
		result = m1 & m3;

		EXPECT_EQ(result.hi, 0x00ULL);
		EXPECT_EQ(result.lo, 0x5555ULL << 48);
	}

	TEST(Bitmask128Test, BitwiseOrTest) {
		Bitmask128 m1{ 0x5555ULL, 0x5555ULL << 48 };
		Bitmask128 m2{ 0x5555ULL << 1U, 0x5555ULL << 49 };

		Bitmask128 result{ m1 | m2 };
		EXPECT_EQ(result.hi, 0xFFFFULL);
		EXPECT_EQ(result.lo, 0xFFFFULL << 48);

		EXPECT_EQ(m1 | Bitmask128::max(), Bitmask128::max());
		EXPECT_EQ(m1 | Bitmask128::min(), m1);
	}

	TEST(Bitmask128Test, BitwiseXOrTest) {
		Bitmask128 m1{ 0x3333ULL, 0xCCCCULL };
		Bitmask128 expected{ 0xFFFFFFFFFFFFCCCC, 0xFFFFFFFFFFFF3333 };

		EXPECT_EQ(m1 ^ Bitmask128::max(), expected);
		EXPECT_EQ(Bitmask128::min() ^ Bitmask128::max(), Bitmask128::max());
		EXPECT_EQ(Bitmask128::max() ^ Bitmask128::max(), Bitmask128::min());

		Bitmask128 m2{ 0b11110000ULL, 0b11110000ULL << 7 };
		Bitmask128 m3{ 0xFFULL, 0xFFULL << 7 };

		expected = { 0b00001111ULL, 0b00001111ULL << 7 };

		EXPECT_EQ(m2 ^ m3, expected);
	}

	TEST(Bitmask128Test, BitshiftLeftByZeroHasNoEffect) {
		Bitmask128 m1{ 1ULL };
		Bitmask128 m2{ m1 << 0 };

		EXPECT_EQ(m2, m1);
	}

	TEST(Bitmask128Test, BitshiftRightByZeroHasNoEffect) {
		Bitmask128 m1{ 0x8000000000000000, 0x00 };
		Bitmask128 m2{ m1 >> 0 };

		EXPECT_EQ(m2, m1);
	}

	TEST(Bitmask128Test, BitshiftLeftTest) {
		Bitmask128 m1{ 0x00FF00FF00FF00FF, 0x00FF00FF00FF00FF };
		
		Bitmask128 expected{ 0xFF00FF00FF00FF00, 0xFF00FF00FF00FF00 };
		EXPECT_EQ(m1 << 8, expected);

		expected = { 0x0FF00FF00FF00FF0, 0x0FF00FF00FF00FF0 };
		EXPECT_EQ(m1 << 4, expected);
	}

	TEST(Bitmask128Test, BitshiftLeftWithOverflow) {
		Bitmask128 m1{ 0xFFFFFFFFFFFFFFFF, 0xFF000000000000FF };

		Bitmask128 expected{ 0xFF000000000000FF, 0x00ULL };
		EXPECT_EQ(m1 << 64, expected);

		expected = { 0x000000000000FF00, 0x00ULL };
		EXPECT_EQ(m1 << 72, expected);

		Bitmask128 m2{ 0xFFULL, 0xFFFF000000000000 };
		expected = { 0xFFFULL, 0xFFF0000000000000 };
		EXPECT_EQ(m2 << 4, expected);
		EXPECT_EQ(m2 << 128, Bitmask128::min());
	}

	TEST(Bitmask128Test, BitshiftRightTest) {
		Bitmask128 m1{ 0x00FF00FF00FF00FF, 0x00FF00FF00FF00FF };

		Bitmask128 expected{ 0x0000FF00FF00FF00, 0xFF00FF00FF00FF00 };
		EXPECT_EQ(m1 >> 8, expected);

		expected = { 0x000FF00FF00FF00F, 0xF00FF00FF00FF00F };
		EXPECT_EQ(m1 >> 4, expected);
	}

	TEST(Bitmask128Test, BitshiftRightWithOverflow) {
		Bitmask128 m1{ 0xFF000000000000FF, 0xFFFFFFFFFFFFFFFF };

		Bitmask128 expected{ 0x00ULL, 0xFF000000000000FF };
		EXPECT_EQ(m1 >> 64, expected);

		expected = { 0x00ULL, 0x00FF000000000000 };
		EXPECT_EQ(m1 >> 72, expected);

		Bitmask128 m2{ 0xFF, 0xFFFF000000000000 };
		expected = { 0xF, 0xFFFFF00000000000 };
		EXPECT_EQ(m2 >> 4, expected);
		EXPECT_EQ(m2 >> 128, Bitmask128::min());
	}

	TEST(Bitmask128Test, BitwiseNotTest) {
		Bitmask128 m1{ 0xFFFFFFFF00000000, 0xFFFFFFFF00000000 };
		Bitmask128 expected{ 0x00000000FFFFFFFF, 0x00000000FFFFFFFF };

		EXPECT_EQ(~m1, expected);
		EXPECT_EQ(~Bitmask128::max(), Bitmask128::min());
		EXPECT_EQ(~Bitmask128::min(), Bitmask128::max());
	}

	TEST(Bitmask128Test, BitwiseAndAssignmentTest) {
		Bitmask128 m1{ Bitmask128::max() };
		Bitmask128 m2{ 0x00FF00FF00FF00FF, 0x00FF00FF00FF00FF };

		m1 &= m2;
		EXPECT_EQ(m1, m2);
	}

	TEST(Bitmask128Test, BitwiseOrAssignmentTest) {
		Bitmask128 m1{};
		Bitmask128 m2{ 0xFFFFULL, 0xFFFFULL << 48 };

		m1 |= m2;
		EXPECT_EQ(m1, m2);
	}

	TEST(Bitmask128Test, BitwiseXOrAssignmentTest) {
		Bitmask128 m1{ 0x3333333333333333ULL, 0x3333333333333333ULL };
		m1 ^= Bitmask128::max();

		Bitmask128 expected{ 0x3333333333333333ULL << 2, 0x3333333333333333ULL << 2 };
		EXPECT_EQ(m1, expected);
	}

	TEST(Bitmask128Test, BitshiftLeftAssignmentTest) {
		Bitmask128 m1{ 0xFFFFFFFFFFFFFFFF, 0xFF000000000000FF };
		m1 <<= 64;

		Bitmask128 expected{ 0xFF000000000000FF, 0x00ULL };
		EXPECT_EQ(m1, expected);

		Bitmask128 m2{ 0xFFFFFFFFFFFFFFFF, 0xFF000000000000FF };
		m2 <<= 72;

		expected = { 0x000000000000FF00, 0x00ULL };
		EXPECT_EQ(m2, expected);

		Bitmask128 m3{ 0xFFULL, 0xFFFF000000000000 };
		m3 <<= 4;

		expected = { 0xFFFULL, 0xFFF0000000000000 };
		EXPECT_EQ(m3, expected);
	}

	TEST(Bitmask128Test, BitshiftRightAssignmentTest) {
		Bitmask128 m1{ 0xFF000000000000FF, 0xFFFFFFFFFFFFFFFF };

		m1 >>= 64;

		Bitmask128 expected{ 0x00ULL, 0xFF000000000000FF };
		EXPECT_EQ(m1, expected);

		Bitmask128 m2{ 0xFF000000000000FF, 0xFFFFFFFFFFFFFFFF };
		m2 >>= 72;

		expected = { 0x00ULL, 0x00FF000000000000 };
		EXPECT_EQ(m2, expected);

		Bitmask128 m3{ 0xFF, 0xFFFF000000000000 };
		m3 >>= 4;

		expected = { 0xF, 0xFFFFF00000000000 };
		EXPECT_EQ(m3, expected);
	}

	TEST(Bitmask128Test, BitwiseAndNotTest) {
		Bitmask128 m1{ Bitmask128::max() };
		Bitmask128 m2{ 0xFF0000000000FFFF, 0xFFFF0000000000FF };
		Bitmask128 expected{ 0x00FFFFFFFFFF0000, 0x0000FFFFFFFFFF00 };

		EXPECT_EQ(m1.andNot(m2), expected);
	}

	TEST(Bitmask128Test, Adds64BitValueWithCarry) {
		Bitmask128 m1{ 0x00, 0xFFFFFFFFFFFFFFFF };
		
		Bitmask128 expected{ 0x01, 0x00 };
		EXPECT_EQ(m1 + 1ULL, expected);

		expected = { 0x01, 0x01 };
		EXPECT_EQ(m1 + 2ULL, expected);

		expected = { 0x01, 0x0F };
		EXPECT_EQ(m1 + 16ULL, expected);
	}

	TEST(Bitmask128Test, AddsBitmaskWithCarry) {
		Bitmask128 m1{ 0x00, 0xFFFFFFFFFFFFFFFF };
		Bitmask128 m2{ 0x00, 0x01 };
		Bitmask128 expected{ 0x01, 0x00 };
		EXPECT_EQ(m1 + m2, expected);

		Bitmask128 m3{ 0x00, 0x02 };
		expected = { 0x01, 0x01 };
		EXPECT_EQ(m1 + m3, expected);

		Bitmask128 m4{ 0x00, 0x10 };
		expected = { 0x01, 0x0F };
		EXPECT_EQ(m1 + m4, expected);
	}

	TEST(Bitmask128Test, Subtracts64BitValueWithCarry) {
		Bitmask128 m1{ 0x01, 0x00 };
		Bitmask128 expected{ 0x00, 0xFFFFFFFFFFFFFFFF };
		EXPECT_EQ(m1 - 1ULL, expected);

		Bitmask128 m2{};
		EXPECT_EQ(m2 - 1ULL, Bitmask128::max());
	}

	TEST(Bitmask128Test, SubtractsBitmaskWithCarry) {
		Bitmask128 m1{ 0x01, 0x00 };
		Bitmask128 m2{ 0x00, 0x01 };

		Bitmask128 expected{ 0x00, 0xFFFFFFFFFFFFFFFF };
		EXPECT_EQ(m1 - m2, expected);

		Bitmask128 m3{};
		EXPECT_EQ(m3 - m2, Bitmask128::max());
	}

	TEST(Bitmask128Test, Adds64BitValueToItselfWithCarry) {
		const Bitmask128 m1{ 0x00, 0xFFFFFFFFFFFFFFFF };
		Bitmask128 m2{ m1 };

		m2 += 1ULL;
		Bitmask128 expected{ 0x01, 0x00 };
		EXPECT_EQ(m2, expected);
		
		m2 = m1;
		m2 += 2ULL;
		expected = { 0x01, 0x01 };
		EXPECT_EQ(m2, expected);

		m2 = m1;
		m2 += 16ULL;
		expected = { 0x01, 0x0F };
		EXPECT_EQ(m2, expected);
	}

	TEST(Bitmask128Test, AddsBitmaskToItselfWithCarry) {
		Bitmask128 m1{ 0x00, 0xFFFFFFFFFFFFFFFF };
		m1 += { 0x00, 0x01 };
		Bitmask128 expected{ 0x01, 0x00 };
		EXPECT_EQ(m1, expected);

		m1 = { 0x00, 0xFFFFFFFFFFFFFFFF };
		m1 += { 0x00, 0x02 };
		expected = { 0x01, 0x01 };
		EXPECT_EQ(m1, expected);

		m1 = { 0x00, 0xFFFFFFFFFFFFFFFF };
		m1 += { 0x00, 0x10 };
		expected = { 0x01, 0x0F };
		EXPECT_EQ(m1, expected);
	}

	TEST(Bitmask128Test, Subtracts64BitValueFromItselfWithCarry) {
		Bitmask128 m1{ 0x01, 0x00 };
		m1 -= 1ULL;
		Bitmask128 expected{ 0x00, 0xFFFFFFFFFFFFFFFF };
		EXPECT_EQ(m1, expected);

		Bitmask128 m2{};
		m2 -= 1ULL;
		EXPECT_EQ(m2, Bitmask128::max());
	}

	TEST(Bitmask128Test, SubtractsBitmaskFromItselfWithCarry) {
		Bitmask128 m1{ 0x01, 0x00 };
		m1 -= { 0x00, 0x01 };

		Bitmask128 expected{ 0x00, 0xFFFFFFFFFFFFFFFF };
		EXPECT_EQ(m1, expected);

		Bitmask128 m3{};
		m3 -= { 0x00, 0x01 };
		EXPECT_EQ(m3, Bitmask128::max());
	}

	TEST(Bitmask128Test, FindsFirstSetBitInLoWord) {
		Bitmask128 m1{ 0x10, 0x08 };
		unsigned long index{};
		
		EXPECT_TRUE(m1.findFirstSetBit(index));
		EXPECT_EQ(index, 3UL);

		Bitmask128 m2{ 0x10, 0x01F100 };
		
		EXPECT_TRUE(m2.findFirstSetBit(index));
		EXPECT_EQ(index, 8UL);
	}

	TEST(Bitmask128Test, FindsFirstSetBitInHiWord) {
		Bitmask128 m1{ 0x08, 0x00 };
		unsigned long index{};

		EXPECT_TRUE(m1.findFirstSetBit(index));
		EXPECT_EQ(index, 67UL);

		Bitmask128 m2{ 0x01F100000000, 0x00 };

		EXPECT_TRUE(m2.findFirstSetBit(index));
		EXPECT_EQ(index, 96UL);

		Bitmask128 m3{ 0x01F200000000, 0x00 };

		EXPECT_TRUE(m3.findFirstSetBit(index));
		EXPECT_EQ(index, 97UL);
	}

	TEST(Bitmask128Test, FindsNoSetBit) {
		Bitmask128 m1{};
		unsigned long index{};

		EXPECT_FALSE(m1.findFirstSetBit(index));
	}

	TEST(Bitmask128Test, FindsLastSetBitInLoWord) {
		Bitmask128 m1{ 0x00, 0x10 };
		unsigned long index{};

		EXPECT_TRUE(m1.findLastSetBit(index));
		EXPECT_EQ(index, 4UL);

		Bitmask128 m2{ 0x00, 0x1FF };

		EXPECT_TRUE(m2.findLastSetBit(index));
		EXPECT_EQ(index, 8UL);
	}

	TEST(Bitmask128Test, FidnsLastSetBitInHiWord) {
		Bitmask128 m1{ Bitmask128::max() };
		unsigned long index{};

		EXPECT_TRUE(m1.findLastSetBit(index));
		EXPECT_EQ(index, 127UL);

		Bitmask128 m2{ 0x1FF, 0xF0F0 };
		
		EXPECT_TRUE(m2.findLastSetBit(index));
		EXPECT_EQ(index, 72UL);
	}
}

}