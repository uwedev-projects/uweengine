#include "gtest/gtest.h"

#include "UE/EnginePlatform.h"
#include "UE/Platform/BitScan.h"
#include "UE/Platform/Bitmask.h"

#include <array>
#include <limits>
#include <bitset>
#include <iostream>
#include <algorithm>

namespace Uwe {

namespace Platform {

	TEST(BitScanTest, FindsFirstSetBitUnsignedLongLong) {
		unsigned long index{};

		EXPECT_TRUE(findFirstSetBit(1ULL, index));
		EXPECT_EQ(index, 0);

		EXPECT_TRUE(findFirstSetBit(2ULL, index));
		EXPECT_EQ(index, 1);

		EXPECT_TRUE(findFirstSetBit(4ULL, index));
		EXPECT_EQ(index, 2);

		EXPECT_TRUE(findFirstSetBit(8ULL, index));
		EXPECT_EQ(index, 3);

		EXPECT_TRUE(findFirstSetBit(16ULL, index));
		EXPECT_EQ(index, 4);

		EXPECT_TRUE(findFirstSetBit(32ULL, index));
		EXPECT_EQ(index, 5);

		EXPECT_TRUE(findFirstSetBit(64ULL, index));
		EXPECT_EQ(index, 6);
	}

	TEST(BitScanTest, DoesNotFindSetBitUnsignedLongLong) {
		unsigned long index{};

		EXPECT_FALSE(findFirstSetBit(0ULL, index));
	}

	TEST(BitScanTest, CountConsecutiveOneBitsTest) {
		EXPECT_EQ(countConsecutiveSetBits(std::numeric_limits<unsigned long long>::max()), sizeof(unsigned long long) * CHAR_BIT);
		EXPECT_EQ(countConsecutiveSetBits(0b1111010011110010), 4U);
		EXPECT_EQ(countConsecutiveSetBits(0b1001010011110010), 4U);
		EXPECT_EQ(countConsecutiveSetBits(0b10011101), 3U);
		EXPECT_EQ(countConsecutiveSetBits(0b1101101), 2U);
		EXPECT_EQ(countConsecutiveSetBits(0ULL), 0U);
	}

	TEST(BitScanTest, BitTestTest) {
		EXPECT_FALSE(bitTest(0LL, 0));
		EXPECT_FALSE(bitTest(1LL, 1));
		EXPECT_FALSE(bitTest(2LL, 0));

		EXPECT_TRUE(bitTest(1LL, 0));
		EXPECT_TRUE(bitTest(2LL, 1));
		EXPECT_TRUE(bitTest(3LL, 0));
		EXPECT_TRUE(bitTest(3LL, 1));
	}

	TEST(BitScanTest, CountSetBitsUnsignedLongLong) {
		EXPECT_EQ(countSetBits(0ULL), 0);
		EXPECT_EQ(countSetBits(4ULL), 1);
		EXPECT_EQ(countSetBits(127ULL), 7);
		EXPECT_EQ(countSetBits(1023ULL), 10);

		constexpr unsigned long long maxVal{ std::numeric_limits<unsigned long long>::max() };
		EXPECT_EQ(countSetBits(maxVal), sizeof(unsigned long long) * CHAR_BIT);
	}

	TEST(BitScanTest, CountSetBitsUnsigned) {
		EXPECT_EQ(countSetBits(0U), 0);
		EXPECT_EQ(countSetBits(4U), 1);
		EXPECT_EQ(countSetBits(127U), 7);
		EXPECT_EQ(countSetBits(1023U), 10);
		
		constexpr unsigned maxVal{ std::numeric_limits<unsigned>::max() };
		EXPECT_EQ(countSetBits(maxVal), sizeof(unsigned) * CHAR_BIT);
	}

	template <size_t Size = 1U>
	using BitScanner128 = BitScanner<typename std::array<Bitmask128, Size>::iterator >;

	TEST(BitScannerTest, FindsASequenceOf6SetBitsInASingleMask) {
		std::array<Bitmask128, 1U> bitString{ Bitmask128 {0x0F, 0xF000000000000000} };
		BitScanner128<1U> scanner{ bitString.begin(), bitString.end() };

		unsigned matchIdx{};
		bool foundMatch{ scanner.findSequenceOfSetBits(6U, matchIdx) };

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIdx, 60U);

		bitString = { Bitmask128 {0xFFFFFFFF00000000, 0x00} };
		foundMatch = scanner.findSequenceOfSetBits(bitString.begin(), bitString.end(), 6U, matchIdx);

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIdx, 96U);
	}

	TEST(BitScannerTest, FindsASquenceOf6SetBits) {
		std::array<Bitmask128, 2U> bitString{
			Bitmask128{ 0xF0F0000000000000, 0x0000000000000000 },
			Bitmask128{ 0x0000000000000000, 0x0000000000000003 }
		};

		BitScanner128<2U> scanner{ bitString.begin(), bitString.end() };

		unsigned matchIdx{};
		bool foundMatch{ scanner.findSequenceOfSetBits(6U, matchIdx) };

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIdx, 124U);
	}

	TEST(BitScannerTest, FindsASequenceOf80SetBitsSpanningTwoMasks) {
		std::array<Bitmask128, 4U> bitString{
			Bitmask128{ 0x0000F0000F0000F0, 0x00F0000FF0000F00 },
			Bitmask128{ 0xFFFFFFFFFFFFFFFF, 0xFF00000000000000 },
			Bitmask128{ 0x0000000000000000, 0x00000000000000FF },
			Bitmask128{ 0x0000000000000000, 0x0000000000000000 }
		};

		BitScanner128<4U> scanner{ bitString.begin(), bitString.end() };

		unsigned matchIdx{};
		bool foundMatch{ scanner.findSequenceOfSetBits(bitString.begin(), bitString.end(), 80U, matchIdx) };

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIdx, 184U);
	}

	TEST(BitScannerTest, FindsASequenceOf80SetBits) {
		std::array<Bitmask128, 4U> bitString{
			Bitmask128{ 0x0000F0000F0000F0, 0x00F0000FF0000F00 },
			Bitmask128{ 0xFFFFFFFFFFFFFFFF, 0xFF00F0000F000F00 },
			Bitmask128{ 0x0000000000FFFFFF, 0xFFFFFFFFFFFFFF00 },
			Bitmask128{ 0x0000000000000000, 0x0000000000000000 }
		};

		BitScanner128<4U> scanner{ bitString.begin(), bitString.end() };

		unsigned matchIdx{};
		bool foundMatch{ scanner.findSequenceOfSetBits(bitString.begin(), bitString.end(), 80U, matchIdx) };

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIdx, 264U);
	}

	TEST(BitScannerTest, FindsTwoSequencesOf80SetBits) {
		std::array<Bitmask128, 4U> bitString{
			Bitmask128{ 0x0000F0000F0000F0, 0x00F0000FF0000F00 },
			Bitmask128{ 0xFFFFFFFFFFFFFFFF, 0xFF00F0000F000F00 },
			Bitmask128{ 0xFFFFFFFF00000000, 0xFFFFFFFFFFFF00FF },
			Bitmask128{ 0x0000000000000000, 0x0000FFFFFFFFFFFF }
		};

		BitScanner128<4U> scanner{ bitString.begin(), bitString.end() };

		unsigned matchIdx{};
		bool foundMatch{ scanner.findSequenceOfSetBits(80U, matchIdx) };

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIdx, 184U);

		foundMatch = scanner.findSequenceOfSetBits(80U, matchIdx);

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIdx, 352U);
	}

	TEST(BitScannerTest, FindsASequenceOf160SetBitsSpanningTwoMasks) {
		std::array<Bitmask128, 4U> bitString{
			Bitmask128{ 0x0000F0000F0000F0, 0x00F0000FF0000F00 },
			Bitmask128{ 0xFFFFFFFFFFFFFFFF, 0xFF00F0000F000F00 },
			Bitmask128{ 0x0000000000FFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128{ 0x0000000000000000, 0x0000000000000000 }
		};

		BitScanner128<4U> scanner{ bitString.begin(), bitString.end() };

		unsigned matchIdx{};
		bool foundMatch{ scanner.findSequenceOfSetBits(160U, matchIdx) };

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIdx, 184U);
	}

	TEST(BitScannerTest, FindsTwoSequencesOf264SetBits) {
		std::array<Bitmask128, 6U> bitString{
			Bitmask128{ 0xFFFFFFFFFFFFFFFF, 0xFFFF000FF0000F00 },
			Bitmask128{ 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128{ 0xFFFFFFFF00000000, 0x00FFFFFFFFFFFFFF },
			Bitmask128{ 0xFFFFFFFFFF000000, 0x0000FFFFFFFFFFFF },
			Bitmask128{ 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128{ 0x00FFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
		};

		BitScanner128<6U> scanner{ bitString.begin(), bitString.end() };

		unsigned matchIdx{};
		bool foundMatch{ scanner.findSequenceOfSetBits(264U, matchIdx) };

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIdx, 48U);

		foundMatch = scanner.findSequenceOfSetBits(264U, matchIdx);

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIdx, 472U);
	}

	TEST(BitScannerTest, FindsAMatchTheFirstScanButNotOnTheSecondOne) {
		std::array<Bitmask128, 6U> bitString{
			Bitmask128{ 0xFFFFFFFFFFFFFFFF, 0xFFFF000FF0000F00 },
			Bitmask128{ 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128{ 0xFFFFFFFF00000000, 0x00FFFFFFFFFFFFFF },
			Bitmask128{ 0xFFF04FFFFF000000, 0x0000FFFFFFFFFFFF },
			Bitmask128{ 0xFFFFFFFFFFFFFFFF, 0xFFFFFF0000FFFFFF },
			Bitmask128{ 0x00FFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF },
		};

		BitScanner128<6U> scanner{ bitString.begin(), bitString.end() };

		unsigned matchIdx{};
		bool foundMatch{ scanner.findSequenceOfSetBits(264U, matchIdx) };

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIdx, 48U);

		foundMatch = scanner.findSequenceOfSetBits(264U, matchIdx);

		EXPECT_FALSE(foundMatch);
		EXPECT_EQ(matchIdx, 48U);
	}

	TEST(BitScannerTest, FindsNoSequenceOfSetBits) {
		std::array<Bitmask128, 16U> bitString{};
		BitScanner128<16U> scanner{ bitString.begin(), bitString.end() };

		unsigned matchIdx{};

		bool foundMatch{ scanner.findSequenceOfSetBits(bitString.begin(), bitString.end(), 4U, matchIdx) };

		EXPECT_FALSE(foundMatch);
		EXPECT_EQ(matchIdx, 0U);
	}

	TEST(BitScannerTest, FindsNoSquenceOf8SetBits) {
		std::array<Bitmask128, 2U> bitString{
			Bitmask128{ 0x0F0F0F0F0F0F0F0F, 0x0F0F0F0F0F0F0F0F },
			Bitmask128{ 0x0F0F0F0F0F0F0F0F, 0x0F0F0F0F0F0F0F0F }
		};

		BitScanner128<2U> scanner{ bitString.begin(), bitString.end() };

		unsigned matchIdx{};

		bool foundMatch{ scanner.findSequenceOfSetBits(bitString.begin(), bitString.end(), 8U, matchIdx) };

		EXPECT_FALSE(foundMatch);
		EXPECT_EQ(matchIdx, 0U);
	}

	TEST(BitScannerTest, FindsNoSequenceOf160SetBitsSpanningTwoMasks) {
		std::array<Bitmask128, 4U> bitString{
			Bitmask128{ 0x0000F0000F0000F0, 0x00F0000FF0000F00 },
			Bitmask128{ 0xFFFFFFFFFFFFFF00, 0xFF00F0000F000F00 },
			Bitmask128{ 0x0000000000FFFFFF, 0xFFFFFFFFFFFFFFFF },
			Bitmask128{ 0x0000000000000000, 0x0000000000000000 }
		};

		BitScanner128<4U> scanner{ bitString.begin(), bitString.end() };

		unsigned matchIdx{};
		bool foundMatch{ scanner.findSequenceOfSetBits(bitString.begin(), bitString.end(), 160U, matchIdx) };

		EXPECT_FALSE(foundMatch);
		EXPECT_EQ(matchIdx, 0U);
	}

	TEST(BitScannerTest, FlipsTheFirst16BitsInASingleMask) {
		std::array<Bitmask128, 1U> bitString{
			Bitmask128{ 0x0000F0000F0000F0, 0x00F0000FF0000F00 }
		};

		BitScanner128<1U> scanner{ bitString.begin(), bitString.end() };
		scanner.flipBitsInRange(0U, 16U);

		Bitmask128 expected{ 0x0000F0000F0000F0, 0x00F0000FF000F0FF };
		EXPECT_EQ(bitString[0], expected);
	}

	TEST(BitScannerTest, Flips16BitsInTheMiddleOfAMask) {
		std::array<Bitmask128, 1U> bitString{
			Bitmask128{ 0x0000F0000F0000F1, 0x10F0000FF0000F00 }
		};

		BitScanner128<1U> scanner{ bitString.begin(), bitString.end() };
		scanner.flipBitsInRange(56U, 16U);

		EXPECT_EQ(bitString[0], Bitmask128(0x0000F0000F00000E, 0xEFF0000FF0000F00));
	}

	TEST(BitScannerTest, Flips32BitsAccrossTwoAdjacentMasks) {
		std::array<Bitmask128, 2U> bitString{
			Bitmask128{ 0x000000000F0000F1, 0x10F0000FF0000F00 },
			Bitmask128{ 0x0000F0000F0000F1, 0x10F0000FF0000000 }
		};

		BitScanner128<2U> scanner{ bitString.begin(), bitString.end() };
		scanner.flipBitsInRange(112U, 32U);

		EXPECT_EQ(bitString[0], Bitmask128(0xFFFF00000F0000F1, 0x10F0000FF0000F00));
		EXPECT_EQ(bitString[1], Bitmask128(0x0000F0000F0000F1, 0x10F0000FF000FFFF));
	}

	TEST(BitScannerTest, Flips64BitsAtTheEndOfTheBitString) {
		std::array<Bitmask128, 2U> bitString{
			Bitmask128{ 0x000000000F0000F1, 0x10F0000FF0000F00 },
			Bitmask128{ 0x0000F0000F0000F1, 0x10F0000FF0000000 }
		};

		BitScanner128<2U> scanner{ bitString.begin(), bitString.end() };
		scanner.flipBitsInRange(192U, 64U);

		EXPECT_EQ(bitString[0], Bitmask128(0x000000000F0000F1, 0x10F0000FF0000F00));
		EXPECT_EQ(bitString[1], Bitmask128(0xFFFF0FFFF0FFFF0E, 0x10F0000FF0000000));
	}

	TEST(BitScannerTest, FlipsAFullMaskOfBits) {
		std::array<Bitmask128, 2U> bitString{
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 },
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 }
		};

		BitScanner128<2U> scanner{ bitString.begin(), bitString.end() };
		scanner.flipBitsInRange(0U, 128U);

		EXPECT_EQ(bitString[0], Bitmask128(0x0000FFFF0000FFFF, 0x0000FFFF0000FFFF));
		EXPECT_EQ(bitString[1], Bitmask128(0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000));

		scanner.flipBitsInRange(128U, 128U);

		EXPECT_EQ(bitString[0], Bitmask128(0x0000FFFF0000FFFF, 0x0000FFFF0000FFFF));
		EXPECT_EQ(bitString[1], Bitmask128(0x0000FFFF0000FFFF, 0x0000FFFF0000FFFF));
	}

	TEST(BitScannerTest, FlipsTwoFullMasksOfBits) {
		std::array<Bitmask128, 2U> bitString{
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 },
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 }
		};

		BitScanner128<2U> scanner{ bitString.begin(), bitString.end() };
		scanner.flipBitsInRange(0U, 256U);

		EXPECT_EQ(bitString[0], Bitmask128(0x0000FFFF0000FFFF, 0x0000FFFF0000FFFF));
		EXPECT_EQ(bitString[1], Bitmask128(0x0000FFFF0000FFFF, 0x0000FFFF0000FFFF));
	}

	TEST(BitScannerTest, Flips216BitsSpanningTwoMasks) {
		std::array<Bitmask128, 2U> bitString{
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 },
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 }
		};

		BitScanner128<2U> scanner{ bitString.begin(), bitString.end() };
		scanner.flipBitsInRange(32U, 216U);

		EXPECT_EQ(bitString[0], Bitmask128(0x0000FFFF0000FFFF, 0x0000FFFFFFFF0000));
		EXPECT_EQ(bitString[1], Bitmask128(0xFF00FFFF0000FFFF, 0x0000FFFF0000FFFF));
	}

	TEST(BitScannerTest, Flips320BitsSpanningThreeMasks) {
		std::array<Bitmask128, 4U> bitString{
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 },
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 },
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 },
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 }
		};

		BitScanner128<4U> scanner{ bitString.begin(), bitString.end() };
		scanner.flipBitsInRange(96U, 320U);

		EXPECT_EQ(bitString[0], Bitmask128(0x0000FFFFFFFF0000, 0xFFFF0000FFFF0000));
		EXPECT_EQ(bitString[1], Bitmask128(0x0000FFFF0000FFFF, 0x0000FFFF0000FFFF));
		EXPECT_EQ(bitString[2], Bitmask128(0x0000FFFF0000FFFF, 0x0000FFFF0000FFFF));
		EXPECT_EQ(bitString[3], Bitmask128(0xFFFF0000FFFF0000, 0xFFFF00000000FFFF));
	}

	TEST(BitScannerTest, FindsSequencesOf16SetBitsAndFlipsTheLastMatchedBits) {
		std::array<Bitmask128, 2U> bitString{
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 },
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 }
		};
		
		BitScanner128<2U> scanner{ bitString.begin(), bitString.end() };
		bool foundMatch{}; unsigned matchIndex{};
		
		foundMatch = scanner.findSequenceOfSetBits(16U, matchIndex);
		scanner.flipBitsInRange(matchIndex, 16U);
		foundMatch = scanner.findSequenceOfSetBits(16U, matchIndex);

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIndex, 48U);

		scanner.flipBitsInRange(matchIndex, 16U);
		foundMatch = scanner.findSequenceOfSetBits(16U, matchIndex);

		EXPECT_TRUE(foundMatch);
		EXPECT_EQ(matchIndex, 80U);
	}

	TEST(BitScannerTest, FlipsNoBitsWhenNumberOfBitsOverflowsTheSequence) {
		std::array<Bitmask128, 2U> bitString{
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 },
			Bitmask128{ 0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000 }
		};

		BitScanner128<2U> scanner{ bitString.begin(), bitString.end() };
		scanner.flipBitsInRange(0U, 257U);

		EXPECT_EQ(bitString[0], Bitmask128(0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000));
		EXPECT_EQ(bitString[1], Bitmask128(0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000));

		scanner.flipBitsInRange(128U, 200U);

		EXPECT_EQ(bitString[0], Bitmask128(0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000));
		EXPECT_EQ(bitString[1], Bitmask128(0xFFFF0000FFFF0000, 0xFFFF0000FFFF0000));
	}
}

}