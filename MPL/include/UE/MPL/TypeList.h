#pragma once

#include <type_traits>
#include <cstddef>
#include <tuple>

namespace Uwe {

namespace MPL {

	struct NullType {};

	namespace Detail {

		template < typename... Types >
		struct TypeList;

		template < class List >
		struct size;

		template < template< typename... > class List, typename... Types >
		struct size < List<Types...> > {
			using type = std::integral_constant < std::size_t, sizeof...(Types) >;
		};

		template < class List, std::size_t Index >
		struct getElementAtIndex {};

		template < template< typename... > class List, std::size_t Index >
		struct getElementAtIndex< List<>, Index > {
			using type = NullType;
		};

		template < template< typename... > class List, typename Head, typename... Types >
		struct getElementAtIndex< List<Head, Types...>, 0 > {
			using type = Head;
		};

		template < template< typename... > class List, typename Head, typename... Types, std::size_t Index >
		struct getElementAtIndex< List<Head, Types...>, Index> {
			using type = typename getElementAtIndex<List<Types...>, Index - 1>::type;
		};

		template < class List, typename Tail >
		struct pushBack;

		template < template< typename... > class List, typename... Types, typename Tail >
		struct pushBack< List<Types...>, Tail> {
			using type = List<Types..., Tail>;
		};

		template < typename T, std::size_t Count >
		struct repeat {
			using type = typename pushBack< typename repeat<T, Count - 1>::type, T>::type;
		};

		template < typename T >
		struct repeat<T, 0> {
			using type = TypeList<>;
		};

		template < class... Lists >
		struct concat {
			using type = TypeList<>;
		};

		template < template< typename... > class List, typename... Types >
		struct concat< List<Types...> > {
			using type = TypeList<Types...>;
		};

		template < template< typename... > class Dest, template < typename... > class Src, class... Rest, typename... TS1, typename... TS2>
		struct concat< Dest<TS1...>, Src<TS2...>, Rest... > {
			using type = typename concat < TypeList<TS1..., TS2...>, Rest...>::type;
		};

		//template < class List, template <typename> typename TypeFn >
		//struct map {
		//	using type = TypeList<>;
		//};

		//template < template< typename... > class List, typename Head, typename... Types, template <typename> typename TypeFn >
		//struct map< List<Head, Types...>, TypeFn > {
		//	using type = typename concat< List<TypeFn<Head>>, typename map< List<Types...>, TypeFn >::type >::type;
		//};

		template < class List, template <typename> typename TypeFn >
		struct map {
			using type = TypeList<>;
		};

		template < template< typename... > class List, typename Head, typename... Types, template <typename> typename TypeFn >
		struct map< List<Head, Types...>, TypeFn > {
			using type = typename concat< List<typename TypeFn<Head>::type>, typename map< List<Types...>, TypeFn >::type >::type;
		};

		template < class, template <typename> typename >
		struct filter {
			using type = TypeList<>;
		};

		template < template< typename... > class List, typename Head, typename... Types, template <typename> typename TypeFn >
		struct filter< List<Head, Types...>, TypeFn > {
			using Next = typename filter<List<Types...>, TypeFn>::type;
			using type = std::conditional_t < TypeFn<Head>{}, typename concat<List<Head>, Next>::type, Next > ;
		};

		template < class, template <typename> typename >
		struct all : std::true_type {};

		template < template< typename... > class List, typename Head, typename... Types, template <typename> typename TypeFn >
		struct all< List<Head, Types...>, TypeFn> : std::integral_constant<bool, TypeFn<Head>{} && all<List<Types...>, TypeFn>{} >
		{};

		template<class OldList, template<typename...> class NewList>
		struct rebind;

		template < template< typename... > class OldList, template< typename... > class NewList, typename... Types >
		struct rebind<OldList<Types...>, NewList> {
			using type = NewList<Types...>;
		};

		template < typename, class >
		struct indexOf {};

		template < typename T, template < typename... > class List, typename... Types >
		struct indexOf< T, List<T, Types...> >
			: std::integral_constant< std::size_t, 0 >
		{};

		template < typename T, template < typename... > class List, typename Head, typename... Types >
		struct indexOf< T, List<Head, Types...> >
			: std::integral_constant< std::size_t, 1 + indexOf<T, List<Types...>>{} >
		{};

		template < typename, class >
		struct count {};

		template < typename T, template <typename...> class List >
		struct count<T, List<>>
			: std::integral_constant< std::size_t, 0>
		{};

		template < typename T, template <typename...> class List, typename Head, typename... Types >
		struct count <T, List<Head, Types...>>
			: std::integral_constant < std::size_t, (std::is_same_v<T, Head> ? 1 : 0) + count<T, List<Types...>>{} >
		{};
	}
	
	template <typename T>
	struct Type {
		using type = T;
	};

	template <typename T>
	struct WrapType {
		using type = Type<T>;
	};

	// Interface type aliases for convenience

	template < typename... Types >
	using TypeList = Detail::TypeList< Types... >;

	template < class List >
	constexpr std::size_t size() noexcept {
		return Detail::size<List>::type::value; 
	}

	template < class List, unsigned Index >
	using getElementAtIndex = typename Detail::getElementAtIndex<List, Index>::type;

	template < class List >
	using front = getElementAtIndex<List, 0>;

	template < class List >
	using back = getElementAtIndex<List, Detail::size<List>::type::value - 1>;

	template < typename T, std::size_t Count > 
	using repeat = typename Detail::repeat<T, Count>::type;

	template < class... Lists >
	using concat = typename Detail::concat<Lists...>::type;

	template < class List, typename Tail >
	using pushBack = typename Detail::pushBack<List, Tail>::type;

	template < class List, typename T >
	using pushFront = concat<TypeList<T>, List>;

	template < class List, template <typename> typename TypeFn >
	using map = typename Detail::map<List, TypeFn>::type;

	template < class List, template <typename> typename TypeFn >
	using filter = typename Detail::filter<List, TypeFn>::type;

	template<class A, template<class...> class B>
	using rebind = typename Detail::rebind<A, B>::type;

	template < class List >
	using toTuple = rebind<List, std::tuple>;

	template < class List, template <typename> typename TypeFn >
	constexpr bool all() noexcept {
		return Detail::all<List, TypeFn>::type::value;
	}

	template < typename T, class List >
	constexpr std::size_t indexOf() noexcept {
		return Detail::indexOf<T, List>::type::value;
	}

	template < typename T, class List >
	constexpr std::size_t count() noexcept {
		return Detail::count<T, List>::type::value;
	}
}

}