#pragma once

#include "UE/EngineMacros.h"
#include "UE/MPL/TypeList.h"

#include <utility>
#include <type_traits>
#include <initializer_list>

namespace Uwe {

namespace MPL {

	namespace Detail {

		template < typename Fn, typename... Args >
		constexpr decltype(auto) forEachArg(Fn&& func, Args&&... args) {
			return (void)std::initializer_list<int>{
				(func(UWE_FORWARD(args)), 0) ...
			};
		}

		template < class Tuple, typename Fn, std::size_t... Indices >
		constexpr decltype(auto) expandCallForArgs(Tuple&& tuple, Fn&& func, std::index_sequence<Indices...>) {
			return forEachArg(UWE_FORWARD(func), std::get<Indices>(UWE_FORWARD(tuple))...);
		}

		template < class Tuple, typename Fn >
		constexpr decltype(auto) forEachTuple(Tuple&& tuple, Fn&& func) noexcept {
			using Indices = std::make_index_sequence<
				std::tuple_size_v<std::decay_t<Tuple>>
			>;

			return expandCallForArgs(UWE_FORWARD(tuple), UWE_FORWARD(func), Indices{});
		}

		template < class List, typename Fn >
		constexpr void forEachType(Fn&& func) noexcept {
			Detail::forEachTuple(
				toTypeTuple<List>{},
				UWE_FORWARD(func)
			);
		}


	}



	template < class List >
	using toTypeTuple = MPL::toTuple< MPL::map<List, MPL::WrapType> >;

	template < class Tuple, typename Fn >
	constexpr void forEachTuple(Tuple&& tuple, Fn&& func) noexcept {
		return Detail::forEachTuple(UWE_FORWARD(tuple), UWE_FORWARD(func));
	}

	template < class List, typename Fn >
	constexpr void forEachType(Fn&& func) noexcept {
		return Detail::forEachType<List>(UWE_FORWARD(func));
	}



}

}