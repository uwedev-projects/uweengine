#ifndef UWE_ENGINE_MARCROS_H
#define UWE_ENGINE_MARCROS_H

#include <utility>

#define UWE_CONCAT_TOKENS(t1, t2) t1##t2
#define UWE_CONCAT_TRIPLE(t1, t2, t3) t1##t2##t3
	
#define UWE_FORWARD(arg) std::forward<decltype(arg)>(arg)

#endif
