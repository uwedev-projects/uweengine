#include "gtest/gtest.h"

#include "UE/MPL/TypeList.h"
#include "UE/MPL/ForEach.h"

#include <type_traits>
#include <typeinfo>
#include <iostream>
#include <tuple>


namespace Uwe {

namespace MPL {

	TEST(MetaForEachTest, InvokesACallbackForEachType) {
		using MyTypes = TypeList<int, char, float>;
		int callCount{};

		MPL::forEachType<MyTypes>([&callCount](const auto& t) {
			const auto& typeInfo{ typeid(decltype(t)) };

			switch (callCount) {
			case 0:
				EXPECT_STREQ(typeInfo.name(), "struct Uwe::MPL::Type<int>");
				break;
			case 1:
				EXPECT_STREQ(typeInfo.name(), "struct Uwe::MPL::Type<char>");
				break;
			case 2:
				EXPECT_STREQ(typeInfo.name(), "struct Uwe::MPL::Type<float>");
				break;
			default:
				ADD_FAILURE() << "Callback was called more than three times.";
				break;
			}

			++callCount;
		});

	}

	TEST(MetaForEachTest, MapsTypeListToTypeTuple) {
		using MyTypes = TypeList<int, char, float>;
		using TypeTuple = toTypeTuple<MyTypes>;

		static_assert(std::is_same_v<TypeTuple, std::tuple<MPL::Type<int>, MPL::Type<char>, MPL::Type<float>> >, "Failed to map type list to type tuple.");
	}

	TEST(MetaForEachTest, InvokesACallbackForEachTypeAtCompileTime) {
		auto constexprCallback = [](auto t) {
			static_assert(
				std::is_same_v<decltype(t), Type<int>> || 
				std::is_same_v<decltype(t), Type<char>> || 
				std::is_same_v<decltype(t), Type<float>>, 
				"Constexpr callback was called with invalid arguments."
			);
		};

		using MyTypes = TypeList<int, char, float>;
		static_assert((static_cast<void>(MPL::forEachType<MyTypes>(constexprCallback)), true), "");
	}

	TEST(MetaForEachTest, InvokesACallbackForEachTupleElement) {
		using MyTuple = std::tuple<int, char, bool>;
		MyTuple tuple{ 1, 'C', false };

		int callCount{};
		MPL::forEachTuple(tuple, [&callCount](const auto& arg) {
			switch (callCount) {
			case 0:
				EXPECT_EQ(arg, 1);
				break;
			case 1:
				EXPECT_EQ(arg, 'C');
				break;
			case 2:
				EXPECT_FALSE(arg);
				break;
			default:
				ADD_FAILURE() << "Callback was called more than three times.";
				break;
			}
			++callCount;
		});
	}

}

}