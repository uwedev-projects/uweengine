#include "gtest/gtest.h"

#include "UE/MPL/TypeList.h"

#include <type_traits>
#include <string>


namespace Uwe {

namespace MPL {
	TEST(TypeListTest, CalculatesTheSizeOfTheList) {
		using MyTypes = TypeList<int, int, char, bool>;
		constexpr auto ListSize = MPL::size<MyTypes>();
		static_assert(ListSize == 4, "Typelist has invalid size");
	}

	TEST(TypeListTest, FrontReturnsTheFirstElement) {
		using MyTypes = TypeList<double, bool, char>;
		using First = MPL::front<MyTypes>;
		static_assert(std::is_same_v<First, double>, "Invalid type returned by front");
	}

	TEST(TypeListTest, FrontWithEmptyList) {
		using First = MPL::front<TypeList<>>;
		static_assert(std::is_same_v<First, MPL::NullType>, "Front on empty list cannot return a valid type");
	}

	TEST(TypeListTest, BackReturnsTheLastElement) {
		using MyTypes = TypeList<int, char, int, bool>;
		using Last = MPL::back<MyTypes>;
		static_assert(std::is_same_v<Last, bool>, "Invalid type returned by back");

	}

	TEST(TypeListTest, BackWithEmptyList) {
		using Last = MPL::back<TypeList<>>;
		static_assert(std::is_same_v<Last, MPL::NullType>, "Back on empty list cannot return a valid type");
	}

	TEST(TypeListTest, GetsTheElementAtTheSpecifiedIndex) {
		using MyTypes = TypeList<int, int, char, double>;
		static_assert(std::is_same_v < getElementAtIndex<MyTypes, 0>, int>, "Invalid type at index 0");
		static_assert(std::is_same_v < getElementAtIndex<MyTypes, 1>, int>, "Invalid type at index 1");
		static_assert(std::is_same_v < getElementAtIndex<MyTypes, 2>, char>, "Invalid type at index 2");
		static_assert(std::is_same_v < getElementAtIndex<MyTypes, 3>, double>, "Invalid type at index 3");
	}

	TEST(TypeListTest, ReturnsNullTypeForEmptyList) {
		static_assert(std::is_same_v<MPL::getElementAtIndex<TypeList<>, 0>, MPL::NullType>, "An empty list cannot return a type");
		static_assert(std::is_same_v<MPL::getElementAtIndex<TypeList<>, 1>, MPL::NullType>, "An empty list cannot return a type");
		static_assert(std::is_same_v<MPL::getElementAtIndex<TypeList<>, 2>, MPL::NullType>, "An empty list cannot return a type");
		static_assert(std::is_same_v<MPL::getElementAtIndex<TypeList<>, 3>, MPL::NullType>, "An empty list cannot return a type");
	}

	TEST(TypeListTest, PushesAnElementToTheBackOfTheList) {
		using List1 = MPL::pushBack< TypeList<>, char >;
		static_assert(std::is_same_v<MPL::back<List1>, char>, "Failed to push back element into the list");

		using List2 = MPL::pushBack< List1, int >;
		static_assert(std::is_same_v<MPL::back<List2>, int>, "Failed to push back element into the list");

		using List3 = MPL::pushBack< List2, long >;
		static_assert(std::is_same_v<MPL::back<List3>, long>, "Failed to push back element into the list");
	}
	TEST(TypeListTest, PushesAnElementToTheFrontOfTheList) {
		using List1 = MPL::pushFront< TypeList<>, void* >;
		static_assert(std::is_same_v<MPL::getElementAtIndex<List1, 0>, void*>, "Failed to push an element to the front of the list");

		using List2 = MPL::pushFront< TypeList<char, int>, int* >;
		static_assert(std::is_same_v<MPL::getElementAtIndex<List2, 0>, int*>, "Failed to push an element to the front of the list");
	}

	TEST(TypeListTest, RepeatsATypeAndReturnsANewList) {
		using MyTypes = MPL::repeat<long, 4>;
		static_assert(MPL::size<MyTypes>() == 4, "Failed to repeat long type four times");
		 
		static_assert(std::is_same_v<MPL::getElementAtIndex<MyTypes, 0>, long>, "Failed to repeat long type 4 times");
		static_assert(std::is_same_v<MPL::getElementAtIndex<MyTypes, 1>, long>, "Failed to repeat long type 4 times");
		static_assert(std::is_same_v<MPL::getElementAtIndex<MyTypes, 2>, long>, "Failed to repeat long type 4 times");
		static_assert(std::is_same_v<MPL::getElementAtIndex<MyTypes, 3>, long>, "Failed to repeat long type 4 times");
	}

	TEST(TypeListTest, RepeatingZeroTimesReturnsEmptyList) {
		using MyTypes = MPL::repeat<int, 0>;
		static_assert(MPL::size<MyTypes>() == 0, "Failed to repeat type zero times");
	}

	TEST(TypeListTest, ConcatenatesMultipleTypeLists) {
		using List1 = TypeList<int, float>;
		using List2 = TypeList<double>;
		using List3 = TypeList<char>;

		using CombinedList = MPL::concat<List1, List2, List3>;
		static_assert(MPL::size<CombinedList>() == 4, "Failed to concatenate multiple type lists");
		static_assert(std::is_same_v<MPL::getElementAtIndex<CombinedList, 0>, int>, "Failed to concatenate multiple type lists");
		static_assert(std::is_same_v<MPL::getElementAtIndex<CombinedList, 1>, float>, "Failed to concatenate multiple type lists");
		static_assert(std::is_same_v<MPL::getElementAtIndex<CombinedList, 2>, double>, "Failed to concatenate multiple type lists");
		static_assert(std::is_same_v<MPL::getElementAtIndex<CombinedList, 3>, char>, "Failed to concatenate multiple type lists");
	}

	TEST(TypeListTest, ConcatenatesEmptyLists) {
		using CombinedList = MPL::concat<TypeList<>, TypeList<>>;
		static_assert(MPL::size<CombinedList>() == 0, "Failed to concatenate two empty type lists");
	}

	TEST(TypeListTest, ConcatenatesEmptyWithNonEmptyList) {
		using CombinedList1 = MPL::concat<TypeList<>, TypeList<int>>;
		static_assert(MPL::size<CombinedList1>() == 1, "Failed to concatenate non empty type list with empty type list");
		static_assert(std::is_same_v<MPL::getElementAtIndex<CombinedList1, 0>, int>, "Failed to concatenate non empty type list with empty type list");

		using CombinedList2 = MPL::concat<TypeList<int>, TypeList<>>;
		static_assert(MPL::size<CombinedList2>() == 1, "Failed to concatenate non empty type list with empty type list");
		static_assert(std::is_same_v<MPL::getElementAtIndex<CombinedList2, 0>, int>, "Failed to concatenate non empty type list with empty type list");
	}

	TEST(TypeListTest, MapsOneListToAnother) {
		using InputList = TypeList<int, char, double>;
		using MappedList = MPL::map<InputList, std::add_const>;
		
		static_assert(std::is_same_v< MPL::getElementAtIndex<MappedList, 0>, const int>, "Expected const int at index 0");
		static_assert(std::is_same_v< MPL::getElementAtIndex<MappedList, 1>, const char>, "Expected const char at index 1");
		static_assert(std::is_same_v< MPL::getElementAtIndex<MappedList, 2>, const double>, "Expected const double at index 2");
	}

	TEST(TypeListTest, MapsEmptyListToEmptyList) {
		using MappedList = MPL::map<TypeList<>, std::add_const>;
		static_assert(std::is_same_v < MappedList, TypeList<> >, "Mapping an empty list must result in an empty list.");
	}

	template <typename T>
	struct MyTypeFunction {
		using type = MPL::Type <T>;
	};

	struct TypeA {};
	struct TypeB {};
	struct TypeC {};

	TEST(TypeListTest, MapsOneListWithCustomTypeFunction) {
		using MyTypes = TypeList<TypeA, TypeB, TypeC>;
		using WrappedTypes = MPL::map<MyTypes, MyTypeFunction>;

		static_assert(std::is_same_v< WrappedTypes, TypeList<MPL::Type<TypeA>, MPL::Type<TypeB>, MPL::Type<TypeC>> >, "Failed to map typelist with custom type function.");
	}

	TEST(TypeListTest, FiltersListByPredicate) {
		using MyTypes = TypeList<int, const int, double, char* const>;
		using ConstTypes = MPL::filter<MyTypes, std::is_const>;

		static_assert(MPL::size<ConstTypes>() == 2, "Failed to filter type list");
		static_assert(std::is_same_v< MPL::getElementAtIndex<ConstTypes, 0>, const int>, "Expected const int at index 0");
		static_assert(std::is_same_v< MPL::getElementAtIndex<ConstTypes, 1>, char* const>, "Expected const char* at index 1");
	}

	TEST(TypeListTest, ReturnsTheIndexOfAType) {
		using MyTypes = TypeList<int*, bool, char, void>;

		static_assert(MPL::indexOf<int*, MyTypes>() == 0, "Expected index of 0 for int*");
		static_assert(MPL::indexOf<bool, MyTypes>() == 1, "Expected index of 1 for bool");
		static_assert(MPL::indexOf<char, MyTypes>() == 2, "Expected index of 2 for char");
		static_assert(MPL::indexOf<void, MyTypes>() == 3, "Expected index of 3 for void");
	}

	TEST(TypeListTest, ReturnsTheCountOfACertainType) {
		using MyTypes = TypeList<char, int, char, char, int, bool>;

		static_assert(MPL::count<char, MyTypes>() == 3, "Expected a count of 3 for char");
		static_assert(MPL::count<int, MyTypes>() == 2, "Expected a count of 2 for int");
		static_assert(MPL::count<bool, MyTypes>() == 1, "Expected a count of 1 for bool");
		static_assert(MPL::count<void, MyTypes>() == 0, "Expected a count of 0 for void");
	}

	template <typename... Types>
	struct NewTypeList {};

	TEST(TypeListTest, RebindsTemplateParamsToAnotherTypeList) {
		using NewList = MPL::rebind<TypeList<bool, char>, NewTypeList>;
		static_assert(std::is_same_v < NewList, NewTypeList<bool, char> >, "Renaming of typelist failed. Expected NewTypeList<bool, char>");
	}

	TEST(TypeListTest, ConvertsTypeListToTuple) {
		using MyTypes = TypeList<float, double, char>;
		using Converted = MPL::toTuple<MyTypes>;
		static_assert(std::is_same_v < Converted, std::tuple<float, double, char> >, "Failed to convert type list to std::tuple.");
	}

	TEST(TypeListTest, ReturnsTrueIfAllTypesSatisfyThePredicate) {
		using MyTypes = TypeList<int*, const bool*, const char*>;
		static_assert(MPL::all<MyTypes, std::is_pointer>(), "Expected type predicate to succeed for type list.");
	}

	TEST(TypeListTest, ReturnsFalseIfOneTypeFailsThePredicate) {
		using MyTypes = TypeList<int, const bool, const char>;
		static_assert(!MPL::all<MyTypes, std::is_const>(), "Expected type predicate to fail for type list.");
	}

	struct ComponentTag {};

	template <typename T>
	constexpr bool isComponent() {
		//return std::is_base_of<
	}

	TEST(TypeListTest, AllTest) {

	}

}

}