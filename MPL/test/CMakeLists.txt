cmake_minimum_required (VERSION 3.12)

include(GoogleTest)

find_package (UweMPL REQUIRED)

add_executable (UweMPLTest 
	main.cpp 
)

target_compile_features (UweMPLTest
	PRIVATE
		cxx_std_17
)

target_include_directories (UweMPLTest 
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}
)

target_sources (UweMPLTest 
	PRIVATE 
		${CMAKE_CURRENT_SOURCE_DIR}/TypeListTest.cpp
		${CMAKE_CURRENT_SOURCE_DIR}/ForEachTest.cpp
 )


target_link_libraries (UweMPLTest 
	PRIVATE
		Uwe::MPL
		gmock_main
)

gtest_discover_tests(UweMPLTest)
