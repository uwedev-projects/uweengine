#pragma once

#include "UE/Memory/Block.h"

#include <stdexcept>
#include <utility>

namespace Uwe {

namespace Memory {

	template <typename T, class Alloc, class Pool>
	class STLAllocator {
	public:

		using size_type = size_t;
		using difference_type = ptrdiff_t;
		using pointer = T*;
		using const_pointer = const T*;
		using reference = T&;
		using const_reference = const T&;
		using value_type = T;

		Alloc& allocator;

		STLAllocator()
			: allocator{ Pool::getAllocatorInstance<T>() }
		{
		}

		~STLAllocator() = default;

		STLAllocator(const STLAllocator&)
			: allocator{ Pool::getAllocatorInstance<T>() }
		{
		}

		template <typename U, typename Alloc> 
		explicit STLAllocator(STLAllocator<U, Alloc, Pool> const& other)  
			: allocator{ Pool::getAllocatorInstance<U>() }
		{
		}

		T* allocate(std::size_t n) {
			//std::cout << "STLAllocator:: allocating " << n << " element(s)." << std::endl;

			Block blk{ allocator.allocate(n * sizeof(T)) };
			
			if (!blk)
				throw std::bad_alloc{};

			return reinterpret_cast<T*>(blk.ptr);
		}

		void deallocate(void* ptr, std::size_t n) noexcept {
			//std::cout << "STLAllocator:: deallocating " << n << "element(s)" << std::endl;

			Block blk{ reinterpret_cast<std::byte*>(ptr), n * sizeof(T) };
			allocator.deallocate(blk);
		}

		void construct(T* ptr) {
			::new (static_cast<void*>(ptr)) T{};
		}

		void construct(T* ptr, T&& val) {
			::new (static_cast<void*>(ptr)) T{ std::forward<T>(val) };
		}

		template <typename... Args>
		void construct(T* ptr, Args&&... args) {
			::new (static_cast<void*>(ptr)) T{ std::forward<Args>(args)... };
		}

		template <class U> 
		void construct(T* ptr, U&& val)
		{
			::new (static_cast<void*>(ptr)) T{ std::forward<U>(val) };
		};

		void destroy(T* ptr) {
			ptr->T::~T();
		}
	};

	//template <class T, class U, class Alloc>
	//bool
	//	operator==(STLAllocator<T, Alloc> const&, STLAllocator<U, Alloc> const&) noexcept
	//{
	//	return true;
	//}

	//template <class T, class U, class Alloc>
	//bool
	//	operator!=(STLAllocator<T, Alloc> const& x, STLAllocator<U, Alloc> const& y) noexcept
	//{
	//	return !(x == y);
	//}
}

}