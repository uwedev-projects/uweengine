#pragma once

#include "UE/Memory/Block.h"

#include <limits>

namespace Uwe {

namespace Memory {

	class Mallocator {
	public:
		static constexpr std::size_t alignment = 4U;
		static constexpr std::size_t capacity  = std::numeric_limits<std::size_t>::max();

		Block allocate(std::size_t size) noexcept;
		bool reallocate(Block& blk, std::size_t desiredSize) noexcept;
		void deallocate(Block& blk) noexcept;

	};

}

}