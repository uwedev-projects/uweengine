#pragma once

#include <cstddef>

namespace Uwe {

namespace Memory {

	struct Block {
		std::byte* ptr;
		std::size_t length;

		Block() noexcept;
		Block(const Block&) noexcept = default;
		Block& operator=(const Block&) noexcept = default;
		Block(Block&&) noexcept; 
		Block& operator=(Block&&) noexcept;

		constexpr Block(std::byte* ptr, std::size_t length) noexcept;

		void reset() noexcept;

		explicit operator bool() const;
		bool operator==(const Block&) const;
	};

	constexpr Block::Block(std::byte* ptr, std::size_t length) noexcept
		: ptr{ ptr }, length{ length }
	{
	}

	constexpr std::size_t roundToAlignment(std::size_t size, std::size_t alignment) noexcept {
		std::size_t remainder{ size & (alignment - 1) };
		return size + (remainder > 0) * (alignment - remainder);
	}

	void copyBlock(const Block& source, Block& destination) noexcept;

}

}