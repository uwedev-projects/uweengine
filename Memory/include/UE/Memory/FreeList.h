#pragma once

#include "UE/Memory/Block.h"
#include "UE/Memory/Reallocator.h"

#include <cstddef>
#include <array>

namespace Uwe {

namespace Memory {

	template <class Alloc, std::size_t Lower, std::size_t Upper, std::size_t Limit, std::size_t BatchSize = 16>
	class FreeList {
	public:

		static constexpr std::size_t alignment	= Alloc::alignment;
		static constexpr std::size_t capacity	= Alloc::capacity;

		static constexpr std::size_t minSize	= Lower;
		static constexpr std::size_t blockSize	= Upper;
		static constexpr std::size_t limit		= Limit;
		static constexpr std::size_t batchSize  = BatchSize;

		static_assert(blockSize >= sizeof(void*), "Upper limit must be at least of pointer size.");
		static_assert(limit >= batchSize, "Limit cannot be lower than batch size.");

		FreeList();
		~FreeList();

		FreeList(const FreeList&) = delete;
		FreeList& operator=(const FreeList&) = delete;

		FreeList(FreeList&&) = default;
		FreeList& operator=(FreeList&&) = default;

		Block allocate(std::size_t size) noexcept;
		void deallocate(Block& blk) noexcept;
		bool reallocate(Block& blk, std::size_t desiredSize) noexcept;

		bool owns(const Block& blk) const noexcept;

	protected:
		struct FreeNode { 
			FreeNode* next; 
		};

		Alloc parent;
		std::array<std::byte*, limit / batchSize> batchBlocks;
		FreeNode* head;
		std::size_t length;
		std::size_t numBatchBlocks;

		bool isBeginningOfBatchBlock(std::byte* ptr) const noexcept;
	};

	template <class Alloc, std::size_t Lower, std::size_t Upper, std::size_t Limit, std::size_t BatchSize>
	FreeList<Alloc, Lower, Upper, Limit, BatchSize>::FreeList()
		: parent{}, batchBlocks{}, head{}, length{}, numBatchBlocks{} {

	}

	template <class Alloc, std::size_t Lower, std::size_t Upper, std::size_t Limit, std::size_t BatchSize>
	FreeList<Alloc, Lower, Upper, Limit, BatchSize>::~FreeList() {
		FreeNode* current{ head };

		while (current != nullptr) {
			Block blk{ reinterpret_cast<std::byte*>(current), blockSize };
			current = current->next;

			if (isBeginningOfBatchBlock(reinterpret_cast<std::byte*>(blk.ptr)) || batchSize == 1)
				parent.deallocate(blk);
		}
	}

	template <class Alloc, std::size_t Lower, std::size_t Upper, std::size_t Limit, std::size_t BatchSize>
	Block FreeList<Alloc, Lower, Upper, Limit, BatchSize>::allocate(std::size_t size) noexcept {

		if (size >= minSize && size <= blockSize) {

			if (head) {
				Block blk = { reinterpret_cast<std::byte*>(head), size };
				head = head->next;
				--length;

				return blk;
			}
			else {

				Block batchBlock{ parent.allocate(blockSize * batchSize) };
				length += batchSize - 1;

				for (std::size_t i{ 1 }; i < batchSize - 1; ++i) {
					
					FreeNode* node{ reinterpret_cast<FreeNode*>(batchBlock.ptr + blockSize * i) };
					node->next = reinterpret_cast<FreeNode*>(batchBlock.ptr + blockSize * (i+1));
				}

				FreeNode* last{ reinterpret_cast<FreeNode*>(batchBlock.ptr + blockSize * (batchSize - 1)) };
				last->next = nullptr;

				if (batchSize > 1) {
					batchBlocks[numBatchBlocks++] = batchBlock.ptr;
					head = reinterpret_cast<FreeNode*>(batchBlock.ptr + blockSize);
				}

				return Block{ batchBlock.ptr, blockSize };
			}

		}

		return parent.allocate(size);
	}

	template <class Alloc, std::size_t Lower, std::size_t Upper, std::size_t Limit, std::size_t BatchSize>
	void FreeList<Alloc, Lower, Upper, Limit, BatchSize>::deallocate(Block& blk) noexcept {

		if (length < limit) {
			FreeNode* node{ reinterpret_cast<FreeNode*>(blk.ptr) };

			node->next = head;
			head = node;
			++length;
		}
		else {
			parent.deallocate(blk);
		}

	}

	template <class Alloc, std::size_t Lower, std::size_t Upper, std::size_t Limit, std::size_t BatchSize>
	inline bool FreeList<Alloc, Lower, Upper, Limit, BatchSize>::reallocate(Block& blk, std::size_t desiredSize) noexcept {
		if (desiredSize > blockSize)
			return false;

		return Detail::reallocateBlockTrivially(*this, blk, desiredSize);
	}

	template <class Alloc, std::size_t Lower, std::size_t Upper, std::size_t Limit, std::size_t BatchSize>
	inline bool FreeList<Alloc, Lower, Upper, Limit, BatchSize>::owns(const Block& blk) const noexcept {
		return blk && blk.length >= minSize && blk.length <= blockSize;
	}

	template <class Alloc, std::size_t Lower, std::size_t Upper, std::size_t Limit, std::size_t BatchSize>
	bool FreeList<Alloc, Lower, Upper, Limit, BatchSize>::isBeginningOfBatchBlock(std::byte* ptr) const noexcept {
		for (std::size_t i{}; i < numBatchBlocks; ++i) {
			if (batchBlocks[i] == ptr)
				return true;
		}

		return false;
	}

}

}