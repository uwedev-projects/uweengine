#pragma once

#include "UE/Memory/Block.h"
#include "UE/Memory/MemTraits.h"

#include <cstddef>
#include <type_traits>

namespace Uwe {

namespace Memory {

	namespace Detail {

		template <class Alloc, typename Enabled = void> struct Reallocator;

		template <class NewAlloc, class OldAlloc>
		bool reallocateBlockWithCopy(OldAlloc& oldAllocator, NewAlloc& newAllocator, Block& blk, std::size_t desiredSize) {
			Block newBlock{ newAllocator.allocate(desiredSize) };

			if (newBlock) {
				copyBlock(blk, newBlock);
				oldAllocator.deallocate(blk);
				blk = newBlock;

				return true;
			}

			return false;
		}

		// Reallocate with expand
		template <class Alloc>
		struct Reallocator<Alloc,
			typename std::enable_if_t < MemTraits::hasExpand<Alloc>::value> > {

			static bool handleTrivialReallocation(Alloc& allocator, Block& blk, std::size_t desiredSize) {
				if (desiredSize > blk.length)
					return allocator.expand(blk, desiredSize - blk.length);

				if (desiredSize == 0) {
					allocator.deallocate(blk);
					return true;
				}

				if (blk.length == desiredSize)
					return true;

				if (!blk) {
					blk = allocator.allocate(desiredSize);
					return static_cast<bool>(blk);
				}

				return false;
			}
		};

		// Reallocate without expand
		template <class Alloc>
		struct Reallocator<Alloc,
			typename std::enable_if_t < !MemTraits::hasExpand<Alloc>::value> > {

			static bool handleTrivialReallocation(Alloc& allocator, Block& blk, std::size_t desiredSize) {
				if (desiredSize == 0) {
					allocator.deallocate(blk);
					return true;
				}

				if (desiredSize == blk.length)
					return true;

				if (!blk) {
					blk = allocator.allocate(desiredSize);
					return static_cast<bool>(blk);
				}

				return false;
			}
		};

		template <class Alloc>
		inline bool reallocateBlockTrivially(Alloc& allocator, Block& blk, std::size_t desiredSize) {
			return Detail::Reallocator<Alloc>::handleTrivialReallocation(allocator, blk, desiredSize);
		}

	}

}

}