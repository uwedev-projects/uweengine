#pragma once

#include "UE/Memory/Block.h"

#include <cstddef>
#include <cstdlib>
#include <limits>

namespace Uwe {

namespace Memory {

	template <std::size_t Alignment = 16>
	class AlignedMallocator {
	public:
		static constexpr std::size_t alignment = Alignment;
		static constexpr std::size_t capacity = std::numeric_limits<std::size_t>::max();

		Block allocate(std::size_t size) noexcept;
		bool reallocate(Block& blk, std::size_t desiredSize) noexcept;
		void deallocate(Block& blk) noexcept;

	protected:

		bool reallocateAligned(Block& blk, std::size_t desiredSize) noexcept;

	};

	template <std::size_t Alignment>
	constexpr std::size_t AlignedMallocator<Alignment>::alignment;

	template <std::size_t Alignment>
	Block AlignedMallocator<Alignment>::allocate(std::size_t size) noexcept {
		Block result{};

		if (!size)
			return result;

		#ifdef _WIN32
			auto blockAddress = _aligned_malloc(size, alignment);
		#else
			auto blockAddress = (void*)memalign(alignment, size);
		#endif

		if (blockAddress) {
			result.ptr = static_cast<std::byte*>(blockAddress);
			result.length = size;
		}

		return result;
	}

	template <std::size_t Alignment>
	void AlignedMallocator<Alignment>::deallocate(Block& blk) noexcept {
		if (blk) {

		#ifdef _WIN32
			_aligned_free(blk.ptr);
		#else
			std::free(blk.ptr);
		#endif

			blk.reset();
		}
	}

	template <std::size_t Alignment>
	bool AlignedMallocator<Alignment>::reallocate(Block& blk, std::size_t desiredSize) noexcept {
		return reallocateAligned(blk, desiredSize);
	}

	#ifdef _WIN32

	template <std::size_t Alignment>
	bool AlignedMallocator<Alignment>::reallocateAligned(Block& blk, std::size_t desiredSize) noexcept {
		bool result{};

		void* blockAddress{ _aligned_realloc(blk.ptr, desiredSize, alignment) };
		Block newBlock{ static_cast<std::byte*>(blockAddress), desiredSize };

		if (newBlock) {
		
			blk = newBlock;
			result = true;

		}

		return result;
	}

	#else

	template <std::size_t Alignment>
	bool AlignedMallocator<Alignment>::reallocateAligned(Block& blk, std::size_t desiredSize) noexcept {
		bool result{};

		Block newBlock{ static_cast<std::byte*>(std::realloc(blk.ptr, desiredSize)), desiredSize };

		if (newBlock) {

			if (newBlock.ptr & (alignment - 1) != 0) {
				// newBlock is not aligned, so we do reallocation by hand
				Block alignedBlock{ allocate(desiredSize) };

				if (alignedBlock) {
					copyBlock(newBlock, alignedBlock);

					blk = alignedBlock;
					result = true;
				}
				
				deallocate(newBlock);
			}
			else {
				result = true;
			}
		}

		return result;
	}

	#endif

}

}