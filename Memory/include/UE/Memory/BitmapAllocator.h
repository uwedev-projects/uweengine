#pragma once

#include "UE/Memory/Block.h"

#include "UE/Platform/Bitmask.h"
#include "UE/Platform/BitScan.h"

#include <algorithm>
#include <cstddef>
#include <array>

namespace Uwe {

namespace Memory {

	using Platform::Bitmask128;

	constexpr std::size_t numberOfBitmaps(std::size_t blockCount) {
		const std::size_t extraBlock{ static_cast<std::size_t>(blockCount % (sizeof(Bitmask128) * CHAR_BIT) != 0ULL) };
		return std::max<std::size_t>(blockCount / (sizeof(Bitmask128) * CHAR_BIT) + extraBlock, 1ULL);
	}

	template <class Alloc, std::size_t BlockSize, std::size_t BlockCount>
	class BitmapAllocator {

	public:

		static constexpr const std::size_t capacity{ BlockCount * BlockSize };
		static constexpr const std::size_t alignment{ Alloc::alignment };
		static constexpr const std::size_t blockSize{ roundToAlignment(BlockSize, alignment) };
		static constexpr const std::size_t blockCount{ BlockCount };

		BitmapAllocator();
		~BitmapAllocator();

		Block allocate(std::size_t size) noexcept;
		void deallocate(Block& blk) noexcept;

		Block allocateArray(std::size_t size, const unsigned elementCount);
		void deallocateArray(Block& arr, const unsigned elementCount);

		void deallocateAll() noexcept;

		bool reallocate(Block& blk, std::size_t desiredSize) noexcept;
		bool expand(Block& blk, std::size_t desiredSize) noexcept;

		bool owns(const Block& blk) const noexcept;

		std::size_t bytesRemaining() const noexcept;
		std::size_t currentSize() const noexcept;

	protected:

		static constexpr const std::size_t maskWidth{ sizeof(Bitmask128) * CHAR_BIT };
		static constexpr const std::size_t mappedSize{ blockSize * maskWidth };
		static constexpr const std::size_t bitmapCount{ numberOfBitmaps(blockCount) };

		using BitmapArray = std::array<Bitmask128, bitmapCount>;
		using IndexPair = std::pair<unsigned long, unsigned long>;
		using BitScanner = Platform::BitScanner<typename BitmapArray::iterator>;

		BitmapArray bitmaps;
		BitScanner scanner;
		std::size_t blocksInUse;
		Block arena;
		Alloc parent;

		std::byte* indexToAddress(unsigned long bitmapIndex, unsigned long bitIndex) const noexcept;
		std::byte* indexToAddress(const IndexPair& indices) const noexcept;
		bool addressToIndex(std::byte* address, IndexPair& result) const noexcept;

	};

	template <class Alloc, std::size_t BlockSize, std::size_t BlockCount>
	BitmapAllocator<Alloc, BlockSize, BlockCount>::BitmapAllocator()
		: bitmaps{}, scanner{bitmaps.begin(), bitmaps.end()}, blocksInUse{}, arena{}, parent{}
	{
		arena = parent.allocate(blockCount * blockSize);
		std::fill_n(bitmaps.begin(), bitmaps.size(), Bitmask128::max());
	}

	template <class Alloc, std::size_t BlockSize, std::size_t BlockCount>
	BitmapAllocator<Alloc, BlockSize, BlockCount>::~BitmapAllocator()
	{
		parent.deallocate(arena);
	}

	template <class Alloc, std::size_t BlockSize, std::size_t BlockCount>
	Block BitmapAllocator<Alloc, BlockSize, BlockCount>::allocate(std::size_t size) noexcept {
		Block block{};

		if (size <= blockSize && blocksInUse < blockCount) {

			unsigned long bitIndex{}, bitmapIndex{};
			bool slotFound{};

			for (; !slotFound && bitmapIndex < bitmaps.size(); bitmapIndex += !slotFound) {
				slotFound = bitmaps[bitmapIndex].findFirstSetBit(bitIndex);
			}

			if (slotFound) {
				bitmaps[bitmapIndex] ^= (Bitmask128{ 1ULL } << bitIndex);
				++blocksInUse;
			}

			block.ptr = indexToAddress(bitmapIndex, bitIndex);
			block.length = blockSize;

		}

		return block;
	};

	template <class Alloc, std::size_t BlockSize, std::size_t BlockCount>
	Block BitmapAllocator<Alloc, BlockSize, BlockCount>::allocateArray(std::size_t size, const unsigned elementCount) {
		Block block{};

		unsigned absoluteIndex{};
		bool slotsFound{ scanner.findSequenceOfSetBits(elementCount, absoluteIndex) };

		if (slotsFound) {

			IndexPair indices{ std::make_pair(
				absoluteIndex / static_cast<unsigned long>(maskWidth),
				absoluteIndex & (static_cast<unsigned long>(maskWidth) - 1UL)
			) };

			block.ptr		= indexToAddress(indices);
			block.length	= blockSize * elementCount;

			scanner.flipBitsInRange(absoluteIndex, elementCount);
			blocksInUse += elementCount;
		}

		return block;
	};

	template <class Alloc, std::size_t BlockSize, std::size_t BlockCount>
	void BitmapAllocator<Alloc, BlockSize, BlockCount>::deallocate(Block& blk) noexcept {
		IndexPair indices{};

		if (addressToIndex(blk.ptr, indices)) {
			const auto[bitmapIndex, bitIndex] = indices;
			bitmaps[bitmapIndex] |= (Bitmask128{ 1ULL } << bitIndex);
			blk.reset();
			--blocksInUse;
		}
	}

	template <class Alloc, std::size_t BlockSize, std::size_t BlockCount>
	void BitmapAllocator<Alloc, BlockSize, BlockCount>::deallocateArray(Block& arr, const unsigned elementCount) {

		IndexPair indices{};
		bool foundSlot{ addressToIndex(arr.ptr, indices) };

		if (foundSlot) {

			scanner.flipBitsInRange(indices.first * maskWidth + indices.second, elementCount);
			blocksInUse -= elementCount;
			arr.reset();

		}

	}

	template <class Alloc, std::size_t BlockSize, std::size_t BlockCount>
	void BitmapAllocator<Alloc, BlockSize, BlockCount>::deallocateAll() noexcept {
		std::for_each(bitmaps.begin(), bitmaps.end(), [](Bitmask128& bitmap) {
			bitmap = Bitmask128::min();
		});

		blocksInUse = 0ULL;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t BlockCount>
	inline bool BitmapAllocator<Alloc, BlockSize, BlockCount>::reallocate(Block& blk, std::size_t desiredSize) noexcept {
		return false;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t BlockCount>
	inline bool BitmapAllocator<Alloc, BlockSize, BlockCount>::expand(Block& blk, std::size_t desiredSize) noexcept {
		return false;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	inline std::byte* BitmapAllocator<Alloc, BlockSize, NumBlocks>::indexToAddress(unsigned long bitmapIndex, unsigned long bitIndex) const noexcept {
		return arena.ptr + bitmapIndex * mappedSize + bitIndex * blockSize;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	inline std::byte* BitmapAllocator<Alloc, BlockSize, NumBlocks>::indexToAddress(const IndexPair& indices) const noexcept {
		return indexToAddress(indices.first, indices.second);
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	inline bool BitmapAllocator<Alloc, BlockSize, NumBlocks>::addressToIndex(std::byte* address, IndexPair& result) const noexcept {
		const auto pointerDelta{ address - arena.ptr };

		if (pointerDelta < 0 || pointerDelta > capacity)
			return false;

		
		result = std::make_pair(
			static_cast<unsigned long>(pointerDelta / mappedSize),
			static_cast<unsigned long>((pointerDelta / blockSize) & (maskWidth - 1ULL))
		);

		return true;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	inline std::size_t BitmapAllocator<Alloc, BlockSize, NumBlocks>::currentSize() const noexcept {
		return blocksInUse * blockSize;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	inline std::size_t BitmapAllocator<Alloc, BlockSize, NumBlocks>::bytesRemaining() const noexcept {
		return (blockCount - blocksInUse) * blockSize;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	inline bool BitmapAllocator<Alloc, BlockSize, NumBlocks>::owns(const Block& blk) const noexcept {
		auto blockDistance = static_cast<std::byte*>(blk.ptr) - static_cast<std::byte*>(arena.ptr);
		return blockDistance >= 0 && static_cast<size_t>(blockDistance) < capacity;
	}

}

}