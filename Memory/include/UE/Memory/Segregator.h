#pragma once

#include "UE/Memory/Block.h"

#include <cstddef>

namespace Uwe {

namespace Memory {

	template <std::size_t Threshold, class SmallAlloc, class LargeAlloc>
	class Segregator {
	public:
		static constexpr std::size_t threshold = Threshold;

		Block allocate(std::size_t size) noexcept;
	protected:
		LargeAlloc large;
		SmallAlloc small;
	};

	template <std::size_t Threshold, class SmallAlloc, class LargeAlloc>
	Block Segregator<Threshold, SmallAlloc, LargeAlloc>::allocate(std::size_t size) noexcept {
		return size <= threshold ? small.allocate(size) : large.allocate(size);
	}

}

}