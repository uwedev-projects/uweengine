#pragma once

#include <cstddef>
#include "UE/Memory/Block.h"
#include "UE/Memory/Reallocator.h"

namespace Uwe {

namespace Memory {

	template <class Alloc, std::size_t Capacity>
	class MonotonicBuffer {
		static_assert(Capacity <= Alloc::capacity, "The parent allocator's capacity is too small.");
	protected:
		//alignas (Alignment) std::byte* data[Capacity];
		Alloc parent;
		Block arena;
		std::byte* nextFree;

	public:
		static constexpr std::size_t capacity  = Capacity;
		static constexpr std::size_t alignment = Alloc::alignment;

		MonotonicBuffer() noexcept;
		~MonotonicBuffer();

		MonotonicBuffer(const MonotonicBuffer&) = delete;
		MonotonicBuffer& operator=(const MonotonicBuffer&) = delete;

		MonotonicBuffer(MonotonicBuffer&&) = default;
		MonotonicBuffer& operator=(MonotonicBuffer&&) = default;

		Block allocate(std::size_t size) noexcept;
		Block allocateArray(std::size_t size, const unsigned elementCount) noexcept;

		bool reallocate(Block& blk, std::size_t desiredSize) noexcept;
		bool expand(Block& blk, std::size_t desiredBytes) noexcept;

		void deallocate(Block& blk) const noexcept;
		void deallocateArray(Block& blk, const unsigned elementCount) const noexcept;
		void deallocateAll() noexcept;

		inline bool owns(const Block& blk) const noexcept;
		inline std::size_t getCurrentSize() const noexcept;

		inline bool isLastAllocated(const Block&) const noexcept;
	};

	template <class Alloc, std::size_t Capacity>
	constexpr std::size_t MonotonicBuffer<Alloc, Capacity>::alignment;

	template <class Alloc, std::size_t Capacity>
	constexpr std::size_t MonotonicBuffer<Alloc, Capacity>::capacity;

	template <class Alloc, std::size_t Capacity>
	MonotonicBuffer<Alloc, Capacity>::MonotonicBuffer() noexcept
		: parent{}, arena{ parent.allocate(capacity) }, nextFree{ static_cast<std::byte*>(arena.ptr) }
	{
	}

	template <class Alloc, std::size_t Capacity>
	MonotonicBuffer<Alloc, Capacity>::~MonotonicBuffer()
	{
		parent.deallocate(arena);
	}

	template <class Alloc, std::size_t Capacity>
	Block MonotonicBuffer<Alloc, Capacity>::allocate(std::size_t size) noexcept {
		Block allocated{};

		if (size == 0)
			return allocated;

		std::size_t alignedSize{ roundToAlignment(size, alignment) };

		if (nextFree + alignedSize <= static_cast<std::byte*>(arena.ptr) + capacity) {
			allocated.ptr = nextFree;
			allocated.length = alignedSize;

			nextFree += alignedSize;
		}

		return allocated;
	}

	template <class Alloc, std::size_t Capacity>
	inline Block MonotonicBuffer<Alloc, Capacity>::allocateArray(std::size_t size, const unsigned elementCount) noexcept {
		return allocate(size * elementCount);
	}

	template <class Alloc, std::size_t Capacity>
	bool MonotonicBuffer<Alloc, Capacity>::expand(Block& blk, std::size_t desiredBytes) noexcept {
		bool result{};

		if (isLastAllocated(blk)) {
			std::size_t alignedSize{ roundToAlignment(desiredBytes, alignment) };

			if (nextFree + alignedSize <= static_cast<std::byte*>(arena.ptr) + capacity) {
				blk.length += alignedSize;
				nextFree += alignedSize;
				result = true;
			}
		}

		if (!blk) {
			blk = allocate(desiredBytes);
			result = blk.length > 0;
		}

		return result;
	}

	template <class Alloc, std::size_t Capacity>
	bool MonotonicBuffer<Alloc, Capacity>::reallocate(Block& blk, std::size_t desiredSize) noexcept {
		if (Detail::reallocateBlockTrivially(*this, blk, desiredSize))
			return true;

		return Detail::reallocateBlockWithCopy(*this, *this, blk, desiredSize);
	}

	template <class Alloc, std::size_t Capacity>
	void MonotonicBuffer<Alloc, Capacity>::deallocate(Block& blk) const noexcept {
		blk.reset();
	}

	template <class Alloc, std::size_t Capacity>
	void MonotonicBuffer<Alloc, Capacity>::deallocateArray(Block& blk, const unsigned) const noexcept {
		blk.reset();
	}

	template <class Alloc, std::size_t Capacity>
	void MonotonicBuffer<Alloc, Capacity>::deallocateAll() noexcept {
		// hard reset to beginning of internal buffer
		nextFree = static_cast<std::byte*>(arena.ptr);
	}

	template <class Alloc, std::size_t Capacity>
	inline bool MonotonicBuffer<Alloc, Capacity>::owns(const Block& blk) const noexcept {
		return blk && arena.ptr <= blk.ptr && blk.ptr < static_cast<std::byte*>(arena.ptr) + capacity;
	}

	template <class Alloc, std::size_t Capacity>
	inline std::size_t MonotonicBuffer<Alloc, Capacity>::getCurrentSize() const noexcept {
		return nextFree - static_cast<std::byte*>(arena.ptr);
	}

	template <class Alloc, std::size_t Capacity>
	inline bool MonotonicBuffer<Alloc, Capacity>::isLastAllocated(const Block& blk) const noexcept {
		return static_cast<std::byte*>(blk.ptr) + blk.length == nextFree;
	}
}

}