#pragma once 

#include <cstring>
#include <cstddef>
#include <type_traits>
#include "UE/Memory/Block.h"
#include "UE/Memory/MemTraits.h"

namespace Uwe {

namespace Memory {

	struct NoAffix {};

	template <class Affix, class Alloc>
	constexpr std::enable_if_t<std::is_default_constructible_v<Affix>>
		emplaceAffix(void* ptr, Alloc&);

	template <class Affix, class Alloc>
	constexpr std::enable_if_t<!std::is_default_constructible_v<Affix>>
		emplaceAffix(void* ptr, Alloc& allocator);

	template <typename Suffix>
	constexpr Suffix* suffixFromOutterBlock(const Block&, std::size_t) noexcept;

	template <typename Prefix>
	constexpr Prefix* prefixFromInnerBlock(const Block&, std::size_t) noexcept;

	template <typename Suffix>
	constexpr Suffix* suffixFromInnerBlock(const Block&, std::size_t) noexcept;

	template <typename Prefix>
	constexpr Prefix* prefixFromOutterBlock(const Block&) noexcept;

	template <class Alloc, class Prefix, class Suffix = NoAffix>
	class AffixAllocator {
		Alloc allocator;
	public:
		AffixAllocator() noexcept;

		AffixAllocator(const AffixAllocator&) = delete;
		AffixAllocator& operator=(const AffixAllocator&) = delete;

		AffixAllocator(AffixAllocator&&) = default;
		AffixAllocator& operator=(AffixAllocator&&) = default;

		static constexpr std::size_t alignment = Alloc::alignment;

		static constexpr std::size_t prefixSize =
			std::is_same_v<Prefix, NoAffix> ? 0 : roundToAlignment(sizeof(Prefix), alignment);

		static constexpr std::size_t suffixSize = 
			std::is_same_v<Suffix, NoAffix> ? 0 : roundToAlignment(sizeof(Suffix), alignment);

		Block allocate(std::size_t size) noexcept;
		bool reallocate(Block&, std::size_t) noexcept;
		void deallocate(Block&) noexcept;
		void deallocateAll() noexcept;

		template <class A = Allocator>
		std::enable_if_t<MemTraits::hasOwns<A>::value, bool>
			owns(const Block&) const noexcept;

		template <class A = Allocator>
		std::enable_if_t<MemTraits::hasExpand<A>::value, bool>
			expand(Block&, std::size_t) const noexcept;

		static constexpr Block getInnerBlock(const Block&) noexcept;
		static constexpr Block getOutterBlock(const Block&) noexcept;
	};

	template <class Alloc, class Prefix, class Suffix>
	constexpr std::size_t AffixAllocator<Alloc, Prefix, Suffix>::alignment;

	template <class Alloc, class Prefix, class Suffix>
	constexpr std::size_t AffixAllocator<Alloc, Prefix, Suffix>::prefixSize;

	template <class Alloc, class Prefix, class Suffix>
	constexpr std::size_t AffixAllocator<Alloc, Prefix, Suffix>::suffixSize;

	template <class Alloc, class Prefix, class Suffix>
	AffixAllocator<Alloc, Prefix, Suffix>::AffixAllocator() noexcept
		: allocator{}
	{

	}

	template <class Alloc, class Prefix, class Suffix>
	Block AffixAllocator<Alloc, Prefix, Suffix>::allocate(std::size_t size) noexcept {

		Block outter { allocator.allocate(prefixSize + size + suffixSize) };
		Block result{};

		if (outter) {

			if (prefixSize > 0) {
				Prefix* prefix{ prefixFromOutterBlock<Prefix>(outter) };
				emplaceAffix<Prefix>(static_cast<void*>(prefix), *this);
			}

			if (suffixSize > 0) {
				Suffix* suffix{ suffixFromOutterBlock<Suffix>(outter, suffixSize) };
				emplaceAffix<Suffix>(static_cast<void*>(suffix), *this);
			}

			result = getInnerBlock(outter);
		}
		else {
			result = outter;
		}
		
		return result;
	}

	template <class Alloc, class Prefix, class Suffix>
	bool AffixAllocator<Alloc, Prefix, Suffix>::reallocate(Block& blk, std::size_t desiredSize) noexcept {
		Block outter{ getOutterBlock(blk) };

		Suffix* suffix{ suffixFromOutterBlock<Suffix>(outter, suffixSize) };
		Suffix  storedSuffix{ *suffix };

		bool result{ allocator.reallocate(outter, desiredSize + prefixSize + suffixSize) };

		suffix  = suffixFromOutterBlock<Suffix>(outter, suffixSize);
		*suffix = storedSuffix;

		blk = getInnerBlock(outter);

		return result;
	}

	template <class Alloc, class Prefix, class Suffix>
	void AffixAllocator<Alloc, Prefix, Suffix>::deallocate(Block& blk) noexcept {
		if (blk) {

			if(prefixSize > 0)
				prefixFromInnerBlock<Prefix>(blk, prefixSize)->~Prefix();

			if (suffixSize > 0)
				suffixFromInnerBlock<Suffix>(blk, suffixSize)->~Suffix();

			Block outter{ getOutterBlock(blk) };
			allocator.deallocate(outter);

			blk.reset();
		}
	}

	template <class Alloc, class Prefix, class Suffix>
	void AffixAllocator<Alloc, Prefix, Suffix>::deallocateAll() noexcept {
		allocator.deallocateAll();
	}

	template <class Alloc, class Prefix, class Suffix>
	constexpr Block AffixAllocator<Alloc, Prefix, Suffix>::getInnerBlock(const Block& blk) noexcept {
		return Block { 
			blk.ptr + prefixSize ,
			blk.length - prefixSize - suffixSize 
		};
	}

	template <class Alloc, class Prefix, class Suffix>
	constexpr Block AffixAllocator<Alloc, Prefix, Suffix>::getOutterBlock(const Block& blk) noexcept {
		return Block{
			blk.ptr - prefixSize,
			blk.length + prefixSize + suffixSize
		};
	}

	template <class Alloc, class Prefix, class Suffix>
		template <class A>
	std::enable_if_t<MemTraits::hasOwns<A>::value, bool>
	AffixAllocator<Alloc, Prefix, Suffix>::owns(const Block& blk) const noexcept {
		return blk && allocator.owns(getOutterBlock(blk));
	}

	template <class Alloc, class Prefix, class Suffix>
		template <class A>
	std::enable_if_t<MemTraits::hasExpand<A>::value, bool>
		AffixAllocator<Alloc, Prefix, Suffix>::expand(Block& blk, std::size_t desiredSize) const noexcept {
		
		if (desiredSize == 0)
			return true;

		if (!blk) {
			blk = allocator.allocate(desiredSize);
			return blk.length > 0;
		}
		
		Block outter{ getOutterBlock(blk) };
		bool result{ allocator.expand(outter, desiredSize) };

		if (result) {

			if (suffixSize > 0) {

				Suffix* suffix{ suffixFromInnerBlock<Suffix>(blk, suffixSize) };
				Suffix* shiftedSuffix{ suffixFromOutterBlock<Suffix>(outter, suffixSize) };

				::new (shiftedSuffix) Suffix{ *suffix };
			}

			blk = getInnerBlock(outter);
		}

		return result;
	}


	template <class Prefix>
	constexpr Prefix* prefixFromInnerBlock(const Block& blk, std::size_t prefixSize) noexcept {
		return blk ? reinterpret_cast<Prefix*>(blk.ptr - prefixSize) : nullptr;
	}

	template <class Suffix>
	constexpr Suffix* suffixFromInnerBlock(const Block& blk, std::size_t suffixSize) noexcept {
		return blk ? reinterpret_cast<Suffix*>(blk.ptr + blk.length) : nullptr;
	}

	template <class Prefix>
	constexpr Prefix* prefixFromOutterBlock(const Block& blk) noexcept {
		return blk ? reinterpret_cast<Prefix*>(blk.ptr) : nullptr;
	}

	template <class Suffix>
	constexpr Suffix* suffixFromOutterBlock(const Block& blk, std::size_t suffixSize) noexcept {
		return blk ? reinterpret_cast<Suffix*>(blk.ptr + blk.length - suffixSize) : nullptr;
	}

	template <class Affix, class Alloc>
	constexpr std::enable_if_t<std::is_default_constructible_v<Affix>>
	emplaceAffix(void* ptr, Alloc&) {
		::new(ptr) Affix{}; 
	}

	template <class Affix, class Alloc>
	constexpr std::enable_if_t<!std::is_default_constructible_v<Affix>>
	emplaceAffix(void* ptr, Alloc& allocator) {
		::new(ptr) Affix(allocator);
	}
}

}