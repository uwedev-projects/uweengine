#pragma once

#include "UE/Memory/Block.h"

namespace Uwe {

namespace Memory {

	class NullAllocator {
	public:
		static constexpr std::size_t alignment{ 0ULL };
		static constexpr std::size_t capacity{ 0ULL };

		Block allocate(std::size_t size) noexcept;
		bool reallocate(Block&, std::size_t) noexcept;
		bool expand(Block&, std::size_t) noexcept;

		void deallocate(Block& blk) noexcept;
		void deallocateAll() noexcept;

		bool owns(const Block& blk) const noexcept;
	};
	
}

}