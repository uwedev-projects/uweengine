#pragma once

#include "UE/Memory/Block.h"
#include "UE/Memory/MemTraits.h"

#include <type_traits>

namespace Uwe {

	namespace Memory {

		template <class Primary, class Fallback>
		class FallbackAllocator : private Primary, private Fallback {
		public:
			static constexpr std::size_t alignment { Primary::alignment > Fallback::alignment ?
				Primary::alignment : Fallback::alignment };
			static constexpr std::size_t capacity{ Primary::capacity + Fallback::capacity };

			Block allocate(std::size_t size) noexcept;
			bool reallocate(Block& blk, std::size_t desiredSize) noexcept;

			void deallocate(Block& blk) noexcept;
			
			template <class P = Primary, class F = Fallback>
			typename std::enable_if_t<
				MemTraits::hasExpand<P>::value ||
				MemTraits::hasExpand<F>::value,
				bool
			>
			expand(Block& blk, std::size_t desiredSize) noexcept;

			template <class P = Primary, class F = Fallback>
			typename std::enable_if_t<
				MemTraits::hasDeallocateAll<P>::value &&
				MemTraits::hasDeallocateAll<F>::value>
			deallocateAll() noexcept;

			template <class P = Primary, class F = Fallback>
			typename std::enable_if_t <
				MemTraits::hasOwns<P>::value && 
				MemTraits::hasOwns<F>::value, 
				bool
			>
			owns(const Block& blk) const noexcept;
		};

		template <class Primary, class Fallback>
		Block FallbackAllocator<Primary, Fallback>::allocate(std::size_t size) noexcept {

			if (!size)
				return Block{};

			Block result{ Primary::allocate(size) };

			if (!result) {

				result = Fallback::allocate(size);

			}

			return result;
		}

		template <class Primary, class Fallback>
		bool FallbackAllocator<Primary, Fallback>::reallocate(Block& blk, std::size_t desiredSize) noexcept {
			bool result{};

			if (Primary::owns(blk)) {

				result = Primary::reallocate(blk, desiredSize);

			}
			else {

				result = Fallback::reallocate(blk, desiredSize);

			}

			return result;
		}

		template <class Primary, class Fallback>
		void FallbackAllocator<Primary, Fallback>::deallocate(Block& blk) noexcept {
			if (!blk)
				return;

			if (Primary::owns(blk)) 
				Primary::deallocate(blk);
			else
				Fallback::deallocate(blk);
		}

		template <class Primary, class Fallback>
			template <class P, class F>
		typename std::enable_if_t<
			MemTraits::hasDeallocateAll<P>::value &&
			MemTraits::hasDeallocateAll<F>::value>
		FallbackAllocator<Primary, Fallback>::deallocateAll() noexcept {
			Primary::deallocateAll();
			Fallback::deallocateAll();
		}

		template <class Primary, class Fallback>
			template <class P, class F>
		typename std::enable_if_t<
			MemTraits::hasOwns<P>::value &&
			MemTraits::hasOwns<F>::value, 
			bool
		>
		FallbackAllocator<Primary, Fallback>::owns(const Block& blk) const noexcept {
			return Primary::owns(blk) || Fallback::owns(blk);
		}


		template <class Primary, class Fallback>
			template <class P, class F>
		typename std::enable_if_t<
			MemTraits::hasExpand<P>::value ||
			MemTraits::hasExpand<F>::value,
			bool
		>
		FallbackAllocator<Primary, Fallback>::expand(Block& blk, std::size_t desiredSize) noexcept {
			bool result{};
			
			if constexpr (MemTraits::hasExpand<P>::value) {
				if (Primary::owns(blk)) {
					result = Primary::expand(blk, desiredSize);
				}
			}
			else if constexpr (MemTraits::hasExpand<F>::value) {
				if (Fallback::owns(blk)) {
					result = Fallback::expand(blk, desiredSize);
				}
			}

			return result;
		}
	}

}