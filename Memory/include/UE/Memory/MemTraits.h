#pragma once

#include <type_traits>
#include "UE/Memory/Block.h"

namespace Uwe {

namespace Memory {

	namespace MemTraits {
		template <typename T>
		struct hasOwns {

			template <typename C>
			static constexpr decltype(std::declval<C>().owns(std::declval<const Block&>()), bool())
				test(int /* unused */) {
				using ReturnType = typename std::invoke_result_t<decltype(&T::owns), T*, const Block&>;
				return std::is_same_v<ReturnType, bool>;
			}

			template <typename C>
			static constexpr bool test(...) {
				return false;
			}

			static constexpr bool value = test<T>(int());
		};

		template <typename T>
		struct hasExpand {

			template <typename C>
			static constexpr decltype(std::declval<C>().expand(std::declval<Block&>(), std::declval<std::size_t>()), bool())
				test(int /* unused */) {
				using ReturnType = typename std::invoke_result_t<decltype(&T::expand), T*, Block&, std::size_t>;
				return std::is_same_v<ReturnType, bool>;
			}

			template <typename C>
			static constexpr bool test(...) {
				return false;
			}

			static constexpr bool value = test<T>(int());
		};

		template <typename T>
		struct hasDeallocateAll {

			template <typename C>
			static constexpr decltype(std::declval<C>().deallocateAll(), bool())
				test(int /* unused */) {
				using ReturnType = typename std::invoke_result_t<decltype(&T::deallocateAll), T*>;
				return std::is_same_v<ReturnType, void>;
			}

			template <typename C>
			static constexpr bool test(...) {
				return false;
			}

			static constexpr bool value = test<T>(int());
		};

		template <typename T>
		struct hasAllocateArray {

			template <typename C>
			static constexpr decltype(std::declval<C>().allocateArray(std::declval<std::size_t>(), std::declval<std::size_t>()), bool())
				test(int /* unused */) {
				using ReturnType = typename std::invoke_result_t<decltype(&T::allocateArray), T*, std::size_t, std::size_t>;
				return std::is_same_v<ReturnType, Block>;
			}

			template <typename C>
			static constexpr bool test(...) {
				return false;
			}

			static constexpr bool value = test<T>(int());
		};

		template <typename T>
		struct hasDeallocateArray {

			template <typename C>
			static constexpr decltype(std::declval<C>().deallocateArray(std::declval<Block&>(), std::declval<std::size_t>()), bool())
				test(int /* unused */) {
				using ReturnType = typename std::invoke_result_t<decltype(&T::deallocateArray), T*, Block&, std::size_t>;
				return std::is_same_v<ReturnType, void>;
			}

			template <typename C>
			static constexpr bool test(...) {
				return false;
			}

			static constexpr bool value = test<T>(int());
		};

	}

}

}