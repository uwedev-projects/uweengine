#pragma once 

#include "UE/Memory/MonotonicBuffer.h"
#include "UE/Memory/Block.h"

namespace Uwe {

namespace Memory {

	template <class Alloc, std::size_t Capacity>
	class StackAllocator : public MonotonicBuffer<Alloc, Capacity> {
	public:
		static constexpr std::size_t capacity  = Capacity;
		static constexpr std::size_t alignment = Alloc::alignment;

		void deallocate(Block& blk) noexcept;
		void deallocateArray(Block& blk, const unsigned elementCount) noexcept;
	};

	template <class Alloc, std::size_t Capacity>
	constexpr std::size_t StackAllocator<Alloc, Capacity>::capacity;

	template <class Alloc, std::size_t Capacity>
	constexpr std::size_t StackAllocator<Alloc, Capacity>::alignment;

	template <class Alloc, std::size_t Capacity>
	void StackAllocator<Alloc, Capacity>::deallocate(Block& blk) noexcept {
		if (isLastAllocated(blk)) {

			nextFree -= blk.length;
			blk.reset();

		}
	}

	template <class Alloc, std::size_t Capacity>
	inline void StackAllocator<Alloc, Capacity>::deallocateArray(Block& blk, const unsigned) noexcept {
		deallocate(blk);
	}
}

}