#pragma once

#include "Memory/Block.h"
#include "Platform/BitScan.h"
#include "Exceptions.h"

#include <array>
#include <string>
#include <limits>
#include <cstddef>
#include <type_traits>

namespace Uwe {

namespace Memory {

	using IndexPair = std::pair<unsigned long, unsigned long>;

	constexpr std::size_t calcNumBitmaps (std::size_t numBlocks, std::size_t bitmapSize, std::size_t bitsPerByte) {
		return std::max(
			numBlocks / (2ULL * bitmapSize * bitsPerByte) +
			static_cast<std::size_t>(numBlocks % (2ULL * bitmapSize * bitsPerByte) != 0ULL),
		1ULL) * 2ULL;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	class BitmappedPool {
	public:
		static constexpr std::size_t alignment		= Alloc::alignment;
		static constexpr std::size_t bitsPerByte	= static_cast<std::size_t>(CHAR_BIT);
		static constexpr std::size_t maskWidth		= sizeof(Bitmask64) * bitsPerByte;

		static constexpr std::size_t numBlocks		= NumBlocks;
		static constexpr std::size_t numBitmaps		= calcNumBitmaps(numBlocks, sizeof(Bitmask64), bitsPerByte);

		static constexpr std::size_t blockSize		= BlockSize;
		static constexpr std::size_t mappedSize		= BlockSize * maskWidth;
		static constexpr std::size_t totalSize		= BlockSize * NumBlocks;

		static constexpr Bitmask64 allFreeMask{ std::numeric_limits<Bitmask64>::max() };

		BitmappedPool();
		~BitmappedPool();

		BitmappedPool(const BitmappedPool&) = delete;
		BitmappedPool& operator=(const BitmappedPool&) = delete;

		BitmappedPool(BitmappedPool&&) = default;
		BitmappedPool& operator=(BitmappedPool&&) = default;

		inline Block allocate(std::size_t size) noexcept;
		Block allocate() noexcept;
		bool reallocate(Block& blk, std::size_t desiredSize) noexcept;

		void deallocate(Block& blk) noexcept;

		inline bool owns(const Block& blk) const noexcept;
		inline std::size_t getCurrentSize() const noexcept;

	protected:
		std::array<Bitmask64, numBitmaps> bitmaps;
		Alloc parent;
		Block pool;

		inline void* indexToAddress(unsigned long index, unsigned long page) const noexcept;
		bool addressToIndex(void* address, IndexPair& result) const noexcept;
		constexpr unsigned nextPowerOfTwo(unsigned n) const noexcept;

		bool reserveBlocks(unsigned blockCount, IndexPair& result) noexcept;
		bool findFreeSlots(Bitmask128& searchMask, unsigned blockCount, unsigned long& index) noexcept;

		inline Block allocateSingleBlock() noexcept;
		inline Block allocateMultiBlock(unsigned blockCount) noexcept;
	};

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	constexpr std::size_t BitmappedPool<Alloc, BlockSize, NumBlocks>::alignment;

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	constexpr std::size_t BitmappedPool<Alloc, BlockSize, NumBlocks>::numBlocks;

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	constexpr std::size_t BitmappedPool<Alloc, BlockSize, NumBlocks>::numBitmaps;

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	constexpr std::size_t BitmappedPool<Alloc, BlockSize, NumBlocks>::blockSize;

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	constexpr std::size_t BitmappedPool<Alloc, BlockSize, NumBlocks>::mappedSize;

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	constexpr std::size_t BitmappedPool<Alloc, BlockSize, NumBlocks>::totalSize;

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	constexpr Bitmask64 BitmappedPool<Alloc, BlockSize, NumBlocks>::allFreeMask;

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	BitmappedPool<Alloc, BlockSize, NumBlocks>::BitmappedPool()
		:	bitmaps{},
			parent{}, 
			pool{ parent.allocate(totalSize) }
			
	{
		for (int i = 0; i < numBitmaps; ++i) {
	
			bitmaps[i] = allFreeMask;

		}
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	BitmappedPool<Alloc, BlockSize, NumBlocks>::~BitmappedPool() {
		parent.deallocate(pool);
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	Block BitmappedPool<Alloc, BlockSize, NumBlocks>::allocate() noexcept {
		return allocate(blockSize);
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	Block BitmappedPool<Alloc, BlockSize, NumBlocks>::allocate(std::size_t size) noexcept {
		if (size <= blockSize)
			return allocateSingleBlock();

		if constexpr (isPowerOfTwo(blockSize)) {
			size += (size & (blockSize - 1));
			return allocateMultiBlock(static_cast<unsigned>(size / blockSize));
		}
		else {
			size += (size % blockSize);
			return allocateMultiBlock(static_cast<unsigned>(size / blockSize));
		}

		return {};
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	Block BitmappedPool<Alloc, BlockSize, NumBlocks>::allocateSingleBlock() noexcept {
		unsigned long firstFreeIndex{}, page{};

		bool foundSlot{};

		for (unsigned long i{}; !foundSlot && i < numBitmaps; ++i) {
			foundSlot = Platform::findFirstSetBit(bitmaps[i], firstFreeIndex);
			page = i;
		}

		Block block{};

		if (foundSlot) {
			block.ptr = indexToAddress(firstFreeIndex, page);
			block.length = blockSize;

			bitmaps[page] ^= 1ULL << firstFreeIndex;
		}

		return block;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	Block BitmappedPool<Alloc, BlockSize, NumBlocks>::allocateMultiBlock(unsigned blockCount) noexcept {
		Block block{};
		IndexPair reserved{};

		if (reserveBlocks(blockCount, reserved)) {
			block.ptr		= indexToAddress(reserved.first, reserved.second);
			block.length	= blockCount * blockSize;
		}

		return block;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	bool BitmappedPool<Alloc, BlockSize, NumBlocks>::reallocate(Block& blk, std::size_t desiredSize) noexcept {
		return false;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	void BitmappedPool<Alloc, BlockSize, NumBlocks>::deallocate(Block& blk) noexcept {
		IndexPair indexPage{};

		if (addressToIndex(blk.ptr, indexPage)) {
			const auto&[index, page] = indexPage;

			bitmaps[page] ^= 1ULL << index;
			blk.reset();
		}
		
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	inline void* BitmappedPool<Alloc, BlockSize, NumBlocks>::indexToAddress(unsigned long index, unsigned long page) const noexcept {
		auto address = static_cast<char*>(pool.ptr) + (page * mappedSize) + (index * blockSize);
		return static_cast<void*>(address);
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	bool BitmappedPool<Alloc, BlockSize, NumBlocks>::addressToIndex(void* address, IndexPair& result) const noexcept {
		auto pointerDelta{ (static_cast<char*>(address) - static_cast<char*>(pool.ptr)) };
		
		if (pointerDelta < 0 || pointerDelta > totalSize)
			return false;

		result = IndexPair {
			static_cast<unsigned long>((pointerDelta / blockSize) & (mappedSize - 1)),
			static_cast<unsigned long>(pointerDelta / mappedSize)
		};

		return true;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	bool BitmappedPool<Alloc, BlockSize, NumBlocks>::reserveBlocks(unsigned blockCount, IndexPair& result) noexcept {
		constexpr unsigned bitsPerMask{ sizeof(Bitmask128) * bitsPerByte };

		unsigned long firstFreeIndex{};
		Bitmask128 searchMask{};
		searchMask[0] = searchMask[1] = std::numeric_limits<Bitmask64>::max();

		bool slotsFound{ findFreeSlots (
			searchMask,
			nextPowerOfTwo(blockCount),
			firstFreeIndex
		) };

		const unsigned long indexInPage{ firstFreeIndex & (maskWidth - 1ULL) };
		const unsigned long firstPage{ firstFreeIndex / maskWidth };
		unsigned long currentPage{ firstPage };

		unsigned countReserved{};
		while (countReserved < blockCount) {
			Bitmask64 reserveMask{ blockCount - countReserved >= maskWidth ? 
				std::numeric_limits<Bitmask64>::max() : 
				((1ULL << blockCount) - 1ULL) << indexInPage
			};

			bitmaps[currentPage] &= ~reserveMask;
			countReserved += (blockCount - countReserved > maskWidth ? maskWidth : blockCount);
			
			++currentPage;
		}

		result.first	= indexInPage;
		result.second	= firstPage;

		return slotsFound;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	bool BitmappedPool<Alloc, BlockSize, NumBlocks>::findFreeSlots(Bitmask128& searchMask, unsigned blockCount, unsigned long& index) noexcept {
		switch (std::max(blockCount, 8U)) {
		case 8U:
			return Platform::SIMD::scanForBitPattern<8U, numBitmaps * sizeof(Bitmask64)>(searchMask, reinterpret_cast<Bitmask128*>(bitmaps.data()), index);
		case 16U:
			return Platform::SIMD::scanForBitPattern<16U, numBitmaps * sizeof(Bitmask64)>(searchMask, reinterpret_cast<Bitmask128*>(bitmaps.data()), index);
		case 32U:
			return Platform::SIMD::scanForBitPattern<32U, numBitmaps * sizeof(Bitmask64)>(searchMask, reinterpret_cast<Bitmask128*>(bitmaps.data()), index);
		case 64U:
			return Platform::SIMD::scanForBitPattern<64U, numBitmaps * sizeof(Bitmask64)>(searchMask, reinterpret_cast<Bitmask128*>(bitmaps.data()), index);
		default:
			return Platform::SIMD::scanForBitPattern<128U, numBitmaps * sizeof(Bitmask64)>(searchMask, reinterpret_cast<Bitmask128*>(bitmaps.data()), index);
		}

		return false;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	inline bool BitmappedPool<Alloc, BlockSize, NumBlocks>::owns(const Block& blk) const noexcept {
		uintptr_t pointerDelta{ reinterpret_cast<uintptr_t>(blk.ptr) - reinterpret_cast<uintptr_t>(pool.ptr) };
		return pointerDelta < static_cast<uintptr_t>(totalSize);
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	constexpr unsigned BitmappedPool<Alloc, BlockSize, NumBlocks>::nextPowerOfTwo(unsigned n) const noexcept {
		if (!isPowerOfTwo(n) || n < 8U) {
			unsigned long msbIndex{};
			Platform::findLastSetBit(static_cast<unsigned long long>(n), msbIndex);

			n = (n << 1U) & ~((1U << msbIndex) - 1U);
		}

		return n;
	}

	template <class Alloc, std::size_t BlockSize, std::size_t NumBlocks>
	inline std::size_t BitmappedPool<Alloc, BlockSize, NumBlocks>::getCurrentSize() const noexcept {
		return 0ULL;
	}
}

}