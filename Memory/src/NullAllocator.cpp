#include "UE/Memory/NullAllocator.h"

#include <cassert>

namespace Uwe {

namespace Memory {

	constexpr std::size_t NullAllocator::alignment;

	Block NullAllocator::allocate(std::size_t size) noexcept {
		return Block{ nullptr, 0 };
	}

	bool NullAllocator::reallocate(Block& blk, std::size_t) noexcept {
		assert(!blk);
		return false;
	}

	bool NullAllocator::expand(Block& blk, std::size_t) noexcept {
		assert(!blk);
		return false;
	}

	void NullAllocator::deallocate(Block& blk) noexcept {
		assert(!blk);
	}

	void NullAllocator::deallocateAll() noexcept {}

	bool NullAllocator::owns(const Block& blk) const noexcept {
		return !blk;
	}

}

}