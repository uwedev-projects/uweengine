#include "UE/Memory/Mallocator.h"

#include <cstdlib>

namespace Uwe {

namespace Memory {

	constexpr std::size_t Mallocator::alignment;

	Block Mallocator::allocate(std::size_t size) noexcept {
		auto blockAddress = std::malloc(size);

		Block result{};
		
		if (!size) 
			return result;

		if (blockAddress) {

			result.ptr = static_cast<std::byte*>(blockAddress);
			result.length = size;

		}

		return result;
	}

	bool Mallocator::reallocate(Block& blk, std::size_t desiredSize) noexcept {
		bool result{};
		std::byte* newAddress{ static_cast<std::byte*>(std::realloc(blk.ptr, desiredSize)) };

		Block newBlock{ newAddress, desiredSize };
		if (newBlock) {
			blk = newBlock;
			result = true;
		}

		return result;
	}

	void Mallocator::deallocate(Block& blk) noexcept {
		if (blk) {
			std::free(blk.ptr);
			blk.reset();
		}
	}

}

}