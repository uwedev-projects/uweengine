#include "UE/Memory/Block.h"

#include <cstring>
#include <utility>
#include <algorithm>

namespace Uwe {

namespace Memory {

	Block::Block() noexcept
		: ptr{nullptr}, length{0}
	{
	}

	Block& Block::operator =(Block&& rhs) noexcept {
		ptr		= rhs.ptr;
		length	= rhs.length;

		rhs.reset();
		return *this;
	}

	Block::Block(Block&& blk) noexcept {
		*this = std::move(blk);
	}

	void Block::reset() noexcept {
		ptr = nullptr;
		length = 0;
	}

	Block::operator bool() const {
		return ptr != nullptr;
	}

	bool Block::operator==(const Block &rhs) const {
		return ptr == rhs.ptr && length == rhs.length;
	}

	void copyBlock(const Block& source, Block& destination) noexcept {
		::memcpy(destination.ptr, source.ptr, std::min(source.length, destination.length));
	}
}

}