#include "gtest/gtest.h"

#include "UE/Memory/Reallocator.h"
#include "UE/Memory/MonotonicBuffer.h"
#include "TestAllocator.h"

namespace Uwe {

namespace Memory {

	TEST(ReallocatorTest, TrivialReallocationDeallocatesIfDesiredSizeIsZero) {
		TestAllocator<64> alloc{};
		Block blk{ alloc.allocate(64) };

		EXPECT_TRUE(Detail::reallocateBlockTrivially(alloc, blk, 0));
		EXPECT_EQ(blk.length, 0);
		EXPECT_EQ(blk.ptr, nullptr);
	}

	TEST(ReallocatorTest, TrivialReallocationReturnsTheBlockWhenDesiredSizeIsEqualToOldSize) {
		TestAllocator<64> alloc{};
		Block blk{ alloc.allocate(64) };

		const void* oldPtr{ blk.ptr };

		EXPECT_TRUE(Detail::reallocateBlockTrivially(alloc, blk, blk.length));
		EXPECT_EQ(oldPtr, blk.ptr);
		EXPECT_EQ(blk.length, 64);
	}

	TEST(ReallocatorTest, TrivialReallocationAllocatesNewBlockIfOldBlockIsNull) {
		TestAllocator<64> alloc{};
		Block blk{};

		EXPECT_TRUE(Detail::reallocateBlockTrivially(alloc, blk, 64));
		EXPECT_NE(blk.ptr, nullptr);
		EXPECT_EQ(blk.length, 64);
	}

	TEST(ReallocatorTest, TrivialReallocationWithExpandDeallocatesIfDesiredSizeIsZero) {
		MonotonicBuffer<TestAllocator<64>, 128> alloc{};
		Block blk{ alloc.allocate(64) };

		EXPECT_TRUE(Detail::reallocateBlockTrivially(alloc, blk, 0));
		EXPECT_EQ(blk.length, 0);
		EXPECT_EQ(blk.ptr, nullptr);
	}

	TEST(ReallocatorTest, TrivialReallocationWithExpandReturnsTheBlockWhenDesiredSizeIsEqualToOldSize) {
		MonotonicBuffer<TestAllocator<64>, 128> alloc{};
		Block blk{ alloc.allocate(64) };

		const void* oldPtr{ blk.ptr };

		EXPECT_TRUE(Detail::reallocateBlockTrivially(alloc, blk, blk.length));
		EXPECT_EQ(oldPtr, blk.ptr);
		EXPECT_EQ(blk.length, 64);
	}

	TEST(ReallocatorTest, TrivialReallocationWithExpandAllocatesNewBlockIfOldBlockIsNull) {
		MonotonicBuffer<TestAllocator<64>, 128> alloc{};
		Block blk{};

		EXPECT_TRUE(Detail::reallocateBlockTrivially(alloc, blk, 64));
		EXPECT_NE(blk.ptr, nullptr);
		EXPECT_EQ(blk.length, 64);
	}

	TEST(ReallocatorTest, TrivialReallocationExpandsTheBlockIfPossible) {
		MonotonicBuffer<TestAllocator<64>, 128> alloc{};
		Block blk{ alloc.allocate(64) };

		const void* oldPtr{ blk.ptr };
		EXPECT_TRUE(Detail::reallocateBlockTrivially(alloc, blk, 128));
		EXPECT_EQ(blk.ptr, oldPtr);
		EXPECT_EQ(blk.length, 128);
	}

	TEST(ReallocatorTest, ReallocatesBlockWithCopy) {
		TestAllocator<64> alloc{};
		Block blk{ alloc.allocate(64) };
		::new(blk.ptr) int{ 1555 };

		const void* oldPtr{ blk.ptr };

		EXPECT_TRUE(Detail::reallocateBlockWithCopy(alloc, alloc, blk, 128));
		EXPECT_NE(blk.ptr, oldPtr);
		EXPECT_EQ(blk.length, 128);
		EXPECT_EQ(*reinterpret_cast<int*>(blk.ptr), 1555);
	}

	TEST(ReallocatorTest, ReallocationWithCopyDeallocatesTheOldBlock) {
		TestAllocator<64> alloc{};
		Block blk{ alloc.allocate(64) };

		const void* oldPtr{ blk.ptr };
		EXPECT_TRUE(Detail::reallocateBlockWithCopy(alloc, alloc, blk, 128));
		EXPECT_EQ(TestAllocator<64>::lastDeallocated, oldPtr);
		EXPECT_EQ(TestAllocator<64>::lastDeallocatedSize, 64);
	}
}

}
