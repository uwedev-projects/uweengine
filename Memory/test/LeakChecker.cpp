#include "LeakChecker.h"

#if defined _MSC_VER

namespace Uwe {

namespace Memory {

	LeakChecker::LeakChecker()
		: state1{}, state2{}, state3{} {
		_CrtMemCheckpoint(&state1);
		_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE | _CRTDBG_MODE_WNDW);
		_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);
	}

	LeakChecker::~LeakChecker() {
		_CrtMemCheckpoint(&state2);
		//EXPECT_EQ(0, _CrtMemDifference(&state3, &state1, &state2)); 
		if (!::testing::Test::HasFailure() && _CrtMemDifference(&state3, &state1, &state2)) {
			_CrtMemDumpStatistics(&state3);

			const ::testing::TestInfo* const testInfo{
				::testing::UnitTest::GetInstance()->current_test_info()
			};

			std::stringstream msgBuf{};
			msgBuf << "Memory leak detected in test case.\n" <<
				"Test suite: " << testInfo->test_case_name() << "\n" <<
				"Test case: " << testInfo->name() << "\n";

			ADD_FAILURE() << msgBuf.str();
		}
	}

}

}

#endif