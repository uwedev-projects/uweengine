#include "gtest/gtest.h"

#include "UE/Memory/FreeList.h"
#include "UE/Memory/Block.h"

#include "TestAllocator.h"

#include <algorithm>
#include <vector>

namespace Uwe {	

namespace Memory {

	using FreeListAllocator = FreeList<TestAllocator<128>, 17U, 128U, 10U, 1U>;
	using FreeListWithBatch = FreeList<TestAllocator<32>, 16U, 32U, 128U, 4U>;

	TEST(FreeListTest, AllocatesFromItsParentWhenNothingWasFreed) {
		FreeListAllocator freeListAllocator{};
		constexpr long blockSize{ 32L };

		Block block1{ freeListAllocator.allocate(blockSize) };

		EXPECT_NE(block1.ptr, nullptr);
		EXPECT_EQ(TestAllocator<128>::lastAllocated, block1.ptr);
		EXPECT_EQ(TestAllocator<128>::lastAllocatedSize, FreeListAllocator::blockSize);

		Block block2{ freeListAllocator.allocate(blockSize) };
		const auto pointerDelta = std::abs(block1.ptr - block2.ptr);

		EXPECT_NE(block2.ptr, nullptr);
		EXPECT_EQ(TestAllocator<128>::lastAllocated, block2.ptr);
		EXPECT_EQ(TestAllocator<128>::lastAllocatedSize, FreeListAllocator::blockSize);
		EXPECT_GE(pointerDelta, blockSize);
	}

	TEST(FreeListTest, DoesNotDeallocateFromItsParent) {
		FreeListAllocator freeListAllocator{};

		Block blk{ freeListAllocator.allocate(64U) };
		freeListAllocator.deallocate(blk);

		EXPECT_EQ(TestAllocator<128>::lastDeallocated, nullptr);
	}

	TEST(FreeListTest, HandsOutPreviouslyFreedBlocks) {
		FreeListAllocator freeListAllocator{};

		Block block1{ freeListAllocator.allocate(64U) };
		Block block2{ freeListAllocator.allocate(64U) };
		Block block3{ freeListAllocator.allocate(64U) };

		void* oldBlock2{ block2.ptr };
		void* oldBlock3{ block3.ptr };

		freeListAllocator.deallocate(block2);
		freeListAllocator.deallocate(block3);

		Block block4{ freeListAllocator.allocate(64U) };
		Block block5{ freeListAllocator.allocate(64U) };

		EXPECT_EQ(block4.ptr, oldBlock3);
		EXPECT_EQ(block5.ptr, oldBlock2);
	}

	TEST(FreeListTest, UsesTheParentWhenSizeClassIsNotMatched) {
		FreeListAllocator freeListAllocator{};

		Block mediumBlock{ freeListAllocator.allocate(32U) };
		void* oldBlock{ mediumBlock.ptr };
		freeListAllocator.deallocate(mediumBlock);

		Block smallBlock{ freeListAllocator.allocate(16U) };
		EXPECT_NE(TestAllocator<128>::lastAllocated, oldBlock);
		EXPECT_EQ(TestAllocator<128>::lastAllocated, smallBlock.ptr);
	}

	TEST(FreeListTest, RoundsAllocationsToTheUpperLimit) {
		FreeListAllocator freeListAllocator{};

		Block blk{ freeListAllocator.allocate(32U) };
		EXPECT_EQ(TestAllocator<128>::lastAllocatedSize, 128U);
	}

	TEST(FreeListTest, DoesNotRememberMoreThanLimitAllocations) {
		constexpr std::size_t FListLimit{ 2U };
		using LimitedFreeList = FreeList<TestAllocator<64>, 16U, 64U, FListLimit, 1>;

		LimitedFreeList freeListAllocator{};
		Block block1{ freeListAllocator.allocate(64U) };
		Block block2{ freeListAllocator.allocate(64U) };
		Block block3{ freeListAllocator.allocate(64U) };

		freeListAllocator.deallocate(block1);
		freeListAllocator.deallocate(block2);

		EXPECT_EQ(TestAllocator<64>::lastDeallocated, nullptr);

		void* block3Location{ block3.ptr };
		freeListAllocator.deallocate(block3);
		EXPECT_EQ(TestAllocator<64>::lastDeallocated, block3Location);
	}

	TEST(FreeListTest, InformsWhetherItOwnsABlock) {
		FreeListAllocator freeListAllocator{};

		Block block1{ freeListAllocator.allocate(64U) };
		Block block2{};

		EXPECT_TRUE(freeListAllocator.owns(block1));
		EXPECT_FALSE(freeListAllocator.owns(block2));
	}

	TEST(FreeListTest, ReallocationBiggerThanUpperSizeAlwaysFails) {
		FreeListAllocator freeListAllocator{};
		Block blk{ freeListAllocator.allocate(128U) };

		EXPECT_FALSE(freeListAllocator.reallocate(blk, 256));
	}

	TEST(FreeListTest, ReallocatesBlockWithinSizeSpectrum) {
		FreeListAllocator freeListAllocator{};
		Block blk{ freeListAllocator.allocate(32U) };

		EXPECT_TRUE(freeListAllocator.reallocate(blk, 128));
	}

	TEST(FreeListTest, DeallocatesFreeBlocksOnDestruction) {
		EXPECT_EQ(TestAllocator<128>::lastDeallocated, nullptr);
		EXPECT_EQ(TestAllocator<128>::lastDeallocatedSize, 0U);
		void* addressOfBlock{};

		{
			FreeListAllocator freeListAllocator{};
			Block blk{ freeListAllocator.allocate(32U) };
			addressOfBlock = blk.ptr;

			freeListAllocator.deallocate(blk);
		}

		EXPECT_EQ(TestAllocator<128>::lastDeallocated, addressOfBlock);
		EXPECT_EQ(TestAllocator<128>::lastDeallocatedSize, 128U);

	}

	TEST(FreeListTest, AllocatesBlocksConsecutivlyInBatches) {
		FreeListWithBatch freeListAllocator{};
		Block firstBlock{ freeListAllocator.allocate(32U) };

		EXPECT_EQ(TestAllocator<32>::lastAllocated, firstBlock.ptr);
		EXPECT_EQ(TestAllocator<32>::lastAllocatedSize, firstBlock.length * FreeListWithBatch::batchSize);

		std::vector<Block> blocks{ firstBlock };

		for (std::size_t i{1U}; i < FreeListWithBatch::batchSize; ++i) {
			blocks.push_back(freeListAllocator.allocate(32U));
			const auto pointerDelta = std::abs(blocks[i-1].ptr - blocks[i].ptr);
			EXPECT_EQ(pointerDelta, FreeListWithBatch::blockSize);
		}

		for (auto& block : blocks)
			freeListAllocator.deallocate(block);
	}

}

}