#include "gtest/gtest.h"

#include "UE/Memory/AffixAllocator.h"
#include "UE/Memory/NullAllocator.h"
#include "UE/Memory/MemTraits.h"

#include "TestAllocator.h"

#include <cstdint>

namespace Uwe {

namespace Memory {

	TEST(AffixAllocatorTest, ReturnsTheInnerBlockWithoutAffix) {
		
		using TestAffixAllocator = AffixAllocator<TestAllocator<8>, uint32_t, uint32_t>;

		std::byte data{ 0x40 };

		Block block{ &data, 32 };
		Block inner{ TestAffixAllocator::getInnerBlock(block) };

		EXPECT_EQ(inner.ptr, block.ptr + 8);
		EXPECT_EQ(inner.length, 16);
	}

	TEST(AffixAllocatorTest, ReturnsTheOutterBlockIncludingAffix) {

		using TestAffixAllocator = AffixAllocator<TestAllocator<8>, uint32_t, uint32_t>;

		std::byte data{ 0x40 };

		Block block{ &data, 32 };
		Block outter{ TestAffixAllocator::getOutterBlock(block) };

		EXPECT_EQ(outter.ptr, block.ptr - 8);
		EXPECT_EQ(outter.length, 48);
	}

	TEST(AffixAllocatorTest, ReturnsPrefixPointerFromOuterBlock) {
		std::byte data{ 0x40 };
		Block block{ &data, 32 };
		uint64_t* prefix{ prefixFromOutterBlock<uint64_t>(block) };

		EXPECT_EQ(reinterpret_cast<std::byte*>(prefix), block.ptr);
	}

	TEST(AffixAllocatorTest, ReturnsSuffixPointerFromOuterBlock) {
		std::byte data{ 0x40 };
		Block block{ &data, 32 };
		uint64_t* suffix{ suffixFromOutterBlock<uint64_t>(block, 8) };

		EXPECT_EQ(reinterpret_cast<std::byte*>(suffix), block.ptr + block.length - 8);
	}

	TEST(AffixAllocatorTest, ReturnsPrefixPointerFromInnerBlock) {
		std::byte data{ 0x40 };
		Block block{ &data , 16 };
		uint64_t* prefix{ prefixFromInnerBlock<uint64_t>(block, 16) };

		EXPECT_EQ(reinterpret_cast<std::byte*>(prefix), block.ptr - 16);
	}

	TEST(AffixAllocatorTest, ReturnsSuffixPointerFromInnerBlock) {
		std::byte data{ 0x40 };
		Block block{ &data , 16 };
		uint64_t* suffix{ suffixFromInnerBlock<uint64_t>(block, 16) };

		EXPECT_EQ(reinterpret_cast<std::byte*>(suffix), block.ptr + block.length);
	}

	TEST(AffixAllocatorTest, ReturnsNullptrWhenGivenAnEmptyBlock) {

		Block block{ nullptr , 0 };

		uint64_t* prefix{ prefixFromInnerBlock<uint64_t>(block, 16) };
		EXPECT_EQ(prefix, nullptr);

		uint64_t* suffix{ suffixFromInnerBlock<uint64_t>(block, 16) };
		EXPECT_EQ(suffix, nullptr);

		prefix = prefixFromOutterBlock<uint64_t>(block);
		EXPECT_EQ(prefix, nullptr);

		suffix = suffixFromOutterBlock<uint64_t>(block, 16);
		EXPECT_EQ(suffix, nullptr);
	}

	TEST(AffixAllocatorTest, PrefixesEachAllocation) {
		AffixAllocator<TestAllocator<16>, uint32_t> prefixAllocator{};
		Block block{ prefixAllocator.allocate(8) };

		EXPECT_EQ(block.length, 16);

		uint32_t* prefix{ reinterpret_cast<uint32_t*>(block.ptr - 16) };
		EXPECT_EQ(*prefix, 0);

		prefixAllocator.deallocate(block);
	}

	TEST(AffixAllocatorTest, SuffixesEachAllocation) {
		AffixAllocator<TestAllocator<16>, NoAffix, uint32_t> suffixAllocator{};
		Block block{ suffixAllocator.allocate(8) };

		EXPECT_EQ(block.length, 16);

		uint32_t* suffix{ reinterpret_cast<uint32_t*>(block.ptr + block.length) };
		EXPECT_EQ(*suffix, 0);

		suffixAllocator.deallocate(block);
	}

	TEST(AffixAllocatorTest, ReturnsEmptyBlockWhenUnderlyingAllocatorFails) {
		AffixAllocator<NullAllocator, uint16_t> prefixAllocator{};
		Block block{ prefixAllocator.allocate(8) };

		EXPECT_EQ(block.length, 0);
	}

	TEST(AffixAllocatorTest, ReturnsEmptyBlockWhenAllocatingZeroBytes) {
		AffixAllocator<TestAllocator<16>, uint32_t> prefixAllocator{};
		Block block{ prefixAllocator.allocate(0) };

		EXPECT_EQ(block.length, 0);
	}

	TEST(AffixAllocatorTest, ResetsABlockOnDeallocation) {
		AffixAllocator<TestAllocator<16>, uint64_t> prefixAllocator{};
		Block block{ prefixAllocator.allocate(64) };

		EXPECT_EQ(block.length, 64);

		prefixAllocator.deallocate(block);

		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0);
	}

	TEST(AffixAllocatorTest, DeallocatesBlockIncludingPrefix) {
		AffixAllocator<TestAllocator<16>, uint64_t> prefixAllocator{};
		Block block{ prefixAllocator.allocate(64) };
		void* addressToDeallocate{ block.ptr };

		prefixAllocator.deallocate(block);

		EXPECT_NE(TestAllocator<16>::lastDeallocated, nullptr);
		EXPECT_EQ((uintptr_t) TestAllocator<16>::lastDeallocated, (uintptr_t) addressToDeallocate - 16);
		EXPECT_EQ(TestAllocator<16>::lastDeallocatedSize, 80);
	}

	TEST(AffixAllocatorTest, DeallocatesBlockIncludingSuffix) {
		AffixAllocator<TestAllocator<16>, NoAffix, uint64_t> suffixAllocator{};
		Block block{ suffixAllocator.allocate(64) };
		void* addressToDeallocate{ block.ptr };

		suffixAllocator.deallocate(block);

		EXPECT_NE(TestAllocator<16>::lastDeallocated, nullptr);
		EXPECT_EQ(TestAllocator<16>::lastDeallocated, addressToDeallocate);
		EXPECT_EQ(TestAllocator<16>::lastDeallocatedSize, 80);
	}

	TEST(AffixAllocatorTest, DeallocatesBlockIncludingAffixes) {
		AffixAllocator<TestAllocator<16>, uint64_t, uint64_t> suffixAllocator{};
		Block block{ suffixAllocator.allocate(64) };
		void* addressToDeallocate{ block.ptr };

		suffixAllocator.deallocate(block);

		EXPECT_NE(TestAllocator<16>::lastDeallocated, nullptr);
		EXPECT_EQ((uintptr_t) TestAllocator<16>::lastDeallocated, (uintptr_t) addressToDeallocate - 16);
		EXPECT_EQ(TestAllocator<16>::lastDeallocatedSize, 96);
	}

	bool isConstructed{};
	bool isDestroyed{};

	struct TestAffix {
		uint64_t bytesAllocated;

		TestAffix() : bytesAllocated{} {
			isConstructed = true;
		}
		
		~TestAffix() {
			isDestroyed = true;
		}
	};

	TEST(AffixAllocatorTest, DefaultConstructsAnAffixOnAllocation) {
		isConstructed = false;

		AffixAllocator<TestAllocator<16>, TestAffix> prefixAllocator{};
		Block block{ prefixAllocator.allocate(16) };

		EXPECT_TRUE(isConstructed);

		prefixAllocator.deallocate(block);
	}

	TEST(AffixAllocatorTest, DestroysPrefixOnDeallocation) {
		isDestroyed = false;
		
		AffixAllocator<TestAllocator<16>, TestAffix> prefixAllocator{};
		Block block{ prefixAllocator.allocate(16) };

		prefixAllocator.deallocate(block);

		EXPECT_TRUE(isDestroyed);
	}

	TEST(AffixAllocatorTest, DestroysSuffixOnDeallocation) {
		isDestroyed = false;

		AffixAllocator<TestAllocator<16>, NoAffix, TestAffix> suffixAllocator{};
		Block block{ suffixAllocator.allocate(16) };

		suffixAllocator.deallocate(block);

		EXPECT_TRUE(isDestroyed);

	}

	TEST(AffixAllocatorTest, ReallocatesABlockUsingTheParentAllocator) {
		AffixAllocator<TestAllocator<8>, uint32_t, uint32_t> affixAllocator{};
		Block block{ affixAllocator.allocate(8) };

		uint32_t* prefix{ prefixFromInnerBlock<uint32_t>(block, 8) };
		uint32_t* suffix{ suffixFromInnerBlock<uint32_t>(block, 8) };

		*prefix = 1111U;
		*suffix = 2222U;

		bool success{ affixAllocator.reallocate(block, 16) };

		EXPECT_TRUE(success);
		EXPECT_EQ(block.length, 16);

		prefix = prefixFromInnerBlock<uint32_t>(block, 8);
		suffix = suffixFromInnerBlock<uint32_t>(block, 8);

		EXPECT_EQ(*prefix, 1111U);
		EXPECT_EQ(*suffix, 2222U);

		affixAllocator.deallocate(block);
	}

}

}