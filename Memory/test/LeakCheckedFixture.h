#pragma once

#include "LeakChecker.h"

#if defined _MSC_VER 

namespace Uwe {

namespace Memory {

	class LeakCheckedFixture : public ::testing::Test {

	protected:
		LeakChecker leakChecker;

	public:
		LeakCheckedFixture()
			: leakChecker{}
		{

		}
	};

}

}

#endif