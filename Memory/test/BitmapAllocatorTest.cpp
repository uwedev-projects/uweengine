#include "gtest/gtest.h"

#include "UE/Memory/BitmapAllocator.h"
#include "UE/Memory/Block.h"

#include "IsolationTest.h"
#include "TestAllocator.h"
#include "LeakChecker.h"

#include <cstddef>
#include <vector>
#include <utility>

namespace Uwe {

namespace Memory {

	using BitmappedAlloc = BitmapAllocator<TestAllocator<8>, 8, 256>;
	constexpr const std::size_t ArenaSize = 8ULL * 256ULL;

	using SmallBitmappedAlloc = BitmapAllocator<TestAllocator<8>, 8, 4>;

	TEST(BitmapAllocatorTest, DoesNotLeakMemoryOnDestruction) {
		{
			LeakChecker leakDetector{};
			BitmappedAlloc alloc{};
		}
	}

	TEST(BitmapAllocatorTest, FreesAllocatedBlocksOnDestruction) {

		{
			LeakChecker leakDetector{};
			BitmappedAlloc alloc{};

			for (int i{}; i < 256; ++i)
				alloc.allocate(8ULL);
		}

	}

	TEST(BitmapAllocatorTest, AllocatesTheMemoryUpFrontFromItsParent) {
		BitmappedAlloc alloc{};
		EXPECT_EQ(TestAllocator<8>::lastAllocatedSize, ArenaSize);
	}

	TEST(BitmapAllocatorTest, DeallocatesTheParentsMemoryOnDestruction) {

		{
			BitmappedAlloc alloc{};
		}

		EXPECT_EQ(TestAllocator<8>::lastDeallocatedSize, ArenaSize);
	}

	TEST(BitmapAllocatorTest, AllocatesBlocksOfEightBytes) {
		BitmappedAlloc alloc{};
		Block block1{ alloc.allocate(8ULL) };

		EXPECT_EQ(block1.length, 8ULL);
		EXPECT_NE(block1.ptr, nullptr);

		Block block2{ alloc.allocate(8ULL) };

		EXPECT_EQ(block2.length, 8ULL);
		EXPECT_NE(block2.ptr, nullptr);
	}

	TEST(BitmapAllocatorTest, TheAllocatedSizeIsRoundedUpToBlockSize) {
		BitmappedAlloc alloc{};
		Block block{ alloc.allocate(6ULL) };

		EXPECT_NE(block.ptr, nullptr);
		EXPECT_EQ(block.length, 8ULL);
	}

	TEST(BitmapAllocatorTest, TheAllocatedSizeIsRoundedToBlockSizeForArrays) {
		BitmappedAlloc alloc{};
		Block array{ alloc.allocateArray(4ULL, 4U) };

		EXPECT_NE(array.ptr, nullptr);
		EXPECT_EQ(array.length, 8ULL * 4U);
	}

	TEST(BitmapAllocatorTest, TheAllocatedSizeIsRoundedUpToTheParentsAlignment) {
		BitmapAllocator<TestAllocator<16>, 8, 256> alloc{};
		Block block{ alloc.allocate(8ULL) };

		EXPECT_EQ(block.length, 16ULL);
	}

	TEST(BitmapAllocatorTest, AllocatedBlocksDoNotOverlap) {
		BitmappedAlloc alloc{};
		std::vector<Block> blocks{};

		for (int i{}; i < 256; ++i) {
			blocks.push_back(alloc.allocate(8ULL));
		}

		IsolationTest memCheck{ std::move(blocks) };
		memCheck.verifyMemoryBlocks();
	}

	TEST(BitmapAllocatorTest, AllocatesContiguousBlocksIfNoDeallocationOccurs) {
		BitmappedAlloc alloc{};
		std::vector<Block> blocks{};

		for (int i{}; i < 5; ++i) {
			blocks.push_back(alloc.allocate(8ULL));
		}

		for (int i{ 1 }; i < 5; ++i) {
			const Block& prevBlock{ blocks[i - 1] };
			const Block& currentBlock{ blocks[i] };

			ASSERT_NE(currentBlock.ptr, nullptr);
			EXPECT_EQ(currentBlock.length, 8ULL);

			auto blockDistance = std::abs(static_cast<std::byte*>(currentBlock.ptr) - static_cast<std::byte*>(prevBlock.ptr));

			EXPECT_EQ(blockDistance, 8ULL);
		}


	}

	TEST(BitmapAllocatorTest, AllocatesContiguousBlocksIfDeallocationOccursInLIFOOrder) {
		BitmappedAlloc alloc{};
		std::vector<Block> blocks{};

		for (int i{}; i < 5; ++i) {
			blocks.push_back(alloc.allocate(8ULL));
		}

		alloc.deallocate(blocks.back());
		blocks.pop_back();

		blocks.push_back(alloc.allocate(8ULL));

		alloc.deallocate(blocks.back());
		blocks.pop_back();

		alloc.deallocate(blocks.back());
		blocks.pop_back();

		blocks.push_back(alloc.allocate(8ULL));
		blocks.push_back(alloc.allocate(8ULL));

		for (int i{ 1 }; i < 5; ++i) {
			const Block& prevBlock{ blocks[i - 1] };
			const Block& currentBlock{ blocks[i] };

			ASSERT_NE(currentBlock.ptr, nullptr);
			EXPECT_EQ(currentBlock.length, 8ULL);

			auto blockDistance = std::abs(static_cast<std::byte*>(currentBlock.ptr) - static_cast<std::byte*>(prevBlock.ptr));

			EXPECT_EQ(blockDistance, 8ULL);
		}
	}

	TEST(BitmapAllocatorTest, OnlyAllocatesFromItsParentOnConstruction) {
		BitmappedAlloc alloc{};
		void* lastAllocation{ TestAllocator<8>::lastAllocated };

		alloc.allocate(8ULL);
		alloc.allocateArray(8ULL, 4U);
		alloc.allocate(8ULL);

		EXPECT_EQ(TestAllocator<8>::lastAllocated, lastAllocation);
	}

	TEST(BitmapAllocatorTest, ReturnsItsCurrentSizeInBytesWhenUsingSingleBlocks) {
		BitmappedAlloc alloc{};
		EXPECT_EQ(alloc.currentSize(), 0ULL);

		std::vector<Block> blocks{};
		blocks.push_back(alloc.allocate(8ULL));
		EXPECT_EQ(alloc.currentSize(), 8ULL);

		blocks.push_back(alloc.allocate(8ULL));
		EXPECT_EQ(alloc.currentSize(), 16ULL);

		blocks.push_back(alloc.allocate(8ULL));
		EXPECT_EQ(alloc.currentSize(), 24ULL);

		alloc.deallocate(blocks.back());
		blocks.pop_back();
		EXPECT_EQ(alloc.currentSize(), 16ULL);

		alloc.deallocate(blocks.back());
		blocks.pop_back();
		EXPECT_EQ(alloc.currentSize(), 8ULL);
	}

	TEST(BitmapAllocatorTest, ReturnsItsCurrentSizeInBytesWhenUsingArrayBlocks) {
		BitmappedAlloc alloc{};
		std::vector<Block> arrayBlocks{};

		arrayBlocks.push_back(alloc.allocateArray(8ULL, 4U));
		EXPECT_EQ(alloc.currentSize(), 8ULL * 4U);

		arrayBlocks.push_back(alloc.allocateArray(8ULL, 2U));
		EXPECT_EQ(alloc.currentSize(), 8ULL * 6U);

		alloc.deallocateArray(arrayBlocks.front(), 4U);
		EXPECT_EQ(alloc.currentSize(), 8ULL * 2U);

		alloc.deallocateArray(arrayBlocks.back(), 2U);
		EXPECT_EQ(alloc.currentSize(), 0ULL);
	}

	TEST(BitmapAllocatorTest, ReturnsTheBytesRemainingForSingleBlocks) {
		BitmappedAlloc alloc{};
		EXPECT_EQ(alloc.bytesRemaining(), 256ULL * 8ULL);

		std::vector<Block> blocks{};

		blocks.push_back(alloc.allocate(8ULL));
		EXPECT_EQ(alloc.bytesRemaining(), 255ULL * 8ULL);

		blocks.push_back(alloc.allocate(8ULL));
		EXPECT_EQ(alloc.bytesRemaining(), 254ULL * 8ULL);

		alloc.deallocate(blocks.back());
		EXPECT_EQ(alloc.bytesRemaining(), 255ULL * 8ULL);
	}

	TEST(BitmapAllocatorTest, ReturnsTheBytesRemainingForArrayBlocks) {
		BitmappedAlloc alloc{};
		std::vector<Block> arrayBlocks{};

		arrayBlocks.push_back(alloc.allocateArray(8ULL, 64U));
		EXPECT_EQ(alloc.bytesRemaining(), 192ULL * 8ULL);

		arrayBlocks.push_back(alloc.allocateArray(8ULL, 64U));
		EXPECT_EQ(alloc.bytesRemaining(), 128ULL * 8ULL);

		alloc.deallocateArray(arrayBlocks.back(), 64U);
		EXPECT_EQ(alloc.bytesRemaining(), 192ULL * 8ULL);
	}

	TEST(BitmapAllocatorTest, DoesNotAllocateMoreThanItsCapacity) {
		BitmappedAlloc alloc{};

		for (size_t allocated{}; allocated < BitmappedAlloc::capacity; allocated += 8ULL) {
			Block block{ alloc.allocate(8ULL) };
			ASSERT_NE(block.ptr, nullptr);
		}

		Block block{ alloc.allocate(8ULL) };
		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0ULL);
	}

	TEST(BitmapAllocatorTest, DeallocationsMakeRoomForNewAllocations) {
		SmallBitmappedAlloc alloc{};
		std::vector<Block> blocks{};

		blocks.push_back(alloc.allocate(8ULL));
		blocks.push_back(alloc.allocate(8ULL));
		blocks.push_back(alloc.allocate(8ULL));
		blocks.push_back(alloc.allocate(8ULL));

		alloc.deallocate(blocks.back());
		blocks.pop_back();

		Block last{ alloc.allocate(8ULL) };
		EXPECT_NE(last.ptr, nullptr);
		EXPECT_EQ(last.length, 8ULL);
	}

	TEST(BitmapAllocatorTest, DeallocatingArraysMakesRoomForNewAllocations) {
		BitmappedAlloc alloc{};
		std::vector<Block> arrayBlocks{};

		arrayBlocks.push_back(alloc.allocateArray(8ULL, 192U));
		arrayBlocks.push_back(alloc.allocateArray(8ULL, 32U));
		arrayBlocks.push_back(alloc.allocateArray(8ULL, 32U));

		alloc.deallocateArray(arrayBlocks.back(), 32U);
		arrayBlocks.pop_back();

		arrayBlocks.push_back(alloc.allocateArray(8ULL, 32U));
		Block last{ arrayBlocks.back() };

		EXPECT_NE(last.ptr, nullptr);
		EXPECT_EQ(last.length, 8ULL * 32ULL);

		alloc.deallocateArray(arrayBlocks.front(), 192U);
		arrayBlocks.push_back(alloc.allocateArray(8ULL, 128U));
		last = arrayBlocks.back();

		EXPECT_NE(last.ptr, nullptr);
		EXPECT_EQ(last.length, 8ULL * 128ULL);

		arrayBlocks.push_back(alloc.allocateArray(8ULL, 64U));
		last = arrayBlocks.back();

		EXPECT_NE(last.ptr, nullptr);
		EXPECT_EQ(last.length, 8ULL * 64U);
	}

	TEST(BitmapAllocatorTest, DeallocationResetsTheBlock) {
		BitmappedAlloc alloc{};
		Block block{ alloc.allocate(8ULL) };
		alloc.deallocate(block);

		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0ULL);
	}

	TEST(BitmapAllocatorTest, DeallocationResetsTheArrayBlock) {
		BitmappedAlloc alloc{};
		Block array{ alloc.allocateArray(8ULL, 4U) };
		alloc.deallocateArray(array, 4U);

		EXPECT_EQ(array.ptr, nullptr);
		EXPECT_EQ(array.length, 0ULL);
	}

	TEST(BitmapAllocatorTest, ReturnsEmptyBlockWhenAllocatingMoreThanBlockSizeBytes) {
		BitmappedAlloc alloc{};
		Block block{ alloc.allocate(9ULL) };

		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0ULL);
	}

	TEST(BitmapAllocatorTest, ChecksIfBlockIsOwned) {
		BitmappedAlloc alloc1{}, alloc2{};

		Block block1{ alloc1.allocate(8ULL) };
		EXPECT_TRUE(alloc1.owns(block1));

		Block block2{ static_cast<std::byte*>(block1.ptr) - 8, 8ULL };
		EXPECT_FALSE(alloc1.owns(block2));

		Block block3{ static_cast<std::byte*>(block1.ptr) + ArenaSize + 1, 8ULL };
		EXPECT_FALSE(alloc1.owns(block3));

		Block block4{ alloc2.allocate(8ULL) };
		EXPECT_TRUE(alloc2.owns(block4));
		EXPECT_FALSE(alloc2.owns(block1));
		EXPECT_FALSE(alloc1.owns(block4));
	}

	TEST(BitmapAllocatorTest, ChecksIfArrayBlockIsOwned) {
		BitmappedAlloc alloc1{}, alloc2{};

		Block block1{ alloc1.allocateArray(8ULL, 4U) };
		Block block2{ alloc2.allocateArray(8ULL, 4U) };
		Block block3{ static_cast<std::byte*>(block1.ptr) + ArenaSize + 1, 8ULL };
		Block block4{ static_cast<std::byte*>(block2.ptr) - 1, 8ULL };

		EXPECT_TRUE(alloc1.owns(block1));
		EXPECT_TRUE(alloc2.owns(block2));

		EXPECT_FALSE(alloc1.owns(block2));
		EXPECT_FALSE(alloc2.owns(block1));

		EXPECT_FALSE(alloc1.owns(block3));
		EXPECT_FALSE(alloc2.owns(block4));
	}

	TEST(BitmapAllocatorTest, AllocatesMultipleBlocksAtOnce) {
		BitmappedAlloc alloc{};

		Block array{ alloc.allocateArray(8ULL, 8U) };
		EXPECT_NE(array.ptr, nullptr);
		EXPECT_EQ(array.length, 64ULL);
	}

	TEST(BitmapAllocatorTest, AllocatedArrayBlocksDoNotOverlap) {
		BitmappedAlloc alloc{};

		Block array1{ alloc.allocateArray(8ULL, 4U) };
		Block array2{ alloc.allocateArray(8ULL, 4U) };

		std::vector<Block> blocks{};
		for (std::size_t offset{}; offset < 8ULL * 4ULL; offset += 8ULL) {
			blocks.push_back({ static_cast<std::byte*>(array1.ptr) + offset, 8ULL });
			blocks.push_back({ static_cast<std::byte*>(array2.ptr) + offset, 8ULL });
		}

		IsolationTest memCheck{ std::move(blocks) };
		memCheck.verifyMemoryBlocks();
	}

	TEST(BitmapAllocatorTest, AllocatedArrayBlocksDoNotOverlapDueToPriorDeallocations) {
		BitmappedAlloc alloc{};
		std::vector<Block> arrayBlocks{};

		arrayBlocks.push_back(alloc.allocateArray(8ULL, 4U));
		arrayBlocks.push_back(alloc.allocateArray(8ULL, 4U));
		arrayBlocks.push_back(alloc.allocateArray(8ULL, 4U));

		alloc.deallocateArray(arrayBlocks[1], 4U);
		arrayBlocks[1] = alloc.allocateArray(8ULL, 4U);

		IsolationTest memCheck{ std::move(arrayBlocks) };
		memCheck.verifyMemoryBlocks();
	}

	TEST(BitmapAllocatorTest, ReturnsEmptyBlockWhenAllocatingArrayLargerThanCapacity) {
		BitmappedAlloc alloc{};
		Block block{ alloc.allocateArray(8ULL, 512U) };

		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0ULL);
	}

	TEST(BitmapAllocatorTest, DoesNotAllocateMoreThanItsCapacityWhenAllocatingArrays) {
		BitmappedAlloc alloc{};
		std::vector<Block> arrayBlocks{};

		for (int i{}; i < 4; ++i)
			arrayBlocks.push_back(alloc.allocateArray(8ULL, 64ULL));

		std::for_each(arrayBlocks.begin(), arrayBlocks.end(), [](const Block& block) {
			ASSERT_NE(block.ptr, nullptr);
			ASSERT_EQ(block.length, 8ULL * 64ULL);
		});

		Block lastArray{ alloc.allocateArray(8ULL, 64ULL) };
		ASSERT_EQ(lastArray.ptr, nullptr);
		ASSERT_EQ(lastArray.length, 0ULL);

		Block lastBlock{ alloc.allocate(8ULL) };
		ASSERT_EQ(lastBlock.ptr, nullptr);
		ASSERT_EQ(lastBlock.length, 0ULL);
	}

	TEST(BitmapAllocatorTest, DeallocatesAllBlocks) {
		SmallBitmappedAlloc alloc{};

		while (alloc.bytesRemaining() >= SmallBitmappedAlloc::blockSize) {
			alloc.allocate(SmallBitmappedAlloc::blockSize);
		}

		alloc.deallocateAll();

		EXPECT_EQ(alloc.bytesRemaining(), SmallBitmappedAlloc::capacity);
	}
}

}