#pragma once

#include "UE/Memory/Block.h"

#include <cstddef>
#include <vector>

namespace Uwe {

namespace Memory {
	
	class IsolationTest {
	protected:
		static constexpr unsigned char UnusedMemory{ 0x00 };

		std::vector<Block> blocks;
		std::size_t corruptedBlockIdx;
		std::size_t corruptionSourceIdx;

		template <typename T>
		void populateBlocks(T first) {
			blocks.push_back(first);
		}

		template <typename T, typename... Args>
		void populateBlocks(T first, Args... args) {
			blocks.push_back(first);
			populateBlocks(args...);
		}

		bool verifyBlocksUpTo(size_t n);

	public:

		template <typename T, typename... Args>
		IsolationTest(T first, Args... args)
			: blocks{}, corruptedBlockIdx{}, corruptionSourceIdx{}
		{
			blocks.push_back(first);
			populateBlocks(args...);
		}

		IsolationTest(std::vector<Block>&& blocks) noexcept 
			: blocks{ blocks }, corruptedBlockIdx{}, corruptionSourceIdx{}
		{}

		void verifyMemoryBlocks();
	};

}

}