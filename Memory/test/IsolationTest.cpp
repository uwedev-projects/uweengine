#include "IsolationTest.h"

#include <algorithm>
#include <cstdlib>
#include <sstream>
#include <string>

#include "gtest/gtest.h"

namespace Uwe {

namespace Memory {

	void IsolationTest::verifyMemoryBlocks() {
		bool testResult{ true };

		std::for_each(blocks.begin(), blocks.end(), [](const Block& block) {
			if (!block)
				FAIL() << "Block address is nullptr, expected valid address.";

			std::fill_n(reinterpret_cast<unsigned char*>(block.ptr), block.length, UnusedMemory);
		});

		for (size_t i{}; testResult && i < blocks.size(); ++i) {
			const Block& current{ blocks.at(i) };
			std::fill_n(reinterpret_cast<unsigned char*>(current.ptr), current.length, static_cast<unsigned char>(i + 1));
			testResult = verifyBlocksUpTo(i);

			if (!testResult) {
				const auto& first = blocks.at(corruptedBlockIdx);
				const auto& second = blocks.at(corruptionSourceIdx);

				std::stringstream errBuf{};
				errBuf << "Memory corruption(s) detected! Affected blocks:\n"
					<< "Block #" << std::to_string(corruptedBlockIdx) << " - Address: " << std::hex
					<< first.ptr << " Size: " << std::dec << std::to_string(first.length) << " bytes \n"
					<< "Block #" << std::to_string(corruptionSourceIdx) << " - Address: " << std::hex
					<< second.ptr << " Size: " << std::dec << std::to_string(second.length) << " bytes \n";

				FAIL() << errBuf.str();
			}
		}

	}

	bool IsolationTest::verifyBlocksUpTo(size_t n) {
		bool testResult{ true };

		for (size_t i{}; testResult && i < blocks.size(); ++i) {
			
			const Block& blk{ blocks.at(i) };
			unsigned char* expectedBlock{ static_cast<unsigned char*>(std::malloc(blk.length)) };

			if (i <= n) {
				std::fill_n(expectedBlock, blk.length, static_cast<unsigned char>(i + 1));
			}
			else {
				std::fill_n(expectedBlock, blk.length, UnusedMemory);
			}

			testResult = std::memcmp(blk.ptr, static_cast<void*>(expectedBlock), blk.length) == 0;

			if (!testResult) {
				corruptedBlockIdx = i;
				corruptionSourceIdx = n;
			}

			std::free(expectedBlock);
		}

		return testResult;
	}



}

}