#include "gtest/gtest.h"

#include "UE/Memory/FallbackAllocator.h"
#include "UE/Memory/NullAllocator.h"
#include "UE/Memory/Block.h"
#include "TestAllocator.h"

namespace Uwe {

namespace Memory {

	using Allocator = FallbackAllocator<TestAllocator<16>, NullAllocator>;
	using ReverseAllocator = FallbackAllocator<NullAllocator, TestAllocator<16>>;

	TEST(FallbackAllocatorTest, ReturnsEmptyBlockWhenRequestingZeroBytes) {
		Allocator fallbackAllocator{};
		Block block{ fallbackAllocator.allocate(0) };

		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0);
	}

	TEST(FallbackAllocatorTest, AllocatesFromThePrimaryAllocatorIfPossible) {
		Allocator fallbackAllocator{};
		Block block{ fallbackAllocator.allocate(4) };

		EXPECT_EQ(block.ptr, TestAllocator<16>::lastAllocated);
		EXPECT_EQ(block.length, 16);

		fallbackAllocator.deallocate(block);
	}

	TEST(FallbackAllocatorTest, AllocatesFromTheFallbackIfPrimaryFails) {

		ReverseAllocator fallbackAllocator{};
		Block block{ fallbackAllocator.allocate(8) };

		EXPECT_EQ(block.ptr, TestAllocator<16>::lastAllocated);
		EXPECT_EQ(block.length, 16);

		fallbackAllocator.deallocate(block);

	}

	TEST(FallbackAllocatorTest, DelegatesDeallocationToPrimaryAllocator) {

		Allocator fallbackAllocator{};
		Block block{ fallbackAllocator.allocate(8) };
		void* blockAddress{ block.ptr };

		fallbackAllocator.deallocate(block);
		EXPECT_EQ(TestAllocator<16>::lastDeallocated, blockAddress);
	}

	TEST(FallbackAllocatorTest, DelegatesDeallocationToFallbackAllocator) {
		ReverseAllocator fallbackAllocator{};

		auto alignment = static_cast<std::align_val_t>(16);
		void* blockAddress{ ::operator new(16, alignment) };

		Block block{ static_cast<std::byte*>(blockAddress), 16 };

		fallbackAllocator.deallocate(block);
		EXPECT_EQ(TestAllocator<16>::lastDeallocated, blockAddress);
	}

	TEST(FallbackAllocatorTest, ReallocatesUsingThePrimaryAllocator) {
		Allocator fallbackAllocator{};
		Block block{ fallbackAllocator.allocate(16) };

		bool success{ fallbackAllocator.reallocate(block, 32) };

		EXPECT_TRUE(success);
		EXPECT_EQ(block.length, 32);
		EXPECT_EQ(block.ptr, TestAllocator<16>::lastAllocated);

		fallbackAllocator.deallocate(block);
	}

	TEST(FallbackAllocatorTest, ReallocatesUsingTheFallbackAllocator) {
		ReverseAllocator fallbackAllocator{};

		auto alignment = static_cast<std::align_val_t>(16);
		void* blockAddress = ::operator new(16, alignment);
		Block block{ static_cast<std::byte*>(blockAddress) , 16 };

		bool success{ fallbackAllocator.reallocate(block, 32) };

		EXPECT_TRUE(success);
		EXPECT_EQ(block.length, 32);
		EXPECT_EQ(block.ptr, TestAllocator<16>::lastAllocated);

		fallbackAllocator.deallocate(block);
	}


}

}