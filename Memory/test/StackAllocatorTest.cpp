#include "gtest/gtest.h"

#include "UE/Memory/StackAllocator.h"
#include "UE/Memory/Block.h"

#include "TestAllocator.h"

#include <vector>

namespace Uwe {

	namespace Memory {

		TEST(StackAllocatorTest, DeallocatesABlock) {
			StackAllocator<TestAllocator<16>, 128> stackAllocator{};
			Block block{ stackAllocator.allocate(32ULL) };

			EXPECT_EQ(stackAllocator.getCurrentSize(), 32ULL);
			stackAllocator.deallocate(block);

			EXPECT_EQ(stackAllocator.getCurrentSize(), 0ULL);
			EXPECT_EQ(block.ptr, nullptr);
			EXPECT_EQ(block.length, 0ULL);
		}

		TEST(StackAllocatorTest, DeallocatesTheLastAllocatedBlock) {
			StackAllocator<TestAllocator<16>, 128> stackAllocator{};
			Block first{ stackAllocator.allocate(16ULL) };
			Block second{ stackAllocator.allocate(16ULL) };

			stackAllocator.deallocate(second);

			EXPECT_EQ(second.ptr, nullptr);
			EXPECT_EQ(second.length, 0ULL);
			EXPECT_EQ(stackAllocator.getCurrentSize(), 16ULL);
		}

		TEST(StackAllocatorTest, DeallocatesTheLastAllocatedArrayBlock) {
			StackAllocator<TestAllocator<16>, 128> stackAllocator{};

			Block first{ stackAllocator.allocate(16ULL) };
			Block second{ stackAllocator.allocateArray(16ULL, 2U) };
			Block third{ stackAllocator.allocateArray(16ULL, 4U) };

			stackAllocator.deallocateArray(third, 4U);

			EXPECT_EQ(third.ptr, nullptr);
			EXPECT_EQ(third.length, 0ULL);
			EXPECT_EQ(stackAllocator.getCurrentSize(), 48ULL);

			stackAllocator.deallocateArray(second, 2U);

			EXPECT_EQ(second.ptr, nullptr);
			EXPECT_EQ(second.length, 0ULL);
			EXPECT_EQ(stackAllocator.getCurrentSize(), 16ULL);
		}

		TEST(StackAllocatorTest, DoesNotDeallocateWhenBlockIsNotTheLastAllocated) {
			StackAllocator<TestAllocator<16>, 128> stackAllocator{};
			Block first{ stackAllocator.allocate(16ULL) };
			Block second{ stackAllocator.allocate(16ULL) };

			const void* startOfFirst{ first.ptr };

			stackAllocator.deallocate(first);

			EXPECT_NE(first.ptr, nullptr);
			EXPECT_EQ(first.ptr, startOfFirst);
			EXPECT_EQ(first.length, 16ULL);
		}

		TEST(StackAllocatorTest, DoesNotDeallocateWhenArrayBlockIsNotTheLastAllocated) {
			StackAllocator<TestAllocator<16>, 128> stackAllocator{};
			Block first{ stackAllocator.allocateArray(16ULL, 2U) };
			Block second{ stackAllocator.allocateArray(16ULL, 2U) };

			const void* startOfFirst{ first.ptr };

			stackAllocator.deallocateArray(first, 2U);

			EXPECT_NE(first.ptr, nullptr);
			EXPECT_EQ(first.ptr, startOfFirst);
			EXPECT_EQ(first.length, 32ULL);
		}

		TEST(StackAllocatorTest, DeallocationsMakeRoomForNewBlocks) {
			StackAllocator<TestAllocator<8>, 128> stackAllocator{};
			std::vector<Block> blocks{};

			for (int i{}; i < 16; ++i) {
				blocks.push_back(stackAllocator.allocate(8ULL));
			}
			
			stackAllocator.deallocate(blocks.back());
			blocks.pop_back();

			EXPECT_EQ(stackAllocator.getCurrentSize(), 120ULL);

			Block newBlock{ stackAllocator.allocate(8ULL) };
			EXPECT_NE(newBlock.ptr, nullptr);
			EXPECT_EQ(newBlock.length, 8ULL);
		}

		TEST(StackAllocatorTest, DeallocationsMakeRoomForNewArrayBlocks) {
			StackAllocator<TestAllocator<8>, 128> stackAllocator{};
			std::vector<Block> blocks{};

			for (int i{}; i < 8; ++i) {
				blocks.push_back(stackAllocator.allocateArray(8ULL, 2U));
			}

			stackAllocator.deallocateArray(blocks.back(), 2U);
			blocks.pop_back();

			EXPECT_EQ(stackAllocator.getCurrentSize(), 112ULL);

			Block newBlock{ stackAllocator.allocateArray(8ULL, 2U) };
			EXPECT_NE(newBlock.ptr, nullptr);
			EXPECT_EQ(newBlock.length, 16ULL);
		}
	}

}