#pragma once

#include <sstream>
#include "gtest/gtest.h"

#if defined _MSC_VER 

#include <crtdbg.h>

namespace Uwe {

namespace Memory {

	class LeakChecker {

	protected:
		_CrtMemState state1;
		_CrtMemState state2;
		_CrtMemState state3;

	public:
		LeakChecker();
		~LeakChecker();
	};

}

}

#endif