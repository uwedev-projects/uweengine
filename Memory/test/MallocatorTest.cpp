#include "gtest/gtest.h"

#include "UE/Memory/Mallocator.h"
#include "UE/Memory/Block.h"

namespace Uwe {

namespace Memory {

	TEST(MallocatorTest, AllocatesABlock) {
		Mallocator mallocator{};
		Block block{ mallocator.allocate(16) };

		EXPECT_NE(block.ptr, nullptr);
		EXPECT_EQ(block.length, 16);

		mallocator.deallocate(block);
	}

	TEST(MallocatorTest, ReturnsEmptyBlockWhenRequestingZeroBytes) {
		Mallocator mallocator{};
		Block empty{ mallocator.allocate(0) };

		EXPECT_EQ(empty.ptr, nullptr);
		EXPECT_EQ(empty.length, 0);
	}

	TEST(MallocatorTest, ResetsBlockOnDeallocation) {
		Mallocator mallocator{};
		Block block{ mallocator.allocate(8) };

		mallocator.deallocate(block);
		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0);
	}

	TEST(MallocatorTest, ReallocatesABlock) {
		Mallocator mallocator{};
		Block block{ mallocator.allocate(4) };

		bool success{ mallocator.reallocate(block, 16) };
		EXPECT_TRUE(success);
		EXPECT_EQ(block.length, 16);
	}

}

}