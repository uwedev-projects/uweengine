#include "gtest/gtest.h"

#include "Memory/BitmappedPool.h"
#include "Memory/Block.h"

#include "IsolationTest.h"
#include "TestAllocator.h"
#include "LeakCheckedFixture.h"

#include <algorithm>
#include <utility>
#include <vector>
#include <array>

namespace Uwe {

namespace Memory {

	using Platform::Bitmask64;

	class BitmappedPoolTest : public LeakCheckedFixture {};

	TEST_F(BitmappedPoolTest, AllocatesAPoolFromItsParent) {
		BitmappedPool<TestAllocator<64>, 64, 128> pool{};
		EXPECT_EQ(TestAllocator<64>::lastAllocatedSize, 64 * 128);
	}

	TEST_F(BitmappedPoolTest, DeallocatesThePoolOnDestruction) {
		void* startOfBlock{};

		{
			BitmappedPool<TestAllocator<64>, 64, 128> pool{};
			startOfBlock = TestAllocator<64>::lastAllocated;
		}

		EXPECT_EQ(TestAllocator<64>::lastDeallocated, startOfBlock);
		EXPECT_EQ(TestAllocator<64>::lastDeallocatedSize, 64 * 128);
	}

	using TestPool = BitmappedPool<TestAllocator<16>, 16, 128>;

	TEST_F(BitmappedPoolTest, AllocatesAFixedSizedBlock) {
		TestPool pool{};
		Block block{ pool.allocate() };

		EXPECT_EQ(block.length, 16);
	}

	TEST_F(BitmappedPoolTest, AllocatesMultipleBlocks) {
		TestPool pool{};
		Block first{ pool.allocate() };
		Block second{ pool.allocate() };

		EXPECT_NE(first.ptr, second.ptr);

		auto pointerDelta = std::abs(static_cast<char*>(second.ptr) - static_cast<char*>(first.ptr));
		EXPECT_GE(pointerDelta, static_cast<int>(first.length));

		EXPECT_EQ(first.length, 16);
		EXPECT_EQ(second.length, 16);

		IsolationTest memCheck{ first, second };
		memCheck.verifyMemoryBlocks();
	}

	TEST_F(BitmappedPoolTest, AllocatesAccrossMultiplePages) {
		BitmappedPool<TestAllocator<4>, 4, 2 * sizeof(Bitmask64) * CHAR_BIT> pool{};
		
		Block first{ pool.allocate() };
		void* previousAddress{ first.ptr };

		for (int i = 1; i < 2 * sizeof(Bitmask64) * CHAR_BIT; ++i) {
			Block block{ pool.allocate() };
			auto pointerDelta = std::abs(static_cast<char*>(block.ptr) - static_cast<char*>(previousAddress));

			EXPECT_EQ(block.length, 4);
			EXPECT_GE(pointerDelta, 4);

			previousAddress = block.ptr;
		}

	}

	TEST_F(BitmappedPoolTest, ReturnsAnEmptyBlockWhenMemoryIsExhausted) {
		TestPool pool{};

		for (int i{}; i < 128; ++i)
			pool.allocate();

		Block block{ pool.allocate() };
		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0ULL);
	}

	TEST_F(BitmappedPoolTest, ReturnsAnEmptyMultiBlockWhenMemoryIsExhausted) {
		TestPool pool{};

		pool.allocate(TestPool::blockSize * 96ULL);

		for (int i{}; i < 32; ++i) {
			pool.allocate();
		}

		Block block{ pool.allocate(TestPool::blockSize * 16ULL) };
		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0ULL);
	}

	TEST_F(BitmappedPoolTest, DellocatesAFixedSizedBlock) {
		TestPool pool{};
		Block block{ pool.allocate() };
		void* oldAddress{ block.ptr };

		pool.deallocate(block);
		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0);

		block = pool.allocate();
		EXPECT_EQ(block.ptr, oldAddress);
		EXPECT_EQ(block.length, 16);
	}

	TEST_F(BitmappedPoolTest, DeallocatesAccrossPages) {
		BitmappedPool<TestAllocator<4>, 4, 2 * sizeof(Bitmask64) * CHAR_BIT> pool{};

		for (int i = 0; i < sizeof(Bitmask64) * CHAR_BIT - 1; ++i) {
			Block block{ pool.allocate() };
		}

		Block lastOfFirstPage{ pool.allocate() };
		Block firstOfSecondPage{ pool.allocate() };

		pool.deallocate(lastOfFirstPage);
		EXPECT_EQ(lastOfFirstPage.ptr, nullptr);
		EXPECT_EQ(lastOfFirstPage.length, 0);

		pool.deallocate(firstOfSecondPage);
		EXPECT_EQ(firstOfSecondPage.ptr, nullptr);
		EXPECT_EQ(firstOfSecondPage.length, 0);
	}

	TEST_F(BitmappedPoolTest, InformsWhetherBlockIsOwned) {
		TestPool pool1{}, pool2{};

		Block block1{ pool1.allocate() };
		Block block2{ pool2.allocate() };

		EXPECT_TRUE(pool1.owns(block1));
		EXPECT_FALSE(pool2.owns(block1));

		EXPECT_TRUE(pool2.owns(block2));
		EXPECT_FALSE(pool1.owns(block2));
	}

	TEST_F(BitmappedPoolTest, UsesItsParentsAlignmentRequirements) {
		TestPool pool{};

		Block block1{ pool.allocate() };
		uint8_t lsb{ reinterpret_cast<uintptr_t>(block1.ptr) & 0x0F };

		EXPECT_EQ(lsb, 0x00);

		Block block2{ pool.allocate() };
		lsb = reinterpret_cast<uintptr_t>(block2.ptr) & 0x0F;

		EXPECT_EQ(lsb, 0x00);
	}

	TEST_F(BitmappedPoolTest, CalculatesTheNumberOfBitmapsNeeded) {
		using PoolAllocator1 = BitmappedPool<TestAllocator<8U>, 8U, 12U>;
		EXPECT_EQ(PoolAllocator1::numBitmaps, 2ULL);

		using PoolAllocator2 = BitmappedPool<TestAllocator<8U>, 8U, 80U>;
		EXPECT_EQ(PoolAllocator2::numBitmaps, 2ULL);

		using PoolAllocator3 = BitmappedPool<TestAllocator<8U>, 8U, 128U>;
		EXPECT_EQ(PoolAllocator3::numBitmaps, 2ULL);

		using PoolAllocator4 = BitmappedPool<TestAllocator<8U>, 8U, 160U>;
		EXPECT_EQ(PoolAllocator4::numBitmaps, 4ULL);

		using PoolAllocator5 = BitmappedPool<TestAllocator<8U>, 8U, 220U>;
		EXPECT_EQ(PoolAllocator5::numBitmaps, 4ULL);

		using PoolAllocator6 = BitmappedPool<TestAllocator<8U>, 8U, 300U>;
		EXPECT_EQ(PoolAllocator6::numBitmaps, 6ULL);
	}

	TEST_F(BitmappedPoolTest, AllocatesMultipleContiguosBlocksAtOnce) {
		BitmappedPool<TestAllocator<8U>, 8U, 256U> pool{};
		Block block1{ pool.allocate(256U) };
		Block block2{ pool.allocate(256U) };

		EXPECT_NE(block1.ptr, nullptr);
		EXPECT_EQ(block1.length, 256U);

		EXPECT_NE(block2.ptr, nullptr);
		EXPECT_EQ(block2.length, 256U);

		auto pointerDelta = std::abs(static_cast<char*>(block2.ptr) - static_cast<char*>(block1.ptr));
		EXPECT_GE(pointerDelta, 256U);

		IsolationTest memCheck{ block1, block2 };
		memCheck.verifyMemoryBlocks();
	}

	TEST_F(BitmappedPoolTest, AllocatesSingleFollowedByMultiBlocks) {
		BitmappedPool<TestAllocator<8U>, 8U, 256U> pool{};
		Block block1{ pool.allocate() };
		Block block2{ pool.allocate() };
		Block multiBlock{ pool.allocate(256U) };

		auto pointerDelta = std::abs(static_cast<char*>(multiBlock.ptr) - static_cast<char*>(block1.ptr));
		EXPECT_GE(pointerDelta, 16U);
		EXPECT_EQ(multiBlock.length, 256U);

		IsolationTest memCheck{ block1, block2, multiBlock };
		memCheck.verifyMemoryBlocks();
	}

	TEST_F(BitmappedPoolTest, AllocatesMultiFollowedBySingleBlocks) {
		BitmappedPool<TestAllocator<4ULL>, 4ULL, 128ULL> pool{};

		Block multiBlock{ pool.allocate(4ULL * 120ULL) };
		Block singleBlock{ pool.allocate() };

		auto pointerDelta = std::abs(static_cast<char*>(singleBlock.ptr) - static_cast<char*>(multiBlock.ptr));
		EXPECT_GE(pointerDelta, static_cast<ptrdiff_t>(multiBlock.length));

		IsolationTest memCheck{ multiBlock, singleBlock };
		memCheck.verifyMemoryBlocks();
	}

	TEST_F(BitmappedPoolTest, AlternatesBetweenSingleAndMultiBlockAllocations) {
		BitmappedPool<TestAllocator<8U>, 8U, 1024U> pool{};
		std::vector<Block> blocks{};

		blocks.push_back(pool.allocate(8U * 140U));

		for (std::size_t i{}; i < 52U; ++i) {
			blocks.push_back(pool.allocate(8U));
		}

		Block last{ pool.allocate(8U * 64U) };

		EXPECT_NE(last.ptr, nullptr);
		EXPECT_EQ(last.length, 8U * 64U);

		IsolationTest memCheck{ std::move(blocks) };
		memCheck.verifyMemoryBlocks();

	}

	TEST_F(BitmappedPoolTest, RoundsSmallerAllocationsToBlockSize) {
		BitmappedPool<TestAllocator<16U>, 16U, 64U> pool{};
		Block block1{ pool.allocate(8U) };
		Block block2{ pool.allocate(4U) };
		Block block3{ pool.allocate(2U) };

		EXPECT_NE(block1.ptr, nullptr);
		EXPECT_EQ(block1.length, 16U);

		EXPECT_NE(block2.ptr, nullptr);
		EXPECT_EQ(block2.length, 16U);

		EXPECT_NE(block3.ptr, nullptr);
		EXPECT_EQ(block3.length, 16U);

		IsolationTest memCheck{ block1, block2, block3 };
		memCheck.verifyMemoryBlocks();
	}

	TEST_F(BitmappedPoolTest, CausesNoCollisionsWhenAllocatingDifferentSizes) {
		BitmappedPool<TestAllocator<16U>, 16U, 1024> pool{};
		std::array<const size_t, 4ULL> sizeClasses{ 
			16ULL,
			16ULL * 8ULL,
			16ULL * 13ULL, 
			16ULL * 40ULL 
		};

		constexpr size_t allocationsPerClass{ 4ULL };

		std::vector<Block> blocks{};
		blocks.reserve(sizeClasses.size() * allocationsPerClass);

		for (const auto size : sizeClasses) {
			for (size_t i{}; i < allocationsPerClass; ++i) {
				Block current{ pool.allocate(size) };

				EXPECT_NE(current.ptr, nullptr);
				EXPECT_EQ(current.length, size);

				blocks.emplace_back(current);
			}
		}

		IsolationTest memCheck{ std::move(blocks) };
		memCheck.verifyMemoryBlocks();
	}

	TEST_F(BitmappedPoolTest, FailsWhenMultiBlockCantBePlacedOnOneByteBoundaryInBitmap) {
		BitmappedPool<TestAllocator<4ULL>, 4ULL, 128ULL> pool{};

		Block multiBlock1{ pool.allocate(4ULL * 120ULL) };
		Block singleBlock{ pool.allocate() };

		auto pointerDelta = std::abs(static_cast<char*>(singleBlock.ptr) - static_cast<char*>(multiBlock1.ptr));
		ASSERT_GE(pointerDelta, static_cast<ptrdiff_t>(multiBlock1.length));

		// 7 slots available but the first one is already taken, so this should fail:
		Block multiBlock2{ pool.allocate(4ULL * 7ULL) };

		EXPECT_EQ(multiBlock2.ptr, nullptr);
		EXPECT_EQ(multiBlock2.length, 0ULL);

		IsolationTest memCheck{ multiBlock1, singleBlock };
		memCheck.verifyMemoryBlocks();
	}

	TEST_F(BitmappedPoolTest, GetsTheCurrentSize) {
	}

	TEST_F(BitmappedPoolTest, IsPowerOfTwoTest) {
		for (std::size_t i{ 1 }; i < std::numeric_limits<std::size_t>::max() / 2; i *= 2) {
			EXPECT_TRUE(isPowerOfTwo(i));
		}
	}

}

}