#pragma once

#include "UE/Memory/Block.h"
#include <cstddef>

namespace Uwe {

namespace Memory {

	template <std::size_t Alignment>
	class TestAllocator {
	public:
		static constexpr std::size_t alignment = Alignment;
		static constexpr std::size_t capacity  = 2ULL * 1024ULL * 1024ULL * 1024ULL;

		static std::byte* lastAllocated;
		static std::byte* lastDeallocated;
		static std::size_t lastAllocatedSize;
		static std::size_t lastDeallocatedSize;

		TestAllocator() {
			lastAllocated = nullptr;
			lastDeallocated = nullptr;

			lastAllocatedSize = 0U;
			lastDeallocatedSize = 0U;
		}

		Block allocate(std::size_t size) noexcept {
			Block result{};

			std::size_t alignedSize{ roundToAlignment(size, alignment) };

			void* blockAddress{ ::operator new(alignedSize, static_cast<std::align_val_t>(alignment)) };

			result.ptr		= static_cast<std::byte*>(blockAddress);
			result.length	= alignedSize;

			lastAllocated	  = result.ptr;
			lastAllocatedSize = result.length;

			return result;
		}

		bool reallocate(Block& blk, std::size_t desiredSize) noexcept {
			if (blk) {
				Block newBlock{ allocate(desiredSize) };
				copyBlock(blk, newBlock);

				deallocate(blk);
				blk = newBlock;

				return true;
			}

			return false;
		}

		void deallocate(Block& blk) noexcept {
			lastDeallocated = blk.ptr;
			lastDeallocatedSize = blk.length;

			::operator delete(blk.ptr, blk.length, static_cast<std::align_val_t>(alignment));
			blk.reset();
		}

		bool owns(const Block& blk) noexcept {
			return true;
		}

		//bool expand(Block& blk, std::size_t desiredSize) noexcept {
		//	return true;
		//}
	};

	template <std::size_t Alignment>
	constexpr std::size_t TestAllocator<Alignment>::alignment;

	template <std::size_t Alignment>
	std::byte* TestAllocator<Alignment>::lastDeallocated{ nullptr };

	template <std::size_t Alignment>
	std::byte* TestAllocator<Alignment>::lastAllocated{ nullptr };

	template <std::size_t Alignment>
	std::size_t TestAllocator<Alignment>::lastAllocatedSize{};

	template <std::size_t Alignment>
	std::size_t TestAllocator<Alignment>::lastDeallocatedSize{};
}

}