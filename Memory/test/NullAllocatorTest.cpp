#include "gtest/gtest.h"

#include "UE/Memory/Block.h"
#include "UE/Memory/NullAllocator.h"

#include "UE/EngineConfig.h"

namespace Uwe {

namespace Memory {
	
	TEST(NullAllocatorTest, AllocatesAnEmptyBlock) {
		NullAllocator allocator{};
		Block block{ allocator.allocate(8) };

		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0);
	}

	TEST(NullAllocatorTest, OwnsABlockWhenItIsEmpty) {
		NullAllocator allocator{};
		Block block{};

		EXPECT_TRUE(allocator.owns(block));

		std::byte data{ 0x40 };

		block.ptr    = &data;
		block.length = 32U;

		EXPECT_FALSE(allocator.owns(block));
	}

	TEST(NullAllocatorTest, ReturnsFalseWhenReallocatingEmptyBlock) {
		NullAllocator allocator{};
		Block block{};

		EXPECT_FALSE(allocator.reallocate(block, 32));
	}

	TEST(NullAllocatorTest, ReturnsFalseWhenExpandingEmptyBlock) {
		NullAllocator allocator{};
		Block block{};

		EXPECT_FALSE(allocator.expand(block, 16));
	}

	// Disable death tests for release builds
#if (UWE_BUILD_TYPE == UWE_BUILD_TYPE_DEBUG)

	TEST(NullAllocatorDeathTest, DiesWhenPassingNonEmptyBlockToDeallocate) {
		NullAllocator allocator{};
		std::byte data{ 0x40 };
		Block block{ &data, 32 };

		EXPECT_DEATH(allocator.deallocate(block), "");
	}

	TEST(NullAllocatorDeathTest, DiesWhenPassingNonEmptyBlockToReallocate) {
		NullAllocator allocator{};
		std::byte data{ 0x40 };
		Block block{ &data, 16 };

		EXPECT_DEATH(allocator.reallocate(block, 32), "");
	}

	TEST(NullAllocatorDeathTest, DiesWhenPassingNonEmptyBlockToExpand) {
		NullAllocator allocator{};
		std::byte data{ 0x40 };
		Block block{ &data, 4 };

		EXPECT_DEATH(allocator.expand(block, 8), "");
	}

#	endif

}

}