#include "gtest/gtest.h"

#include "UE/Memory/MonotonicBuffer.h"
#include "UE/Memory/STLAllocator.h"

#include "TestAllocator.h"
#include "LeakChecker.h"

#include <vector>
#include <array>

namespace Uwe {

namespace Memory {

	TEST(RoundToAlignmentTest, CalculatesTheNextBiggestSizeDivisibleByAlignment) {
		std::size_t alignedSize{ roundToAlignment(2, 4) };
		EXPECT_EQ(alignedSize, 4);

		alignedSize = roundToAlignment(1, 4);
		EXPECT_EQ(alignedSize, 4);

		alignedSize = roundToAlignment(4, 4);
		EXPECT_EQ(alignedSize, 4);

		alignedSize = roundToAlignment(2, 8);
		EXPECT_EQ(alignedSize, 8);

		alignedSize = roundToAlignment(4, 8);
		EXPECT_EQ(alignedSize, 8);

		alignedSize = roundToAlignment(8, 8);
		EXPECT_EQ(alignedSize, 8);

		alignedSize = roundToAlignment(12, 4);
		EXPECT_EQ(alignedSize, 12);

		alignedSize = roundToAlignment(9, 4);
		EXPECT_EQ(alignedSize, 12);
	}

	TEST(MonotonicBufferTest, AllocatesAnArenaFromItsParentOnConstruction) {
		MonotonicBuffer<TestAllocator<4>, 128> buffer{};
		EXPECT_EQ(TestAllocator<4>::lastAllocatedSize, 128);
	}

	TEST(MonotonicBufferTest, DeallocatesTheArenaOnDestruction) {
		
		{
			MonotonicBuffer<TestAllocator<4>, 128> buffer{};
		}
		
		EXPECT_EQ(TestAllocator<4>::lastDeallocatedSize, 128);
	}

	TEST(MonotonicBufferTest, AllocatesAlignedBlocksOfGivenSize) {

		MonotonicBuffer<TestAllocator<4>, 128> buffer{};

		Block block{ buffer.allocate(4) };

		EXPECT_NE(block.ptr, nullptr);
		EXPECT_EQ((uintptr_t) block.ptr & 3, 0);
		EXPECT_EQ(block.length, 4);
	}

	TEST(MonotonicBufferTest, DoesNotLeakMemoryOnDestruction) {

		{
			LeakChecker leakDetector{};
			MonotonicBuffer<TestAllocator<4>, 128> buffer{};
			buffer.allocate(8ULL);
			buffer.allocate(8ULL);
		}

	}

	TEST(MonotonicBufferTest, AllocatesDifferentSizesWithEightByteAlignment) {

		MonotonicBuffer<TestAllocator<8>, 128> buffer{};

		Block oneByte{ buffer.allocate(1) };
		Block twoByte{ buffer.allocate(2) };
		Block fourByte{ buffer.allocate(4) };
		Block eightByte{ buffer.allocate(8) };

		EXPECT_EQ((uintptr_t) oneByte.ptr & 7, 0);
		EXPECT_EQ((uintptr_t) twoByte.ptr & 7, 0);
		EXPECT_EQ((uintptr_t) fourByte.ptr & 7, 0);
		EXPECT_EQ((uintptr_t) eightByte.ptr & 7, 0);

		EXPECT_EQ(oneByte.length, 8);
		EXPECT_EQ(twoByte.length, 8);
		EXPECT_EQ(fourByte.length, 8);
		EXPECT_EQ(eightByte.length, 8);
	}

	TEST(MonotonicBufferTest, AllocatesSequentialBlocks) {

		MonotonicBuffer<TestAllocator<4>, 128> buffer{};
			
		Block first{ buffer.allocate(4) };
		Block second{ buffer.allocate(4) };
		Block third{ buffer.allocate(2) };

		EXPECT_LT(first.ptr, second.ptr);
		EXPECT_LT(second.ptr, third.ptr);

		EXPECT_EQ((uintptr_t) second.ptr - (uintptr_t) first.ptr, 4);
	}

	TEST(MonotonicBufferTest, ReturnsNullptrWhenCapacityIsExhausted) {
		MonotonicBuffer<TestAllocator<1>, 128> buffer{};

		Block bigBlock{ buffer.allocate(128) };

		EXPECT_NE(bigBlock.ptr, nullptr);
		EXPECT_EQ(bigBlock.length, 128);

		Block overflowingBlock{ buffer.allocate(1) };

		EXPECT_EQ(overflowingBlock.ptr, nullptr);
		EXPECT_EQ(overflowingBlock.length, 0);
	}

	TEST(MonotonicBufferTest, ReturnsNullptrWhenRequestedSizeIsZero) {
		MonotonicBuffer<TestAllocator<4>, 128> buffer{};

		Block block{ buffer.allocate(0) };
		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0);
	}

	TEST(MonotonicBufferTest, StatesIfAGivenBlockIsOwned) {
		MonotonicBuffer<TestAllocator<16>, 128> buffer{};

		Block block{ buffer.allocate(16) };
		bool isOwned{ buffer.owns(block) };

		EXPECT_TRUE(isOwned);
	}

	TEST(MonotonicBufferTest, StatesIfAGivenBlockIsNotOwned) {
		MonotonicBuffer<TestAllocator<16>, 128> firstBuffer{};
		MonotonicBuffer<TestAllocator<16>, 128> secondBuffer{};

		Block first{ firstBuffer.allocate(16) };
		Block second{ secondBuffer.allocate(16) };

		EXPECT_FALSE(firstBuffer.owns(second));
		EXPECT_FALSE(secondBuffer.owns(first));
	}

	TEST(MonotonicBufferTest, ReturnsTheCurrentSizeInBytes) {
		MonotonicBuffer<TestAllocator<16>, 128> buffer{};

		buffer.allocate(16);
		EXPECT_EQ(buffer.getCurrentSize(), 16);

		buffer.allocate(16);
		EXPECT_EQ(buffer.getCurrentSize(), 32);
	}

	TEST(MonotonicBufferTest, DeallocatesAllBlocks) {
		MonotonicBuffer<TestAllocator<1>, 128> buffer{};

		const auto [ startOfFirstBlock, len ] = buffer.allocate(64);
		buffer.allocate(64);

		Block block{ buffer.allocate(1) };
		EXPECT_EQ(block.length, 0);

		buffer.deallocateAll();
		block = buffer.allocate(128);

		EXPECT_EQ(block.ptr, startOfFirstBlock);
		EXPECT_EQ(block.length, 128);
	}

	TEST(MonotonicBufferTest, ExpandsBlockByRequestedNumberOfBytesInPlace) {
		MonotonicBuffer<TestAllocator<16>, 128> buffer{};

		Block block{ buffer.allocate(16) };

		void* startOfLastBlock{ block.ptr };

		bool success{ buffer.expand(block, 32) };
		EXPECT_TRUE(success);
		EXPECT_EQ(block.ptr, startOfLastBlock);
		EXPECT_EQ(block.length, 48);
	}

	TEST(MonotonicBufferTest, AllocatesAfterExpansion) {
		MonotonicBuffer<TestAllocator<16>, 128> buffer{};
		Block first{ buffer.allocate(16) };
		buffer.expand(first, 16);

		Block second{ buffer.allocate(16) };
		EXPECT_EQ((uintptr_t) second.ptr, (uintptr_t) first.ptr + 32);
		EXPECT_EQ(second.length, 16);
	}

	TEST(MonotonicBufferTest, ReturnsFalseWhenExpansionIsNotPossible) {
		MonotonicBuffer<TestAllocator<16>, 128> buffer{};

		Block first{ buffer.allocate(16) };
		Block second{ buffer.allocate(32) };

		bool success{ buffer.expand(first, 32) };
		EXPECT_FALSE(success);
	}

	TEST(MonotonicBufferTest, DoesNotMutateTheBlocWhenExpansionIsNotPossible) {
		MonotonicBuffer<TestAllocator<16>, 128> buffer{};

		Block first{ buffer.allocate(16) };
		Block second{ buffer.allocate(32) };

		void* startOfFirstBlock{ first.ptr };

		buffer.expand(first, 32);
		EXPECT_EQ(first.ptr, startOfFirstBlock);
		EXPECT_EQ(first.length, 16);
	}

	TEST(MonotonicBufferTest, AllocatesAFreshBlockWhenExpandingAnEmptyBlock) {
		MonotonicBuffer<TestAllocator<32>, 128> buffer{};

		Block block{};
		bool success{ buffer.expand(block, 32) };

		EXPECT_TRUE(success);
		EXPECT_EQ(block.length, 32);

		Block other{};
		buffer.expand(other, 32);

		EXPECT_EQ(block.length, 32);
		EXPECT_EQ(other.length, 32);
		EXPECT_TRUE(block.ptr < other.ptr);
	}

	TEST(MonotonicBufferTest, ReturnsAnEmptyBlockWhenExpandingBeyondCapacity) {
		MonotonicBuffer<TestAllocator<32>, 64> buffer{};

		Block block{ buffer.allocate(32) };
		bool success{ buffer.expand(block, 128) };

		EXPECT_FALSE(success);
	}

	TEST(MonotonicBufferTest, RoundsToAlignmentOnExpansion) {
		MonotonicBuffer<TestAllocator<16>, 128> buffer{};
		Block block{ buffer.allocate(16) };

		buffer.expand(block, 30);
		EXPECT_EQ(block.length, 48);
	}

	TEST(MonotonicBufferTest, RoundsToAlignmentWhenExpandingEmptyBlock) {
		MonotonicBuffer<TestAllocator<32>, 256> buffer{};
		Block block{};

		buffer.expand(block, 16);
		EXPECT_EQ(block.length, 32);
	}

	TEST(MonotonicBufferTest, StatesWheterABlockIsTheLastAllocated) {
		MonotonicBuffer<TestAllocator<16>, 64> buffer{};

		Block first{ buffer.allocate(32) };
		EXPECT_TRUE(buffer.isLastAllocated(first));

		Block second{ buffer.allocate(16) };
		EXPECT_FALSE(buffer.isLastAllocated(first));
		EXPECT_TRUE(buffer.isLastAllocated(second));
	}

	TEST(MonotonicBufferTest, ReallocatesLastAllocatedBlock) {
		MonotonicBuffer<TestAllocator<8>, 128> buffer{};

		Block block{ buffer.allocate(8) };
		bool success { buffer.reallocate(block, 16) };

		EXPECT_TRUE(success);
		EXPECT_EQ(block.length, 16);
	}

	TEST(MonotonicBufferTest, AllocatesAfterReallocation) {
		MonotonicBuffer<TestAllocator<8>, 128> buffer{};

		Block first{ buffer.allocate(8) };
		buffer.reallocate(first, 16);

		Block second{ buffer.allocate(16) };
		EXPECT_GE( (uintptr_t) second.ptr, (uintptr_t) first.ptr + 16);
		EXPECT_EQ(second.length, 16);
	}

	TEST(MonotonicBufferTest, RoundsToAlignmentOnReallocation) {
		MonotonicBuffer<TestAllocator<32>, 128> buffer{};
		Block block{ buffer.allocate(32) };

		buffer.reallocate(block, 60);
		EXPECT_EQ(block.length, 64);
	}

	TEST(MonotonicBufferTest, ReturnsFalseOnReallocationFailure) {
		MonotonicBuffer<TestAllocator<8>, 128> buffer{};
		Block first{ buffer.allocate(64) };
		Block second{ buffer.allocate(32) };

		bool success{ buffer.reallocate(second, 128) };
		EXPECT_FALSE(success);
		EXPECT_GE( (uintptr_t) second.ptr, (uintptr_t)first.ptr + 64);
		EXPECT_EQ(second.length, 32);
	}

	TEST(MonotonicBufferTest, ReallocatesANewBlockWhenInBetweenBlocks) {
		MonotonicBuffer<TestAllocator<16>, 128> buffer{};
		Block first{ buffer.allocate(16) };
		Block second{ buffer.allocate(16) };

		void* originalAddress{ first.ptr };
		buffer.reallocate(first, 32);

		EXPECT_NE(first.ptr, originalAddress);
		EXPECT_GE((uintptr_t)first.ptr, (uintptr_t) originalAddress + 32);
		EXPECT_EQ(first.length, 32);
	}

	TEST(MonotonicBufferTest, CopiesTheBlockOnReallocation) {
		MonotonicBuffer<TestAllocator<16>, 128> buffer{};
		Block first{ buffer.allocate(16) };
		Block second{ buffer.allocate(16) };

		::new(first.ptr) uint64_t{0x4444};
		buffer.reallocate(first, 32);

		uint64_t* data{ reinterpret_cast<uint64_t*>(first.ptr) };
		EXPECT_EQ(*data, 0x4444);
	}

	TEST(MonotonicBufferTest, AllocatesAnArrayOfBlocks) {
		MonotonicBuffer<TestAllocator<16>, 128> buffer{};
		Block block{ buffer.allocateArray(16ULL, 8U) };

		EXPECT_NE(block.ptr, nullptr);
		EXPECT_EQ(block.length, 128ULL);
	}

	TEST(MonotonicBufferTest, ResetsBlockOnDeallocation) {
		MonotonicBuffer<TestAllocator<16>, 128> buffer{};
		Block blk{ buffer.allocate(16) };

		buffer.deallocate(blk);
		EXPECT_EQ(blk.ptr, nullptr);
		EXPECT_EQ(blk.length, 0);
	}

	TEST(MonotonicBufferTest, ResetsArrayBlockOnDeallocation) {
		MonotonicBuffer<TestAllocator<16>, 128> buffer{};
		Block blk{ buffer.allocateArray(16, 4) };

		buffer.deallocateArray(blk, 4);
		EXPECT_EQ(blk.ptr, nullptr);
		EXPECT_EQ(blk.length, 0);
	}

}

}