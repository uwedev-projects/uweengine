#include "gtest/gtest.h"

#include "UE/Memory/Segregator.h"
#include "UE/Memory/Block.h"

#include "TestAllocator.h"

namespace Uwe {

namespace Memory {

	using TestSegregator = Segregator<128, TestAllocator<128>, TestAllocator<256>>;

	TEST(SegregatorTest, AllocatesFromSmallAllocatorWhenSizeIsLessThanThreshold) {
		TestSegregator alloc{};
		Block blk{ alloc.allocate(64) };
		
		EXPECT_EQ(TestAllocator<128>::lastAllocated, blk.ptr);
		EXPECT_EQ(TestAllocator<128>::lastAllocatedSize, blk.length);
		EXPECT_GE(blk.length, 64);
	}

	TEST(SegregatorTest, AllocatesFromSmallAllocatorWhenSizeIsEqualToThreshold) {
		TestSegregator alloc{};
		Block blk{ alloc.allocate(128) };

		EXPECT_EQ(TestAllocator<128>::lastAllocated, blk.ptr);
		EXPECT_EQ(TestAllocator<128>::lastAllocatedSize, blk.length);
		EXPECT_GE(blk.length, 128);
	}

	TEST(SegregatorTest, AllocatesFromLargeAllocatorWhenSizeIsBiggerThanThreshold) {
		TestSegregator alloc{};
		Block blk{ alloc.allocate(129) };

		EXPECT_EQ(TestAllocator<256>::lastAllocated, blk.ptr);
		EXPECT_EQ(TestAllocator<256>::lastAllocatedSize, blk.length);
		EXPECT_GE(blk.length, 129);
	}

}

}