#include "gtest/gtest.h"

#include "UE/Memory/AlignedMallocator.h"
#include "UE/Memory/Block.h"

namespace Uwe {

namespace Memory {

	TEST(AlignedMallocatorTest, AllocatesABlockWithGivenAlignment) {

		AlignedMallocator<16> allocator{};
		Block block{ allocator.allocate(64) };

		EXPECT_NE(block.ptr, nullptr);
		EXPECT_EQ(block.length, 64);
		EXPECT_TRUE((uintptr_t) block.ptr % 16 == 0);

		allocator.deallocate(block);
	}

	TEST(AlignedMallocatorTest, AllocatesABlockWithSizeSmallerThanAlignment) {

		AlignedMallocator<32> allocator{};
		Block block{ allocator.allocate(20) };

		EXPECT_NE(block.ptr, nullptr);
		EXPECT_EQ(block.length, 20);
		EXPECT_TRUE((uintptr_t) block.ptr % 32 == 0);

		allocator.deallocate(block);
	}

	TEST(AlignedMallocatorTest, ReturnsAnEmptyBlockWhenRequestingZeroBytes) {

		AlignedMallocator<16> allocator{};
		Block block{ allocator.allocate(0) };

		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0);

	}

	TEST(AlignedMallocatorTest, DeallocatesAnAlignedBlock) {
	
		AlignedMallocator<16> allocator{};
		Block block{ allocator.allocate(16) };

		allocator.deallocate(block);

		EXPECT_EQ(block.ptr, nullptr);
		EXPECT_EQ(block.length, 0);
	}

	TEST(AlignedMallocatorTest, ReallocatesAnAlignedBlock) {

		AlignedMallocator<16> allocator{};
		Block block{ allocator.allocate(32) };

		bool success{ allocator.reallocate(block, 64) };

		EXPECT_TRUE(success);
		EXPECT_EQ(block.length, 64);
		EXPECT_TRUE((uintptr_t)block.ptr % 16 == 0);
	}

	TEST(AlignedMallocatorTest, CopiesOnReallocation) {

		AlignedMallocator allocator{};
		Block block{ allocator.allocate(16) };

		int* startOfBlock{ reinterpret_cast<int*>(block.ptr) };
		*startOfBlock = 555;

		allocator.reallocate(block, 32);
		startOfBlock = reinterpret_cast<int*>(block.ptr);

		EXPECT_EQ(*startOfBlock, 555);
	}

}

}