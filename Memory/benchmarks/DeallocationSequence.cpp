#include "DeallocationSequence.h"

namespace Uwe {

namespace Memory {

	namespace Detail {

		std::mt19937_64& getRngEngine() {
			static std::mt19937_64 engine{};
			return engine;
		}

	}

}

}