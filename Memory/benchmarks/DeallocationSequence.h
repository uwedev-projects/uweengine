#pragma once

#include <type_traits>
#include <functional>
#include <exception>
#include <random>
#include <vector>
#include <set>

#include <cassert>

namespace Uwe {

namespace Memory {


	namespace Detail {
		std::mt19937_64& getRngEngine();
	}

	template <typename T>
	class DeallocationSequence {
		static_assert(std::is_integral_v<T>, "T needs to be integral.");

	public:
		DeallocationSequence(T maxIndex);
		~DeallocationSequence();

		T getNextIndex();

	protected:
		std::vector<T> blockIndexes;
		std::set<T> indexesInUse;
		T maxIndex;

		T generateNextIndex(std::function<T()> rng);
	};


	template <typename T>
	DeallocationSequence<T>::~DeallocationSequence() {
		assert(blockIndexes.empty());
	}

	template <typename T>
	DeallocationSequence<T>::DeallocationSequence(T maxIndex) 
		: blockIndexes(maxIndex + 1), indexesInUse{}, maxIndex{maxIndex}
	{
		std::mt19937_64& rngEngine{ Detail::getRngEngine() };
		std::uniform_int_distribution<T> rngDistribution{ 0, maxIndex };
		
		auto randomIndex = [&rngEngine, &rngDistribution]() {
			return rngDistribution(rngEngine);
		};

		std::generate_n(blockIndexes.begin(), maxIndex + 1, [this, &randomIndex]() {
			return generateNextIndex(randomIndex);
		});
	}

	template <typename T>
	T DeallocationSequence<T>::generateNextIndex(std::function<T()> rng) {
		T index{};

		do {
			index = rng();
		} while (indexesInUse.count(index) > 0);

		indexesInUse.insert(index);

		return index;
	}

	template <typename T>
	inline T DeallocationSequence<T>::getNextIndex() {
		if (blockIndexes.size() == 0)
			throw std::out_of_range{ "No more blocks to be allocated." };

		T result{ blockIndexes.back() };
		blockIndexes.pop_back();
		return result;
	}
}

}