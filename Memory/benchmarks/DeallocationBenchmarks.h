#ifndef _UWE_MEMORY_DEALLOCATION_BENCHMARKS_
#define _UWE_MEMORY_DEALLOCATION_BENCHMARKS_

#include <benchmark/benchmark.h>
#include <vector>

#include "UE/Memory/Block.h"
#include "DeallocationSequence.h"

namespace Uwe {

namespace Memory {

	namespace Detail {

		template <typename Alloc>
		void fillAllocator(Alloc& alloc, std::vector<Block>& blocks, std::size_t blockSize) {
			while (alloc.bytesRemaining() >= blockSize) {
				blocks.push_back(alloc.allocate(blockSize));
			}
		}
	
	}

	template <typename Alloc>
	void UweBenchmark_Deallocate_SingleBlock(Alloc& alloc, benchmark::State& state, std::size_t blockSize, std::size_t numBlocks) {
		std::vector<Block> blocks{};
		Detail::fillAllocator(alloc, blocks, blockSize);

		DeallocationSequence<std::size_t> seq{ numBlocks - 1ULL };

		for (auto _ : state) {
			state.PauseTiming();

			if (alloc.bytesRemaining() >= Alloc::capacity) {
				seq = { numBlocks - 1ULL };

				blocks.clear();
				Detail::fillAllocator(alloc, blocks, blockSize);
			}

			Block& blk{ blocks.at(seq.getNextIndex()) };

			state.ResumeTiming();

			alloc.deallocate(blk);

		}

		state.SetItemsProcessed(state.iterations());
		state.SetBytesProcessed(state.iterations() * blockSize);
	}

}

}

#endif