#include <benchmark/benchmark.h>

#include "UE/Memory/AlignedMallocator.h"
#include "UE/Memory/BitmapAllocator.h"

#include "AllocationBenchmarks.h"
#include "DeallocationBenchmarks.h"

#include <cstddef>

using namespace Uwe::Memory;

template <std::size_t BlockSize, std::size_t NumBlocks>
void BitmapAllocator_Allocate_SingleBlock(benchmark::State& state) {
	using Alloc = BitmapAllocator<AlignedMallocator<BlockSize>, BlockSize, NumBlocks>;
	Alloc alloc{};

	UweBenchmark_Allocate_SingleBlock(alloc, state, BlockSize);
}

BENCHMARK_TEMPLATE(BitmapAllocator_Allocate_SingleBlock, 4096ULL, 512ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Allocate_SingleBlock, 1024ULL, 512ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Allocate_SingleBlock, 512ULL, 512ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Allocate_SingleBlock, 256ULL, 1024ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Allocate_SingleBlock, 128ULL, 1024ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Allocate_SingleBlock, 64ULL, 1024ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Allocate_SingleBlock, 32ULL, 1024ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Allocate_SingleBlock, 16ULL, 1024ULL);

template <std::size_t BlockSize, std::size_t NumBlocks>
void BitmapAllocator_Deallocate_SingleBlock(benchmark::State& state) {
	using Alloc = BitmapAllocator<AlignedMallocator<BlockSize>, BlockSize, NumBlocks>;
	Alloc alloc{};

	UweBenchmark_Deallocate_SingleBlock(alloc, state, BlockSize, NumBlocks);
}

BENCHMARK_TEMPLATE(BitmapAllocator_Deallocate_SingleBlock, 4096ULL, 512ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Deallocate_SingleBlock, 1024ULL, 512ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Deallocate_SingleBlock, 512ULL, 512ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Deallocate_SingleBlock, 256ULL, 1024ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Deallocate_SingleBlock, 128ULL, 1024ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Deallocate_SingleBlock, 64ULL, 1024ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Deallocate_SingleBlock, 32ULL, 1024ULL);
BENCHMARK_TEMPLATE(BitmapAllocator_Deallocate_SingleBlock, 16ULL, 1024ULL);