#include <benchmark/benchmark.h>
#include <algorithm>
#include <vector>
#include <cstddef>

#include "UE/Memory/AlignedMallocator.h"

using Uwe::Memory::Block;
using Uwe::Memory::AlignedMallocator;

template <std::size_t BlockSize>
void AlignedMallocator_Allocate_SingleBlocks(benchmark::State& state) {

	const auto batchSize = state.range(0);
	std::vector<Block> blocks{};
	blocks.reserve(batchSize);

	AlignedMallocator alloc{};

	while (state.KeepRunningBatch(batchSize)) {
		for (int i{}; i < batchSize; ++i) {
			blocks.emplace_back(alloc.allocate(BlockSize));
		}

		state.PauseTiming();

		std::for_each(blocks.begin(), blocks.end(), [&alloc](Block& block) {
			alloc.deallocate(block);
		});

		blocks.clear();

		state.ResumeTiming();
	}
}

//BENCHMARK_TEMPLATE(AlignedMallocator_Allocate_SingleBlocks, 4096ULL)->Range(2, 256);
//BENCHMARK_TEMPLATE(AlignedMallocator_Allocate_SingleBlocks, 1024ULL)->Range(2, 256);
//BENCHMARK_TEMPLATE(AlignedMallocator_Allocate_SingleBlocks, 512ULL)->Range(2, 256);
//BENCHMARK_TEMPLATE(AlignedMallocator_Allocate_SingleBlocks, 256ULL)->Range(2, 512);
//BENCHMARK_TEMPLATE(AlignedMallocator_Allocate_SingleBlocks, 128ULL)->Range(2, 512);
//BENCHMARK_TEMPLATE(AlignedMallocator_Allocate_SingleBlocks, 64ULL)->Range(2, 1024);
//BENCHMARK_TEMPLATE(AlignedMallocator_Allocate_SingleBlocks, 32ULL)->Range(2, 1024);
//BENCHMARK_TEMPLATE(AlignedMallocator_Allocate_SingleBlocks, 16ULL)->Range(2, 1024);