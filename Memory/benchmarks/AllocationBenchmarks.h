#ifndef _UWE_MEMORY_ALLOCATION_BENCHMARKS_
#define _UWE_MEMORY_ALLOCATION_BENCHMARKS_

#include <benchmark/benchmark.h>

#include "UE/Memory/Block.h"

namespace Uwe {

namespace Memory {

	template <typename Alloc>
	void UweBenchmark_Allocate_SingleBlock(Alloc& alloc, benchmark::State& state, std::size_t blockSize) {
		for (auto _ : state) {
			state.PauseTiming();

			if (alloc.bytesRemaining() < blockSize) {
				alloc.deallocateAll();
			}

			state.ResumeTiming();

			Block blk{ alloc.allocate(blockSize) };
			benchmark::DoNotOptimize(blk.ptr);
			benchmark::ClobberMemory();
		}

		state.SetItemsProcessed(state.iterations());
		state.SetBytesProcessed(state.iterations() * blockSize);
	}

}

}

#endif