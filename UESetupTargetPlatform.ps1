Param(
    [Parameter(Mandatory=$False)]
    [string]$Arch="x64"
)

$SupportedArchitectures = "x64", "x86"

if(!$SupportedArchitectures.contains(${Arch})) {
    Write-Host "Invalid target platform: ${Arch}"
    Exit 4
}

Write-Host "Setting up command line environment for the target platform: ${Arch}"
Write-Host "Checking if VSSetup Module is installed"

if (Get-Module -ListAvailable -Name VSSetup) {
    Write-Host "VSSetup Module exists"
} 
else {
    Write-Host "VSSetup Module does not exist, installing .."
    Install-PackageProvider -Name NuGet -Scope CurrentUser -Force
    Install-Module VSSetup -Scope CurrentUser -Repository PSGallery -Force
}

$VSInst = Get-VSSetupInstance | Select-VSSetupInstance -Version "17.0"

if (!$VSInst) {
    Write-Host "No suitable Visual Studio instance was found."
    Write-Host "Please install Microsoft Visual Studio Version 17.0 or higher with the native x86/x64 C++ compiler."
    Exit 4
}

Write-Host "Found suitable Visual Studio environment:"
Write-Host "Product: $($VSInst.DisplayName)"
Write-Host "Version: $($VSInst.InstallationVersion)"

$VSInstallPath = $VSInst.InstallationPath
$VSSetupEnvPath = "${VSInstallPath}\VC\Auxiliary\Build"

Write-Host $VSInstallPath
Write-Host $VSSetupEnvPath


switch ($Arch) {
    "x64" { $SetupEnvCommand = "vcvars64.bat" }
    "x86" { $SetupEnvCommand = "vcvars32.bat" }
}

Write-Host "Executing vc environment batch file: ${SetupEnvCommand}"

pushd ${VSSetupEnvPath}

cmd /c "${SetupEnvCommand} > nul 2>&1 && set" | .{process{
    if ($_ -match '^([^=]+)=(.*)') {
        [System.Environment]::SetEnvironmentVariable($matches[1], $matches[2])
    }
}}

if ($LASTEXITCODE) {
    throw "Command ${SetupEnvCommand} failed: exit code: ${LASTEXITCODE}"
}

popd

