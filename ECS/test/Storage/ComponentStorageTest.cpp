#include "gtest/gtest.h"

#include "UE/ECS/Storage/ComponentStorage.h"
#include "TestSettings.h"

#include <set>

namespace Uwe {

namespace ECS {

	using namespace Impl;

	struct Position {
		float x;
		float y;
	};

	using PositionStorage = ComponentStorage<Position, TestSettings>;

	TEST(ComponentStorageTest, PushesBackNewComponents) {
		PositionStorage positions{};
		auto& c1 = positions.pushBack(0, 1.0f, 2.0f);
		auto& c2 = positions.pushBack(1, 1.5f, 2.5f);
		auto& c3 = positions.pushBack(2, 2.0f, 2.0f);

		EXPECT_FLOAT_EQ(positions[0].x, 1.0f);
		EXPECT_FLOAT_EQ(positions[0].y, 2.0f);
		EXPECT_FLOAT_EQ(positions[1].x, 1.5f);
		EXPECT_FLOAT_EQ(positions[1].y, 2.5f);
		EXPECT_FLOAT_EQ(positions[2].x, 2.0f);
		EXPECT_FLOAT_EQ(positions[2].y, 2.0f);
	}

	TEST(ComponentStorageTest, PushesBackIndicesForNewComponents) {
		PositionStorage positions{};

		positions.pushBack(6, 1.0f, 2.0f);
		positions.pushBack(8, 1.5f, 2.5f);
		positions.pushBack(12, 2.0f, 2.0f);

		const auto& indices = positions.getIndices();
		EXPECT_EQ(indices[0], 6);
		EXPECT_EQ(indices[1], 8);
		EXPECT_EQ(indices[2], 12);
	}

	TEST(ComponentStorageTest, RemovesComponentAndSwapsWithTheLastIndex) {
		PositionStorage positions{};
		positions.pushBack(0, 1.0f, 2.0f);
		positions.pushBack(1, 1.5f, 2.5f);
		positions.pushBack(2, 2.0f, 2.0f);
		positions.pushBack(3, 2.0f, 0.5f);

		positions.removeAndSwapBack(1);
		EXPECT_EQ(positions.size(), 3);
		EXPECT_FLOAT_EQ(positions[0].x, 1.0f);
		EXPECT_FLOAT_EQ(positions[0].y, 2.0f);
		EXPECT_FLOAT_EQ(positions[1].x, 2.0f);
		EXPECT_FLOAT_EQ(positions[1].y, 0.5f);
		EXPECT_FLOAT_EQ(positions[2].x, 2.0f);
		EXPECT_FLOAT_EQ(positions[2].y, 2.0f);

		positions.removeAndSwapBack(0);
		EXPECT_EQ(positions.size(), 2);
		EXPECT_FLOAT_EQ(positions[0].x, 2.0f);
		EXPECT_FLOAT_EQ(positions[0].y, 2.0f);
		EXPECT_FLOAT_EQ(positions[1].x, 2.0f);
		EXPECT_FLOAT_EQ(positions[1].y, 0.5f);

		positions.removeAndSwapBack(0);
		EXPECT_EQ(positions.size(), 1);
		EXPECT_FLOAT_EQ(positions[0].x, 2.0f);
		EXPECT_FLOAT_EQ(positions[0].y, 0.5f);
	}

	TEST(ComponentStorageTest, RemovesTheLastComponent) {
		PositionStorage positions{};
		positions.pushBack(0, 1.0f, 2.0f);
		positions.removeAndSwapBack(0);
		EXPECT_EQ(positions.size(), 0);
	}

	class ComponentWithDestructor {

		int data;

		static std::set<ComponentWithDestructor*>& getObjectsDestroyed() {
			static std::set<ComponentWithDestructor*> objectsDestroyed{};
			return objectsDestroyed;
		}

	public:

		ComponentWithDestructor()
			: data{}
		{}

		ComponentWithDestructor(int data)
			: data{ data }
		{}

		~ComponentWithDestructor()
		{
			addDestroyedObject(this);
		}

		static bool isDestroyed(ComponentWithDestructor* obj) {
			return getObjectsDestroyed().count(obj) == 1;
		}

		static void addDestroyedObject(ComponentWithDestructor* obj) {
			getObjectsDestroyed().insert(obj);
		}
	};

	using ComponentWithDestructorStorage = ComponentStorage<ComponentWithDestructor, TestSettings>;

	TEST(ComponentStorageTest, RemovingAComponentCallsItsDestructor) {
		ComponentWithDestructorStorage components{};
		components.pushBack(0, 1);

		ComponentWithDestructor* comp{ &components[0] };
		components.removeAndSwapBack(0);
		EXPECT_TRUE(ComponentWithDestructor::isDestroyed(comp));
	}

	TEST(ComponentStorageTest, RemovingAnIndexOutOfBoundsReturnsInvalidIndex) {
		PositionStorage positions{};
		positions.pushBack(0, 1.0f, 2.0f);

		EXPECT_EQ(positions.removeAndSwapBack(-1), PositionStorage::InvalidIndex);
		EXPECT_EQ(positions.removeAndSwapBack(1), PositionStorage::InvalidIndex);
		
	}

	TEST(ComponentStorageTest, RemoveAndSwapBackReturnsEntityIndexOfTheSwappedComponent) {
		PositionStorage positions{};
		positions.pushBack(5, 1.0f, 0.0f);
		positions.pushBack(2, 2.0f, 0.0f);
		positions.pushBack(3, 3.0f, 0.0f);

		EXPECT_EQ(positions.removeAndSwapBack(0), 3);
		EXPECT_EQ(positions.removeAndSwapBack(1), 2);
		EXPECT_EQ(positions.removeAndSwapBack(0), 3);

	}

	TEST(ComponentStorageTest, RemoveAndSwapBackUpdatesTheIndices) {
		PositionStorage positions{};
		positions.pushBack(4, 1.0f, 0.0f);
		positions.pushBack(2, 2.0f, 0.0f);
		positions.pushBack(5, 3.0f, 0.0f);

		const auto& indices = positions.getIndices();
		
		positions.removeAndSwapBack(0);
		EXPECT_EQ(indices[0], 5);
		EXPECT_EQ(indices[1], 2);
		EXPECT_EQ(indices[2], PositionStorage::InvalidIndex);

		positions.removeAndSwapBack(0);
		EXPECT_EQ(indices[0], 2);
		EXPECT_EQ(indices[1], PositionStorage::InvalidIndex);
		EXPECT_EQ(indices[2], PositionStorage::InvalidIndex);

	}

	TEST(ComponentStorageTest, ClearsAllComponentsFromTheStorage) {
		PositionStorage positions{};
		positions.pushBack(0, 1.0f, 2.0f);
		positions.pushBack(1, 1.5f, 2.5f);

		positions.clear();
		EXPECT_EQ(positions.size(), 0);
	}

	TEST(ComponentStorageTest, ClearsAllIndicesFromTheStorage) {
		PositionStorage positions{};
		positions.pushBack(0, 1.0f, 2.0f);
		positions.pushBack(1, 1.5f, 2.5f);

		positions.clear();
		const auto& indices = positions.getIndices();
		EXPECT_EQ(indices.size(), 0);
	}

	TEST(ComponentStorageTest, ReusesOldIndicesWhenPossible) {
		PositionStorage positions{};
		positions.pushBack(0, 1.0f, 2.0f);
		positions.pushBack(1, 1.5f, 2.5f);

		positions.clear();
		positions.pushBack(0, 0.0f, 0.25f);
		positions.pushBack(1, -1.0f, 1.0f);
		
		auto& c0 = positions[0];
		auto& c1 = positions[1];

		EXPECT_FLOAT_EQ(c0.x, 0.0f);
		EXPECT_FLOAT_EQ(c0.y, 0.25f);
		EXPECT_FLOAT_EQ(c1.x, -1.0f);
		EXPECT_FLOAT_EQ(c1.y, 1.0f);
	}

	TEST(ComponentStorageTest, SwapsTwoComponentsInTheStorage) {
		PositionStorage positions{};
		positions.pushBack(0, 1.0f, 2.0f);
		positions.pushBack(1, 1.5f, 2.5f);
		positions.pushBack(2, 2.5f, 4.5f);

		positions.swap(0, 2);
		EXPECT_FLOAT_EQ(positions[0].x, 2.5f);
		EXPECT_FLOAT_EQ(positions[0].y, 4.5f);
		EXPECT_FLOAT_EQ(positions[2].x, 1.0f);
		EXPECT_FLOAT_EQ(positions[2].y, 2.0f);

		positions.swap(1, 2);
		EXPECT_FLOAT_EQ(positions[1].x, 1.0f);
		EXPECT_FLOAT_EQ(positions[1].y, 2.0f);
		EXPECT_FLOAT_EQ(positions[2].x, 1.5f);
		EXPECT_FLOAT_EQ(positions[2].y, 2.5f);
	}

	TEST(ComponentStorageTest, SwappingTwoComponentsSwapsTheirIndices) {
		PositionStorage positions{};
		positions.pushBack(12, 1.0f, 2.0f);
		positions.pushBack(6, 1.5f, 2.5f);
		positions.pushBack(0, 2.5f, 4.5f);

		const auto& indices = positions.getIndices();

		positions.swap(0, 2);
		EXPECT_EQ(indices[0], 0);
		EXPECT_EQ(indices[2], 12);

		positions.swap(1, 2);
		EXPECT_EQ(indices[1], 12);
		EXPECT_EQ(indices[2], 6);

		positions.swap(1, 0);
		EXPECT_EQ(indices[0], 12);
		EXPECT_EQ(indices[1], 0);
	}

}

}