#include "gtest/gtest.h"

#include "UE/ECS/Storage/IndexTable.h"
#include "TestSettings.h"
#include <cstdint>

namespace Uwe {

namespace ECS {

	namespace Impl {

		TEST(IndexTableTest, InsertsNewIndices) {
			IndexTable<TestSettings> indices{};

			indices.insert(0, 12);
			indices.insert(1, 13);
			indices.insert(2, 14);
			indices.insert(3, 8);
			indices.insert(4, 9);

			EXPECT_EQ(indices[0], 12);
			EXPECT_EQ(indices[1], 13);
			EXPECT_EQ(indices[2], 14);
			EXPECT_EQ(indices[3], 8);
			EXPECT_EQ(indices[4], 9);
		}

		TEST(IndexTableTest, ChecksIfAnIndexExists) {
			IndexTable<TestSettings> indices{};

			EXPECT_FALSE(indices.exists(0));
			EXPECT_FALSE(indices.exists(1));

			indices.insert(0, 0);
			indices.insert(3, 3);
			indices.insert(5, 5);

			EXPECT_TRUE(indices.exists(0));
			EXPECT_TRUE(indices.exists(3));
			EXPECT_TRUE(indices.exists(5));
			EXPECT_FALSE(indices.exists(1));
			EXPECT_FALSE(indices.exists(4));
			EXPECT_FALSE(indices.exists(6));
		}

		TEST(IndexTableTest, RemovesAnIndexAndSwapsItsEntryWithTheSwapIndex) {
			IndexTable<TestSettings> indices{};

			indices.insert(10, 0);
			indices.insert(11, 1);
			indices.insert(12, 2);
			indices.insert(13, 3);

			indices.removeAndSwap(10, 13);
			EXPECT_EQ(indices[10], IndexTable<TestSettings>::InvalidIndex);
			EXPECT_EQ(indices[11], 1);
			EXPECT_EQ(indices[12], 2);
			EXPECT_EQ(indices[13], 0);

			indices.removeAndSwap(12, 12);
			EXPECT_EQ(indices[10], IndexTable<TestSettings>::InvalidIndex);
			EXPECT_EQ(indices[11], 1);
			EXPECT_EQ(indices[12], IndexTable<TestSettings>::InvalidIndex);
			EXPECT_EQ(indices[13], 0);

			indices.removeAndSwap(13, 11);
			EXPECT_EQ(indices[10], IndexTable<TestSettings>::InvalidIndex);
			EXPECT_EQ(indices[11], 0);
			EXPECT_EQ(indices[12], IndexTable<TestSettings>::InvalidIndex);
			EXPECT_EQ(indices[13], IndexTable<TestSettings>::InvalidIndex);
		}

		TEST(IndexTableTest, RemoveAndSwapReturnsTheDeadComponentIndex) {
			IndexTable<TestSettings> indices{};

			indices.insert(5, 0);
			indices.insert(6, 1);
			indices.insert(7, 2);

			EXPECT_EQ(indices.removeAndSwap(6, 7), 1);
			EXPECT_EQ(indices.removeAndSwap(5, 7), 0);
			EXPECT_EQ(indices.removeAndSwap(7, 7), 0);
		}

		TEST(IndexTableDeathTest, RemovingAnInvalidIndexIsNotAllowed) {
			IndexTable<TestSettings> indices{};

			EXPECT_DEATH(indices.removeAndSwap(0, 0), "");

			indices.insert(5, 0);
			indices.insert(8, 1);
			indices.insert(12, 2);

			EXPECT_DEATH(indices.removeAndSwap(-1, 0), "");
			EXPECT_DEATH(indices.removeAndSwap(indices.size(), 0), "");
		}

		TEST(IndexTableTest, FailedRemovalDoesNotChangeTheIndices) {
			IndexTable<TestSettings> indices{};

			indices.insert(10, 1);
			indices.insert(11, 2);
			indices.insert(12, 0);

			indices.removeAndSwap(0, 12);

			EXPECT_EQ(indices[10], 1);
			EXPECT_EQ(indices[11], 2);
			EXPECT_EQ(indices[12], 0);
		}

		TEST(IndexTableTest, GetReturnsInvalidIndexWhenRetrievingNonExistingIndex) {
			IndexTable<TestSettings> indices{};
			
			EXPECT_EQ(indices.get(0), IndexTable<TestSettings>::InvalidIndex);
			EXPECT_EQ(indices.get(1), IndexTable<TestSettings>::InvalidIndex);

			indices.insert(5, 4);
			indices.insert(3, 2);

			EXPECT_EQ(indices.get(2), IndexTable<TestSettings>::InvalidIndex);
			EXPECT_EQ(indices.get(4), IndexTable<TestSettings>::InvalidIndex);
		}

		TEST(IndexTableTest, ClearsAllIndices) {
			IndexTable<TestSettings> indices{};

			indices.insert(5, 0);
			indices.insert(6, 1);
			indices.insert(7, 2);

			indices.clear();

			EXPECT_EQ(indices.size(), 0);
			EXPECT_EQ(indices.get(5), IndexTable<TestSettings>::InvalidIndex);
			EXPECT_EQ(indices.get(6), IndexTable<TestSettings>::InvalidIndex);
			EXPECT_EQ(indices.get(7), IndexTable<TestSettings>::InvalidIndex);
		}

		TEST(IndexTableTest, SwapsTwoIndices) {
			IndexTable<TestSettings> indices{};

			indices.insert(5, 0);
			indices.insert(6, 1);
			indices.insert(7, 2);

			indices.swap(5, 7);
			EXPECT_EQ(indices[5], 2);
			EXPECT_EQ(indices[6], 1);
			EXPECT_EQ(indices[7], 0);

			indices.swap(6, 5);
			indices.swap(6, 5);
		}

		
	}

}

}