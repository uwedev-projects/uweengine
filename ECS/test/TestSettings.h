#pragma once

namespace Uwe {

namespace ECS {

	struct TestSettings {
		using EntityIndex = std::int32_t;
		using ComponentIndex = std::int32_t;

		static constexpr std::size_t MaxEntities = 1ULL << 16;
	};

}

}