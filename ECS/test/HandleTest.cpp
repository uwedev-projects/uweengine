#include "gtest/gtest.h"

#include "UE/ECS/Handle.h"

namespace Uwe {

namespace ECS {

	TEST(HandleTest, ChecksEntitiesAreEqual) {
		Handle h1{ 0, 1 }, h2{ 0, 1 }, h3{ 1, 1 }, h4{ 1, 1 };

		EXPECT_EQ(h1, h2);
		EXPECT_EQ(h3, h4);
		EXPECT_EQ(h1, h1);
		EXPECT_EQ(h4, h4);
	}

	TEST(HandleTest, ChecksEntitesAreNotEqual) {
		Handle h1{ 0, 1 }, h2{ 1, 1 }, h3{ 2, 1 }, h4{ 3, 1 };

		EXPECT_NE(h1, h2);
		EXPECT_NE(h1, h3);
		EXPECT_NE(h1, h4);
	}

	TEST(HandleTest, IsAliveWhenCounterAndIndexAreNotZero) {
		Handle h1{ 0, 1 }, h2{ 1, 0 }, h3{ 1, 1 };

		EXPECT_TRUE(h1.isValid());
		EXPECT_TRUE(h2.isValid());
		EXPECT_TRUE(h3.isValid());
		
	}

	TEST(HandleTest, IsDeadWhenCoutnerAndIndexAreZero) {
		Handle h{ 0 , Handle::CounterInvalid };
		EXPECT_FALSE(h.isValid());
	}
}

}