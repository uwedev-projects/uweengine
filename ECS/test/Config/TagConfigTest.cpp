#include "gtest/gtest.h"

#include "UE/ECS/Config/TagConfig.h"
#include "UE/ECS/Tags.h"

namespace Uwe {

namespace ECS {

	struct TagA : TagTag {};
	struct TagB : TagTag {};
	struct TagC : TagTag {};

	using MyTags = TagConfig<TagA, TagB, TagC>;

	TEST(TagConfigList, GetsTheTagsTypeID) {
		static_assert(MyTags::getTypeID<TagA>() == 0, "Expected a type id of 0");
		static_assert(MyTags::getTypeID<TagB>() == 1, "Expected a type id of 1");
		static_assert(MyTags::getTypeID<TagC>() == 2, "Expected a type id of 2");
	}

	TEST(TagConfigList, CalculatesTheTagCount) {
		static_assert(MyTags::TagCount == 3, "Expected a tag count of 3");
	}
}

}