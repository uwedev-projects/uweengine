#include "gtest/gtest.h"

#include "UE/ECS/Config/ComponentConfig.h"
#include "UE/ECS/Tags.h"

namespace Uwe {

namespace ECS {

	struct ComponentA : ComponentTag {};
	struct ComponentB : ComponentTag {};
	struct ComponentC : ComponentTag {};

	using MyComponentConfig = ComponentConfig<ComponentA, ComponentB, ComponentC>;

	TEST(ComponentConfigTest, GetsTheComponentsTypeID) {
		static_assert(MyComponentConfig::getTypeID<ComponentA>() == 0, "Expected a type id of 0");
		static_assert(MyComponentConfig::getTypeID<ComponentB>() == 1, "Expected a type id of 1");
		static_assert(MyComponentConfig::getTypeID<ComponentC>() == 2, "Expected a type id of 2");
	}

	TEST(ComponentConfigTest, CalculatesTheComponentCount) {
		static_assert(MyComponentConfig::ComponentCount == 3, "Expected a component count of 3.");
	}

}

}
