#include "gtest/gtest.h"

#include "UE/ECS/Config/ComponentConfig.h"
#include "UE/ECS/Config/TagConfig.h"
#include "UE/ECS/Settings.h"
#include "UE/ECS/Tags.h"

namespace Uwe {

namespace ECS {

	struct ComponentA : ComponentTag {};
	struct ComponentB : ComponentTag {};
	struct ComponentC : ComponentTag {};

	struct TagA : TagTag {};
	struct TagB : TagTag {};
	struct TagC : TagTag {};

	using MyComponentConfig = ComponentConfig<ComponentA, ComponentB, ComponentC>;
	using MyTagConfig = TagConfig<TagA, TagB, TagC>;
	struct MySystemConfig : SystemConfigTag {};

	using MySettings = Settings<
		MyComponentConfig, 
		MyTagConfig, 
		MySystemConfig
	>;

	TEST(SettingsTest, ReturnsTheComponentsTypeID) {
		static_assert(MySettings::getComponentTypeID<ComponentA>() == 0, "Expected a component type id of 0");
		static_assert(MySettings::getComponentTypeID<ComponentB>() == 1, "Expected a component type id of 1");
		static_assert(MySettings::getComponentTypeID<ComponentC>() == 2, "Expected a component type id of 2");
	}

	TEST(SettingsTest, ReturnsTheTagsTypeID) {
		static_assert(MySettings::getTagTypeID<TagA>() == 0, "Expected a tag type id of 0");
		static_assert(MySettings::getTagTypeID<TagB>() == 1, "Expected a tag type id of 1");
		static_assert(MySettings::getTagTypeID<TagC>() == 2, "Expected a tag type id of 2");
	}

	TEST(SettingsTest, ReturnsTheComponentsCorrespondingBit) {
		static_assert(MySettings::getComponentBit<ComponentA>() == 0, "Expected a component bit of 0");
		static_assert(MySettings::getComponentBit<ComponentB>() == 1, "Expected a component bit of 1");
		static_assert(MySettings::getComponentBit<ComponentC>() == 2, "Expected a component bit of 2");
	}

	TEST(SettingsTest, ReturnsTheTagsCorrespondingBit) {
		static_assert(MySettings::getTagBit<TagA>() == 3, "Expected a tag bit of 3");
		static_assert(MySettings::getTagBit<TagB>() == 4, "Expected a tag bit of 4");
		static_assert(MySettings::getTagBit<TagC>() == 5, "Expected a tag bit of 5");
	}

	TEST(SettingsTest, ReturnsTheComponentCount) {
		static_assert(MySettings::ComponentCount == 3, "Expected a component count of 3");
	}

	TEST(SettingsTest, ReturnsTheTagCount) {
		static_assert(MySettings::TagCount == 3, "Expected a tag count of 3");
	}

	TEST(SettingsTest, ReturnsTheBitCount) {
		static_assert(MySettings::BitCount == 6, "Expected a bit count of 6");
	}

}

}