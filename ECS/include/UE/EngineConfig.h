#ifndef _UWE_ENGINE_CONFIG_H_
#define _UWE_ENGINE_CONFIG_H_

#define UWE_BUILD_TYPE_UNDEFINED		0x00
#define UWE_BUILD_TYPE_DEBUG			0x01
#define UWE_BUILD_TYPE_RELEASE			0x02
#define UWE_BUILD_TYPE_RELWITHDEBINFO	0x06
#define UWE_BUILD_TYPE_MINSIZEREL		0x0A

#define UWE_BUILD_TYPE UWE_BUILD_TYPE_DEBUG

namespace Uwe {

namespace Config {

	constexpr char* UweEngineVersion = "0.1.0";
	constexpr short UweEngineVersionMajor = 0;
	constexpr short UweEngineVersionMinor = 1;
	constexpr short UweEngineVersionPatch = 0;


	enum class BuildType : unsigned char { Undefined = 0U, Debug, Release, RelWithDebInfo, MinSizeRel };
	constexpr BuildType Build		= BuildType::Debug;

	constexpr bool IsBigEndian		= false;
	constexpr bool IsLittleEndian	= !IsBigEndian;

}

}

#endif
