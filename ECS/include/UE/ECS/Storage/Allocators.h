#pragma once

#include "UE/ECS/Settings.h"
#include "UE/Memory/STLAllocator.h"
#include "UE/Memory/MonotonicBuffer.h"
#include "UE/Memory/AlignedMallocator.h"

#include <cstddef>
#include <type_traits>

namespace Uwe {

namespace ECS {

	namespace Impl {

		using Uwe::Memory::STLAllocator;
		using Uwe::Memory::MonotonicBuffer;
		using Uwe::Memory::AlignedMallocator;

		struct DefaultAllocationPolicy {
			static constexpr std::size_t ArenaSize{ 16 * 1024 * 1024 };
		};

		template <typename Settings>
		struct DefaultIndexAllocationPolicy {
			static constexpr std::size_t ArenaSize{ sizeof(typename Settings::ComponentIndex) * Settings::MaxEntities };
		};

		struct ComponentAllocatorPool {

			template <typename C>
			static auto& getAllocatorInstance() {
				static DefaultComponentAllocator<C> instance{};
				return instance;
			}

		};

		template <typename Settings>
		struct IndexAllocatorPool {

			template <typename T>
			static auto& getAllocatorInstance() {
				static DefaultIndexAllocator<Settings> instance{};
				return instance;
			}
		};

		template <typename C, class Policy = DefaultAllocationPolicy>
		using DefaultComponentAllocator = MonotonicBuffer <AlignedMallocator<alignof(C)>, Policy::ArenaSize>;

		template <typename Settings, class Policy = DefaultIndexAllocationPolicy<Settings> >
		using DefaultIndexAllocator = MonotonicBuffer <
			AlignedMallocator<alignof(typename Settings::ComponentIndex)>, 
			Policy::ArenaSize
		>;

		template <typename C>
		using DefaultSTLComponentAllocator = STLAllocator< C, DefaultComponentAllocator<C>, ComponentAllocatorPool>;

		template <typename Settings>
		using DefaultSTLIndexAllocator = STLAllocator<
			typename Settings::ComponentIndex, 
			DefaultIndexAllocator<Settings>, 
			IndexAllocatorPool<Settings>
		>;

	}

}

}