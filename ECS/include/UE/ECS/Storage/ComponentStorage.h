#pragma once

#include "UE/ECS/Storage/Allocators.h"
#include <utility>
#include <vector>
#include <cstddef>

namespace Uwe {

namespace ECS {

	namespace Impl {

		template <
			typename C, 
			typename Settings, 
			class ComponentAllocator = DefaultSTLComponentAllocator<C>, 
			class IndexAllocator = DefaultSTLIndexAllocator<Settings>
		>
		class ComponentStorage {

		private:

			using EntityIndex = typename Settings::EntityIndex;
			using ComponentIndex = typename Settings::ComponentIndex;

			ComponentAllocator componentAlloc;
			IndexAllocator indexAlloc;
			std::vector<C, ComponentAllocator> components;
			std::vector<EntityIndex, IndexAllocator> indices;

		public:

			using Component = C;
			static constexpr ComponentIndex InvalidIndex{ -1 };

			ComponentStorage() 
				: componentAlloc{}, indexAlloc{}, components{ componentAlloc }, indices{indexAlloc}
			{}

			ComponentIndex size() const noexcept {
				return static_cast<ComponentIndex>(components.size());
			}

			template <typename... Args>
			C& pushBack(EntityIndex index, Args&&... args) {
				components.emplace_back(std::forward<Args>(args)...);
				indices.emplace_back(index);
				return components.back();
			}

			EntityIndex removeAndSwapBack(ComponentIndex index) {
				EntityIndex swapIndex{ InvalidIndex };

				if (index >= 0 && index < size()) {
					const ComponentIndex lastIndex = size() - 1;
					std::swap(components[index], components[lastIndex]);
					components.pop_back();

					indices[index] = swapIndex = indices[lastIndex];
					indices[lastIndex] = InvalidIndex;
				}
				
				return swapIndex;
			}

			inline void clear() {
				components.clear();
				indices.clear();
			}

			inline void swap(int first, int second) {
				std::swap(components[first], components[second]);
				std::swap(indices[first], indices[second]);
			}

			inline C& operator[](std::size_t index) {
				return components[index];
			}


			inline const C& operator[](std::size_t index) const {
				return components[index];
			}

			inline const auto& getIndices() const noexcept {
				return indices;
			}
		};

	}

}

}