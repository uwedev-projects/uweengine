#pragma once

#include "UE/ECS/Storage/Allocators.h"
#include <vector>
#include <cstddef>
#include <cassert>


namespace Uwe {

namespace ECS {

	namespace Impl {

		template <typename Settings, class StorageAllocator = DefaultSTLIndexAllocator<Settings>>
		class IndexTable {
	
		private:

			using EntityIndex		= typename Settings::EntityIndex;
			using ComponentIndex	= typename Settings::ComponentIndex;
		
			StorageAllocator alloc;
			std::vector<ComponentIndex, StorageAllocator> indices;

		public:

			static constexpr ComponentIndex InvalidIndex{ -1 };

			IndexTable() 
				: alloc{}, indices{ alloc }
			{}

			void insert(EntityIndex index, ComponentIndex mappedIndex) {
				if (index >= size())
					growToIndex(index);

				indices[index] = mappedIndex;
			};

			bool exists(EntityIndex index) const {
				bool result{};

				if (index >= 0 && index < indices.size()) 
					result = indices[index] != InvalidIndex;

				return result;
			};

			ComponentIndex get(EntityIndex index) const {
				ComponentIndex result = InvalidIndex;
				if (index >= 0 && index < size())
					result = indices[index];

				return result;
			};

			inline ComponentIndex operator[](std::size_t index) const {
				return get(static_cast<EntityIndex>(index));
			}

			void swap(EntityIndex first, EntityIndex second) {
				std::swap(indices[first], indices[second]);
			}

			ComponentIndex removeAndSwap(EntityIndex deadEntityIndex, EntityIndex swapIndex) {
				assert(deadEntityIndex >= 0 && deadEntityIndex < size() && "Removing invalid index");

				ComponentIndex deadComponentIndex{ get(deadEntityIndex) };
				if (deadComponentIndex >= 0) {
					indices[deadEntityIndex] = InvalidIndex;

					if (deadEntityIndex != swapIndex)
						indices[swapIndex] = deadComponentIndex;
				}

				return deadComponentIndex;
			}

			void clear() {
				indices.clear();
			}

			inline int size() const noexcept {
				return static_cast<int>(indices.size());
			}

		private:

			void growToIndex(EntityIndex index) {
				auto newSize = size() > 0 ? size() : 1;

				while (index >= size()) {
					newSize *= 2;
					indices.resize(newSize, InvalidIndex);
				}
				
			}

		};

	}

}

}