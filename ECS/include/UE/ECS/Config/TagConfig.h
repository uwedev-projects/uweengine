#pragma once

#include "UE/ECS/Tags.h"
#include "UE/MPL/TypeList.h"

#include <cstddef>

namespace Uwe {

namespace ECS {

	template <typename... Tags>
	struct TagConfig : TagConfigTag {
		static_assert(IsTagList<Tags...>(), "Cannot create TagConfig from non tag types.");
		
		using TagList = MPL::TypeList<Tags...>;
		static constexpr std::size_t TagCount{ MPL::size<TagList>() };

		template <typename T>
		static constexpr std::size_t getTypeID() noexcept {
			static_assert(IsTag<T>(), "Cannot retrieve the type id of a non tag type.");
			return MPL::indexOf<T, TagList>();
		}
	};

}

}