#pragma once

#include "UE/MPL/TypeList.h"
#include "UE/ECS/Tags.h"

#include "UE/MPL/TypeList.h"

#include <cstddef>
#include <type_traits>

namespace Uwe {

namespace ECS {

	template <typename... Components>
	struct ComponentConfig : ComponentConfigTag {
		static_assert(IsComponentList<Components...>(), "Cannot create a ComponentConfig from non component types.");

		using ComponentList = MPL::TypeList<Components...>;
		static constexpr std::size_t ComponentCount{ MPL::size<ComponentList>() };

		template <typename T>
		static constexpr std::size_t getTypeID() noexcept {
			static_assert(IsComponent<T>(), "Cannot retrieve type id of non component type.");
			return MPL::indexOf<T, ComponentList>();
		}
	};

}

}