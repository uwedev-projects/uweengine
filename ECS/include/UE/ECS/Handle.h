#pragma once

#include <cstddef>
#include <bitset>

namespace Uwe {

namespace ECS {

	struct Handle {
		static constexpr int IndexBits{ 16 };
		static constexpr int CounterBits{ 16 };
		static constexpr int CounterInvalid{ 0 };

		unsigned index : IndexBits;
		unsigned counter : CounterBits;

		constexpr Handle(unsigned index, unsigned counter)
			: index{index}, counter{counter}
		{}

		constexpr inline bool operator==(const Handle& rhs) const noexcept {
			return index == rhs.index && counter == rhs.counter;
		}

		constexpr inline bool operator!=(const Handle& rhs) const noexcept {
			return !(*this == rhs);
		}

		constexpr bool isValid() const noexcept {
			return !(index == 0 && counter == CounterInvalid);
		}

		constexpr operator bool() const noexcept {
			return isValid();
		}
	};

}

}