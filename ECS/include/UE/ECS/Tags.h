#ifndef UWE_ECS_TAGS_H
#define UWE_ECS_TAGS_H

#include "UE/ECS/Util/Macros.h"

namespace Uwe {

namespace ECS {

	UWE_CREATE_TAG(Settings)
	UWE_CREATE_TAG(Tag)
	UWE_CREATE_TAG(System)
	UWE_CREATE_TAG(Component)
	UWE_CREATE_TAG(TagConfig)
	UWE_CREATE_TAG(ComponentConfig)
	UWE_CREATE_TAG(SystemConfig)
	UWE_CREATE_TAG(SignatureConfig)

}

}

#endif