#pragma once

#include "UE/MPL/TypeList.h"
#include "UE/ECS/Tags.h"

#include <cstdint>

namespace Uwe {

namespace ECS {

	template <
		typename ComponentConfig,
		typename TagConfig,
		typename SystemConfig
	> 
	struct Settings : SettingsTag {
		static_assert(IsComponentConfig<ComponentConfig>(), "Invalid ComponentConfig given to Settings");
		static_assert(IsTagConfig<TagConfig>(), "Invalid TagConfig given to Settings");

		static constexpr std::size_t IndexBits = 16;
		static constexpr std::size_t MaxEntites = 1ULL << IndexBits;

		using EntityIndex = std::int32_t;
		using ComponentIndex = std::int32_t;



		static constexpr std::size_t ComponentCount{ ComponentConfig::ComponentCount };
		static constexpr std::size_t TagCount{ TagConfig::TagCount };
		static constexpr std::size_t BitCount{ ComponentCount + TagCount };

		template <typename T>
		static constexpr auto getComponentTypeID() noexcept {
			return ComponentConfig::getTypeID<T>();
		}

		template <typename T>
		static constexpr auto getTagTypeID() noexcept {
			return TagConfig::getTypeID<T>();
		}

		template <typename T>
		static constexpr auto getComponentBit() noexcept {
			return getComponentTypeID<T>();
		}

		template <typename T>
		static constexpr auto getTagBit() noexcept {
			return ComponentCount + getTagTypeID<T>();
		}
	};

}

}