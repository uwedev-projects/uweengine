#ifndef UWE_ECS_MACROS_H
#define UWE_ECS_MACROS_H

#include <type_traits>
#include "UE/EngineMacros.h"
#include "UE/MPL/TypeList.h"

#define UWE_CREATE_TAG(name) \
	struct UWE_CONCAT_TOKENS(name, Tag) {};																	\
																											\
	template < typename T >																					\
	constexpr bool UWE_CONCAT_TOKENS(Is, name) () {															\
		return std::is_base_of<UWE_CONCAT_TOKENS(name, Tag), T>();											\
	}																										\
																											\
	template < typename T >																					\
	using UWE_CONCAT_TOKENS(IsValid, name) = std::is_base_of<UWE_CONCAT_TOKENS(name, Tag), T>;				\
																											\
																											\
	template < typename... Types >																			\
	constexpr bool UWE_CONCAT_TRIPLE(Is, name, List) () {													\
		return MPL::all<MPL::TypeList<Types...>, UWE_CONCAT_TOKENS(IsValid, name)>();						\
	} 


#endif