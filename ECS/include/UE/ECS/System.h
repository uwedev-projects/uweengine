#pragma once

#include "UE/ECS/Tags.h"
#include "UE/MPL/TypeList.h"

namespace Uwe {

namespace ECS {

	namespace Detail {
		template <typename Bitset>
		struct BitsetSingle {
			Bitset required;
		};

		template <typename Bitset>
		struct BitsetDouble {
			Bitset required;
			Bitset forbidden;
		};
	}


	template <class Manager, typename Derived, typename Required, typename Forbidden = MPL::TypeList<>>
	class System : public SystemTag {
	protected:
		Manager& mgr;
		Derived& implementation() const noexcept {
			return *static_cast<Derived*>(this);
		}

	public:
		System(Manager& mgr)
			: mgr{ mgr }
		{

		}


	};

}

}