# UweEngine v0.1.0 #

Disclaimer:
This is a privat project which I am working on in my spare time. The project is only
supported on Windows and uses CMake as it's build tool. 

## Components (incomplete) ##

### UweMemory ###
Memory management library which enables users to build custom allocators from composable 
templates. The idea was inspired by a talk from Andrei Alexandrescu at CppCon2015 (https://www.youtube.com/watch?v=LIb3L4vKZ7U).

### UwePlatform ###
Library that contains the windows platform layer code and isolates it from the rest of the 
components.

### UweECS ###
Entity component system to manage game objects in a data oriented way.

### UweMPL ###
Small header only library containing meta programming functions.

...

## Minimum Prerequisites ##

To build the project you'll need:

*  Microsoft Visual Studio [Build Tools] 2017
*  CMake Version 3.0.12
*  Git for Windows

With git and CMake installed all third party dependencies will be pulled during the CMake
configure stage.
