cmake_minimum_required (VERSION 3.12)

cmake_policy (SET CMP0069 NEW)

set(CMAKE_EXPORT_PACKAGE_REGISTRY ON)

message (STATUS "Loading cmake cache for UweEngine ..")

include (TestBigEndian)
TEST_BIG_ENDIAN(ENDIAN)

if(ENDIAN) 
	set(IS_BIG_ENDIAN "true" CACHE STRING "" FORCE)
else()
	set(IS_BIG_ENDIAN "false" CACHE STRING "" FORCE)
endif()

if(CMAKE_BUILD_TYPE)
	set(UWE_BUILD_TYPE "${CMAKE_BUILD_TYPE}" CACHE STRING "" FORCE)
else()
	set(UWE_BUILD_TYPE "Undefined" CACHE STRING "" FORCE)
endif()

string (TOUPPER "UWE_BUILD_TYPE_${UWE_BUILD_TYPE}" UWE_BUILD_TYPE_DEF)


set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

option(ENABLE_IPO "Use link time optimization" OFF)

if (ENABLE_IPO)
	include(CheckIPOSupported)
	check_ipo_supported(RESULT result OUTPUT output)

	if(result)
		set (CMAKE_INTERPROCEDURAL_OPTIMIZATION ON)
	else()
		message (WARNING "IPO is not supported: ${output}")
	endif()
endif()

function (addCompileOptions)
	# enable ASAN thread sanitizer
	#target_compile_options(${ARGV0} ${ARGV1} "$<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:/fsanitize=address>")
endfunction()

set(MEMORYCHECK_COMMAND "${CMAKE_CURRENT_BINARY_DIR}\\Tools\\TestCoverageHelper\\TestCoverageHelper.exe")
set(MEMORYCHECK_COMMAND_OPTIONS "--sep--")
set(MEMORYCHECK_TYPE "Valgrind")

set(UEBaseLibs "Core" "ECS" "Memory" "MPL" "Platform")
foreach(Lib IN LISTS UEBaseLibs) 
	string(APPEND UELibs "\"${Lib}\", ")
endforeach()

string(REGEX REPLACE ", $" "" UELibs "${UELibs}")

message ( STATUS "${UELibs}")